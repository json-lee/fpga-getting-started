# FPGA Getting Started

#### Introduction

Xilinx Domain Customized Computing.

#### Content

- 1.尝试着跑了一下Xilinx Vivado自带的例子，熟悉了FPGA的开发流程。代码见[tutorials目录](./tutorials)；
- 2.利用HLS实现了卷积层，并尝试着利用pragma去优化。代码和数据见目录[conv-layer-hls](./conv-layer-hls)和[conv-layer-hls-naive](./conv-layer-hls-naive).


实验平台：Xilinx PYNQ-Z2 Zynq 7020.

Note: 本repo的主要目的以学习为主，熟悉FPGA的开发和部署流程。

#### Refs

- <https://github.com/Xilinx/HLS-Tiny-Tutorials/>
- <https://github.com/xupgit/High-Level-Synthesis-Flow-on-Zynq-using-Vivado-HLS>
- <https://llvm.org/devmtg/2012-11/Hongbin-Generating.pdf>
- <http://vision.deis.unibo.it/~smatt/Seminars/Macloc_2017/Convolution_filters_HLS.pdf>
- <https://gitlab.cs.washington.edu/cse599s>
- <https://gitlab.cs.washington.edu/cse599s/hls-tutorials>
