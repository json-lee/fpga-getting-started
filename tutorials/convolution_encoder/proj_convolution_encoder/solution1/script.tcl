############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
############################################################
open_project proj_convolution_encoder
set_top convolution_encoder_top
add_files convolution_encoder.cpp
add_files -tb din.dat
add_files -tb dout.dat
add_files -tb convolution_encoder_tb.cpp
open_solution "solution1"
set_part {xc7k160tfbg484-1}
create_clock -period 4 -name default
#source "./proj_convolution_encoder/solution1/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -format ip_catalog
