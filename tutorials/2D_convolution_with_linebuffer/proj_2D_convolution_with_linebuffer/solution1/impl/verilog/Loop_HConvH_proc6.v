// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2018.3
// Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module Loop_HConvH_proc6 (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_continue,
        ap_idle,
        ap_ready,
        height_dout,
        height_empty_n,
        height_read,
        width_dout,
        width_empty_n,
        width_read,
        src_V_TDATA,
        src_V_TVALID,
        src_V_TREADY,
        hconv_V_din,
        hconv_V_full_n,
        hconv_V_write
);

parameter    ap_ST_fsm_state1 = 6'd1;
parameter    ap_ST_fsm_state2 = 6'd2;
parameter    ap_ST_fsm_state3 = 6'd4;
parameter    ap_ST_fsm_state4 = 6'd8;
parameter    ap_ST_fsm_pp0_stage0 = 6'd16;
parameter    ap_ST_fsm_state13 = 6'd32;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
input   ap_continue;
output   ap_idle;
output   ap_ready;
input  [31:0] height_dout;
input   height_empty_n;
output   height_read;
input  [31:0] width_dout;
input   width_empty_n;
output   width_read;
input  [31:0] src_V_TDATA;
input   src_V_TVALID;
output   src_V_TREADY;
output  [31:0] hconv_V_din;
input   hconv_V_full_n;
output   hconv_V_write;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg height_read;
reg width_read;
reg hconv_V_write;

reg    ap_done_reg;
(* fsm_encoding = "none" *) reg   [5:0] ap_CS_fsm;
wire    ap_CS_fsm_state1;
reg   [31:0] src_V_0_data_out;
wire    src_V_0_vld_in;
wire    src_V_0_vld_out;
wire    src_V_0_ack_in;
reg    src_V_0_ack_out;
reg   [31:0] src_V_0_payload_A;
reg   [31:0] src_V_0_payload_B;
reg    src_V_0_sel_rd;
reg    src_V_0_sel_wr;
wire    src_V_0_sel;
wire    src_V_0_load_A;
wire    src_V_0_load_B;
reg   [1:0] src_V_0_state;
wire    src_V_0_state_cmp_full;
reg    height_blk_n;
reg    width_blk_n;
reg    src_V_TDATA_blk_n;
wire    ap_CS_fsm_pp0_stage0;
reg    ap_enable_reg_pp0_iter0;
wire    ap_block_pp0_stage0;
wire   [0:0] exitcond_flatten_fu_210_p2;
reg    hconv_V_blk_n;
reg    ap_enable_reg_pp0_iter7;
reg   [0:0] tmp_10_i_reg_575;
reg   [0:0] tmp_10_i_reg_575_pp0_iter6_reg;
reg   [63:0] indvar_flatten_reg_137;
reg   [30:0] row_0_i_i_reg_148;
reg   [31:0] height_read_reg_483;
reg    ap_block_state1;
reg   [31:0] width_read_reg_488;
wire    ap_CS_fsm_state2;
wire   [63:0] grp_fu_165_p2;
reg   [63:0] bound_reg_504;
wire    ap_CS_fsm_state4;
reg   [31:0] hwin_1_1_i_load_reg_509;
reg    ap_block_state5_pp0_stage0_iter0;
wire    ap_block_state6_pp0_stage0_iter1;
wire    ap_block_state7_pp0_stage0_iter2;
wire    ap_block_state8_pp0_stage0_iter3;
wire    ap_block_state9_pp0_stage0_iter4;
wire    ap_block_state10_pp0_stage0_iter5;
wire    ap_block_state11_pp0_stage0_iter6;
reg    ap_block_state12_pp0_stage0_iter7;
reg    ap_block_pp0_stage0_11001;
reg   [31:0] hwin_1_1_i_load_reg_509_pp0_iter1_reg;
reg   [31:0] hwin_1_1_i_load_reg_509_pp0_iter2_reg;
reg  signed [31:0] hwin_3_load_reg_525;
reg  signed [31:0] hwin_3_load_reg_525_pp0_iter1_reg;
reg  signed [31:0] hwin_6_load_reg_540;
reg  signed [31:0] hwin_7_load_reg_545;
reg   [0:0] exitcond_flatten_reg_555;
reg   [0:0] exitcond_flatten_reg_555_pp0_iter1_reg;
reg   [0:0] exitcond_flatten_reg_555_pp0_iter2_reg;
reg   [0:0] exitcond_flatten_reg_555_pp0_iter3_reg;
reg   [0:0] exitcond_flatten_reg_555_pp0_iter4_reg;
reg   [0:0] exitcond_flatten_reg_555_pp0_iter5_reg;
wire   [63:0] indvar_flatten_next_fu_215_p2;
reg   [31:0] tmp_16_reg_569;
reg   [31:0] tmp_16_reg_569_pp0_iter1_reg;
reg   [31:0] tmp_16_reg_569_pp0_iter2_reg;
wire   [0:0] tmp_10_i_fu_313_p2;
reg   [0:0] tmp_10_i_reg_575_pp0_iter1_reg;
reg   [0:0] tmp_10_i_reg_575_pp0_iter2_reg;
reg   [0:0] tmp_10_i_reg_575_pp0_iter3_reg;
reg   [0:0] tmp_10_i_reg_575_pp0_iter4_reg;
reg   [0:0] tmp_10_i_reg_575_pp0_iter5_reg;
wire   [30:0] row_fu_324_p2;
wire   [31:0] grp_fu_277_p2;
reg   [31:0] tmp_23_1_i_reg_584;
wire   [31:0] grp_fu_283_p2;
reg   [31:0] tmp_23_2_i_reg_589;
wire   [31:0] grp_fu_289_p2;
reg   [31:0] tmp_23_4_i_reg_594;
wire   [31:0] grp_fu_295_p2;
reg   [31:0] tmp_23_5_i_reg_599;
wire   [31:0] grp_fu_301_p2;
reg   [31:0] tmp_23_8_i_reg_604;
wire   [31:0] grp_fu_307_p2;
reg   [31:0] tmp_23_9_i_reg_609;
wire   [31:0] grp_fu_330_p2;
reg   [31:0] tmp_23_6_i_reg_614;
wire   [31:0] grp_fu_335_p2;
reg   [31:0] tmp_23_7_i_reg_619;
wire   [31:0] tmp2_fu_371_p2;
reg   [31:0] tmp2_reg_624;
reg   [31:0] tmp2_reg_624_pp0_iter4_reg;
reg   [31:0] tmp2_reg_624_pp0_iter5_reg;
wire   [31:0] tmp4_fu_381_p2;
reg   [31:0] tmp4_reg_629;
reg   [31:0] tmp4_reg_629_pp0_iter4_reg;
reg   [31:0] tmp4_reg_629_pp0_iter5_reg;
wire   [31:0] tmp8_fu_387_p2;
reg   [31:0] tmp8_reg_634;
reg   [31:0] tmp8_reg_634_pp0_iter4_reg;
wire   [31:0] tmp11_fu_391_p2;
reg   [31:0] tmp11_reg_639;
wire   [31:0] grp_fu_340_p2;
reg   [31:0] tmp_23_3_i_reg_644;
wire   [31:0] tmp9_fu_399_p2;
reg   [31:0] tmp9_reg_649;
wire   [31:0] tmp6_fu_408_p2;
reg   [31:0] tmp6_reg_654;
wire   [31:0] tmp_4_fu_417_p2;
reg   [31:0] tmp_4_reg_659;
reg    ap_block_pp0_stage0_subdone;
reg    ap_condition_pp0_exit_iter0_state5;
reg    ap_enable_reg_pp0_iter1;
reg    ap_enable_reg_pp0_iter2;
reg    ap_enable_reg_pp0_iter3;
reg    ap_enable_reg_pp0_iter4;
reg    ap_enable_reg_pp0_iter5;
reg    ap_enable_reg_pp0_iter6;
reg    ap_block_pp0_stage0_01001;
reg   [31:0] hwin_1_1_i_fu_72;
reg  signed [31:0] hwin_1_fu_76;
reg  signed [31:0] hwin_2_fu_80;
reg   [31:0] hwin_3_fu_84;
reg  signed [31:0] hwin_4_fu_88;
reg  signed [31:0] hwin_5_fu_92;
reg   [31:0] hwin_6_fu_96;
reg   [31:0] hwin_7_fu_100;
reg  signed [31:0] hwin_8_fu_104;
reg  signed [31:0] hwin_9_fu_108;
wire   [31:0] grp_fu_165_p0;
wire   [31:0] grp_fu_165_p1;
wire   [31:0] row_0_i_cast_i_fu_201_p1;
wire   [0:0] tmp_4_i_fu_205_p2;
wire   [7:0] grp_fu_277_p0;
wire   [9:0] grp_fu_283_p0;
wire   [10:0] grp_fu_289_p0;
wire   [10:0] grp_fu_295_p0;
wire   [9:0] grp_fu_301_p0;
wire   [7:0] grp_fu_307_p0;
wire   [30:0] row_0_i_i_mid2_fu_269_p3;
wire   [10:0] grp_fu_330_p0;
wire   [9:0] grp_fu_335_p0;
wire   [9:0] grp_fu_340_p0;
wire   [31:0] tmp_14_fu_355_p2;
wire   [31:0] tmp_fu_345_p2;
wire   [31:0] tmp3_fu_365_p2;
wire   [31:0] tmp_15_fu_360_p2;
wire   [31:0] tmp5_fu_377_p2;
wire   [31:0] tmp_13_fu_350_p2;
wire   [31:0] tmp10_fu_395_p2;
wire   [31:0] tmp7_fu_404_p2;
wire   [31:0] tmp1_fu_413_p2;
reg    grp_fu_277_ce;
reg    grp_fu_283_ce;
reg    grp_fu_289_ce;
reg    grp_fu_295_ce;
reg    grp_fu_301_ce;
reg    grp_fu_307_ce;
reg    grp_fu_330_ce;
reg    grp_fu_335_ce;
reg    grp_fu_340_ce;
wire    ap_CS_fsm_state13;
reg   [5:0] ap_NS_fsm;
reg    ap_idle_pp0;
wire    ap_enable_pp0;
wire   [63:0] grp_fu_165_p00;
wire   [63:0] grp_fu_165_p10;

// power-on initialization
initial begin
#0 ap_done_reg = 1'b0;
#0 ap_CS_fsm = 6'd1;
#0 src_V_0_sel_rd = 1'b0;
#0 src_V_0_sel_wr = 1'b0;
#0 src_V_0_state = 2'd0;
#0 ap_enable_reg_pp0_iter0 = 1'b0;
#0 ap_enable_reg_pp0_iter7 = 1'b0;
#0 ap_enable_reg_pp0_iter1 = 1'b0;
#0 ap_enable_reg_pp0_iter2 = 1'b0;
#0 ap_enable_reg_pp0_iter3 = 1'b0;
#0 ap_enable_reg_pp0_iter4 = 1'b0;
#0 ap_enable_reg_pp0_iter5 = 1'b0;
#0 ap_enable_reg_pp0_iter6 = 1'b0;
end

filter11x11_strm_bkb #(
    .ID( 1 ),
    .NUM_STAGE( 3 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 64 ))
filter11x11_strm_bkb_U10(
    .clk(ap_clk),
    .reset(ap_rst),
    .din0(grp_fu_165_p0),
    .din1(grp_fu_165_p1),
    .ce(1'b1),
    .dout(grp_fu_165_p2)
);

filter11x11_strm_cud #(
    .ID( 1 ),
    .NUM_STAGE( 3 ),
    .din0_WIDTH( 8 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
filter11x11_strm_cud_U11(
    .clk(ap_clk),
    .reset(ap_rst),
    .din0(grp_fu_277_p0),
    .din1(hwin_1_fu_76),
    .ce(grp_fu_277_ce),
    .dout(grp_fu_277_p2)
);

filter11x11_strm_dEe #(
    .ID( 1 ),
    .NUM_STAGE( 3 ),
    .din0_WIDTH( 10 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
filter11x11_strm_dEe_U12(
    .clk(ap_clk),
    .reset(ap_rst),
    .din0(grp_fu_283_p0),
    .din1(hwin_2_fu_80),
    .ce(grp_fu_283_ce),
    .dout(grp_fu_283_p2)
);

filter11x11_strm_eOg #(
    .ID( 1 ),
    .NUM_STAGE( 3 ),
    .din0_WIDTH( 11 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
filter11x11_strm_eOg_U13(
    .clk(ap_clk),
    .reset(ap_rst),
    .din0(grp_fu_289_p0),
    .din1(hwin_4_fu_88),
    .ce(grp_fu_289_ce),
    .dout(grp_fu_289_p2)
);

filter11x11_strm_eOg #(
    .ID( 1 ),
    .NUM_STAGE( 3 ),
    .din0_WIDTH( 11 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
filter11x11_strm_eOg_U14(
    .clk(ap_clk),
    .reset(ap_rst),
    .din0(grp_fu_295_p0),
    .din1(hwin_5_fu_92),
    .ce(grp_fu_295_ce),
    .dout(grp_fu_295_p2)
);

filter11x11_strm_dEe #(
    .ID( 1 ),
    .NUM_STAGE( 3 ),
    .din0_WIDTH( 10 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
filter11x11_strm_dEe_U15(
    .clk(ap_clk),
    .reset(ap_rst),
    .din0(grp_fu_301_p0),
    .din1(hwin_8_fu_104),
    .ce(grp_fu_301_ce),
    .dout(grp_fu_301_p2)
);

filter11x11_strm_cud #(
    .ID( 1 ),
    .NUM_STAGE( 3 ),
    .din0_WIDTH( 8 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
filter11x11_strm_cud_U16(
    .clk(ap_clk),
    .reset(ap_rst),
    .din0(grp_fu_307_p0),
    .din1(hwin_9_fu_108),
    .ce(grp_fu_307_ce),
    .dout(grp_fu_307_p2)
);

filter11x11_strm_eOg #(
    .ID( 1 ),
    .NUM_STAGE( 3 ),
    .din0_WIDTH( 11 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
filter11x11_strm_eOg_U17(
    .clk(ap_clk),
    .reset(ap_rst),
    .din0(grp_fu_330_p0),
    .din1(hwin_6_load_reg_540),
    .ce(grp_fu_330_ce),
    .dout(grp_fu_330_p2)
);

filter11x11_strm_dEe #(
    .ID( 1 ),
    .NUM_STAGE( 3 ),
    .din0_WIDTH( 10 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
filter11x11_strm_dEe_U18(
    .clk(ap_clk),
    .reset(ap_rst),
    .din0(grp_fu_335_p0),
    .din1(hwin_7_load_reg_545),
    .ce(grp_fu_335_ce),
    .dout(grp_fu_335_p2)
);

filter11x11_strm_dEe #(
    .ID( 1 ),
    .NUM_STAGE( 3 ),
    .din0_WIDTH( 10 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
filter11x11_strm_dEe_U19(
    .clk(ap_clk),
    .reset(ap_rst),
    .din0(grp_fu_340_p0),
    .din1(hwin_3_load_reg_525_pp0_iter1_reg),
    .ce(grp_fu_340_ce),
    .dout(grp_fu_340_p2)
);

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_fsm_state1;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_done_reg <= 1'b0;
    end else begin
        if ((ap_continue == 1'b1)) begin
            ap_done_reg <= 1'b0;
        end else if ((1'b1 == ap_CS_fsm_state13)) begin
            ap_done_reg <= 1'b1;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter0 <= 1'b0;
    end else begin
        if (((1'b0 == ap_block_pp0_stage0_subdone) & (1'b1 == ap_condition_pp0_exit_iter0_state5) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
            ap_enable_reg_pp0_iter0 <= 1'b0;
        end else if ((1'b1 == ap_CS_fsm_state4)) begin
            ap_enable_reg_pp0_iter0 <= 1'b1;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter1 <= 1'b0;
    end else begin
        if ((1'b0 == ap_block_pp0_stage0_subdone)) begin
            if ((1'b1 == ap_condition_pp0_exit_iter0_state5)) begin
                ap_enable_reg_pp0_iter1 <= (1'b1 ^ ap_condition_pp0_exit_iter0_state5);
            end else if ((1'b1 == 1'b1)) begin
                ap_enable_reg_pp0_iter1 <= ap_enable_reg_pp0_iter0;
            end
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter2 <= 1'b0;
    end else begin
        if ((1'b0 == ap_block_pp0_stage0_subdone)) begin
            ap_enable_reg_pp0_iter2 <= ap_enable_reg_pp0_iter1;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter3 <= 1'b0;
    end else begin
        if ((1'b0 == ap_block_pp0_stage0_subdone)) begin
            ap_enable_reg_pp0_iter3 <= ap_enable_reg_pp0_iter2;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter4 <= 1'b0;
    end else begin
        if ((1'b0 == ap_block_pp0_stage0_subdone)) begin
            ap_enable_reg_pp0_iter4 <= ap_enable_reg_pp0_iter3;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter5 <= 1'b0;
    end else begin
        if ((1'b0 == ap_block_pp0_stage0_subdone)) begin
            ap_enable_reg_pp0_iter5 <= ap_enable_reg_pp0_iter4;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter6 <= 1'b0;
    end else begin
        if ((1'b0 == ap_block_pp0_stage0_subdone)) begin
            ap_enable_reg_pp0_iter6 <= ap_enable_reg_pp0_iter5;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter7 <= 1'b0;
    end else begin
        if ((1'b0 == ap_block_pp0_stage0_subdone)) begin
            ap_enable_reg_pp0_iter7 <= ap_enable_reg_pp0_iter6;
        end else if ((1'b1 == ap_CS_fsm_state4)) begin
            ap_enable_reg_pp0_iter7 <= 1'b0;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        src_V_0_sel_rd <= 1'b0;
    end else begin
        if (((src_V_0_ack_out == 1'b1) & (src_V_0_vld_out == 1'b1))) begin
            src_V_0_sel_rd <= ~src_V_0_sel_rd;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        src_V_0_sel_wr <= 1'b0;
    end else begin
        if (((src_V_0_ack_in == 1'b1) & (src_V_0_vld_in == 1'b1))) begin
            src_V_0_sel_wr <= ~src_V_0_sel_wr;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        src_V_0_state <= 2'd0;
    end else begin
        if ((((src_V_0_state == 2'd2) & (src_V_0_vld_in == 1'b0)) | ((src_V_0_state == 2'd3) & (src_V_0_vld_in == 1'b0) & (src_V_0_ack_out == 1'b1)))) begin
            src_V_0_state <= 2'd2;
        end else if ((((src_V_0_state == 2'd1) & (src_V_0_ack_out == 1'b0)) | ((src_V_0_state == 2'd3) & (src_V_0_ack_out == 1'b0) & (src_V_0_vld_in == 1'b1)))) begin
            src_V_0_state <= 2'd1;
        end else if (((~((src_V_0_vld_in == 1'b0) & (src_V_0_ack_out == 1'b1)) & ~((src_V_0_ack_out == 1'b0) & (src_V_0_vld_in == 1'b1)) & (src_V_0_state == 2'd3)) | ((src_V_0_state == 2'd1) & (src_V_0_ack_out == 1'b1)) | ((src_V_0_state == 2'd2) & (src_V_0_vld_in == 1'b1)))) begin
            src_V_0_state <= 2'd3;
        end else begin
            src_V_0_state <= 2'd2;
        end
    end
end

always @ (posedge ap_clk) begin
    if (((exitcond_flatten_fu_210_p2 == 1'd0) & (ap_enable_reg_pp0_iter0 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        indvar_flatten_reg_137 <= indvar_flatten_next_fu_215_p2;
    end else if ((1'b1 == ap_CS_fsm_state4)) begin
        indvar_flatten_reg_137 <= 64'd0;
    end
end

always @ (posedge ap_clk) begin
    if (((exitcond_flatten_fu_210_p2 == 1'd0) & (ap_enable_reg_pp0_iter0 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        row_0_i_i_reg_148 <= row_fu_324_p2;
    end else if ((1'b1 == ap_CS_fsm_state4)) begin
        row_0_i_i_reg_148 <= 31'd0;
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_CS_fsm_state4)) begin
        bound_reg_504 <= grp_fu_165_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        exitcond_flatten_reg_555 <= exitcond_flatten_fu_210_p2;
        exitcond_flatten_reg_555_pp0_iter1_reg <= exitcond_flatten_reg_555;
        hwin_1_1_i_load_reg_509 <= hwin_1_1_i_fu_72;
        hwin_1_1_i_load_reg_509_pp0_iter1_reg <= hwin_1_1_i_load_reg_509;
        hwin_3_load_reg_525 <= hwin_3_fu_84;
        hwin_3_load_reg_525_pp0_iter1_reg <= hwin_3_load_reg_525;
        hwin_6_load_reg_540 <= hwin_6_fu_96;
        hwin_7_load_reg_545 <= hwin_7_fu_100;
        tmp_10_i_reg_575_pp0_iter1_reg <= tmp_10_i_reg_575;
        tmp_16_reg_569_pp0_iter1_reg <= tmp_16_reg_569;
    end
end

always @ (posedge ap_clk) begin
    if ((1'b0 == ap_block_pp0_stage0_11001)) begin
        exitcond_flatten_reg_555_pp0_iter2_reg <= exitcond_flatten_reg_555_pp0_iter1_reg;
        exitcond_flatten_reg_555_pp0_iter3_reg <= exitcond_flatten_reg_555_pp0_iter2_reg;
        exitcond_flatten_reg_555_pp0_iter4_reg <= exitcond_flatten_reg_555_pp0_iter3_reg;
        exitcond_flatten_reg_555_pp0_iter5_reg <= exitcond_flatten_reg_555_pp0_iter4_reg;
        hwin_1_1_i_load_reg_509_pp0_iter2_reg <= hwin_1_1_i_load_reg_509_pp0_iter1_reg;
        tmp2_reg_624_pp0_iter4_reg[31 : 2] <= tmp2_reg_624[31 : 2];
        tmp2_reg_624_pp0_iter5_reg[31 : 2] <= tmp2_reg_624_pp0_iter4_reg[31 : 2];
        tmp4_reg_629_pp0_iter4_reg <= tmp4_reg_629;
        tmp4_reg_629_pp0_iter5_reg <= tmp4_reg_629_pp0_iter4_reg;
        tmp8_reg_634_pp0_iter4_reg <= tmp8_reg_634;
        tmp_10_i_reg_575_pp0_iter2_reg <= tmp_10_i_reg_575_pp0_iter1_reg;
        tmp_10_i_reg_575_pp0_iter3_reg <= tmp_10_i_reg_575_pp0_iter2_reg;
        tmp_10_i_reg_575_pp0_iter4_reg <= tmp_10_i_reg_575_pp0_iter3_reg;
        tmp_10_i_reg_575_pp0_iter5_reg <= tmp_10_i_reg_575_pp0_iter4_reg;
        tmp_10_i_reg_575_pp0_iter6_reg <= tmp_10_i_reg_575_pp0_iter5_reg;
        tmp_16_reg_569_pp0_iter2_reg <= tmp_16_reg_569_pp0_iter1_reg;
    end
end

always @ (posedge ap_clk) begin
    if ((~((width_empty_n == 1'b0) | (height_empty_n == 1'b0) | (ap_start == 1'b0) | (ap_done_reg == 1'b1)) & (1'b1 == ap_CS_fsm_state1))) begin
        height_read_reg_483 <= height_dout;
        width_read_reg_488 <= width_dout;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_enable_reg_pp0_iter0 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        hwin_1_1_i_fu_72 <= hwin_1_fu_76;
        hwin_1_fu_76 <= hwin_2_fu_80;
        hwin_2_fu_80 <= hwin_3_fu_84;
        hwin_3_fu_84 <= hwin_4_fu_88;
        hwin_4_fu_88 <= hwin_5_fu_92;
        hwin_5_fu_92 <= hwin_6_fu_96;
        hwin_6_fu_96 <= hwin_7_fu_100;
        hwin_7_fu_100 <= hwin_8_fu_104;
        hwin_8_fu_104 <= hwin_9_fu_108;
    end
end

always @ (posedge ap_clk) begin
    if (((exitcond_flatten_fu_210_p2 == 1'd0) & (ap_enable_reg_pp0_iter0 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        hwin_9_fu_108 <= src_V_0_data_out;
    end
end

always @ (posedge ap_clk) begin
    if ((src_V_0_load_A == 1'b1)) begin
        src_V_0_payload_A <= src_V_TDATA;
    end
end

always @ (posedge ap_clk) begin
    if ((src_V_0_load_B == 1'b1)) begin
        src_V_0_payload_B <= src_V_TDATA;
    end
end

always @ (posedge ap_clk) begin
    if (((exitcond_flatten_reg_555_pp0_iter2_reg == 1'd0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        tmp11_reg_639 <= tmp11_fu_391_p2;
        tmp2_reg_624[31 : 2] <= tmp2_fu_371_p2[31 : 2];
        tmp4_reg_629 <= tmp4_fu_381_p2;
        tmp8_reg_634 <= tmp8_fu_387_p2;
        tmp_23_6_i_reg_614 <= grp_fu_330_p2;
        tmp_23_7_i_reg_619 <= grp_fu_335_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((exitcond_flatten_reg_555_pp0_iter4_reg == 1'd0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        tmp6_reg_654 <= tmp6_fu_408_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((exitcond_flatten_reg_555_pp0_iter3_reg == 1'd0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        tmp9_reg_649 <= tmp9_fu_399_p2;
        tmp_23_3_i_reg_644 <= grp_fu_340_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((exitcond_flatten_fu_210_p2 == 1'd0) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        tmp_10_i_reg_575 <= tmp_10_i_fu_313_p2;
        tmp_16_reg_569 <= src_V_0_data_out;
    end
end

always @ (posedge ap_clk) begin
    if (((exitcond_flatten_reg_555_pp0_iter1_reg == 1'd0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        tmp_23_1_i_reg_584 <= grp_fu_277_p2;
        tmp_23_2_i_reg_589 <= grp_fu_283_p2;
        tmp_23_4_i_reg_594 <= grp_fu_289_p2;
        tmp_23_5_i_reg_599 <= grp_fu_295_p2;
        tmp_23_8_i_reg_604 <= grp_fu_301_p2;
        tmp_23_9_i_reg_609 <= grp_fu_307_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((exitcond_flatten_reg_555_pp0_iter5_reg == 1'd0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        tmp_4_reg_659 <= tmp_4_fu_417_p2;
    end
end

always @ (*) begin
    if ((exitcond_flatten_fu_210_p2 == 1'd1)) begin
        ap_condition_pp0_exit_iter0_state5 = 1'b1;
    end else begin
        ap_condition_pp0_exit_iter0_state5 = 1'b0;
    end
end

always @ (*) begin
    if ((1'b1 == ap_CS_fsm_state13)) begin
        ap_done = 1'b1;
    end else begin
        ap_done = ap_done_reg;
    end
end

always @ (*) begin
    if (((ap_start == 1'b0) & (1'b1 == ap_CS_fsm_state1))) begin
        ap_idle = 1'b1;
    end else begin
        ap_idle = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter6 == 1'b0) & (ap_enable_reg_pp0_iter5 == 1'b0) & (ap_enable_reg_pp0_iter4 == 1'b0) & (ap_enable_reg_pp0_iter3 == 1'b0) & (ap_enable_reg_pp0_iter2 == 1'b0) & (ap_enable_reg_pp0_iter1 == 1'b0) & (ap_enable_reg_pp0_iter7 == 1'b0) & (ap_enable_reg_pp0_iter0 == 1'b0))) begin
        ap_idle_pp0 = 1'b1;
    end else begin
        ap_idle_pp0 = 1'b0;
    end
end

always @ (*) begin
    if ((1'b1 == ap_CS_fsm_state13)) begin
        ap_ready = 1'b1;
    end else begin
        ap_ready = 1'b0;
    end
end

always @ (*) begin
    if (((1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        grp_fu_277_ce = 1'b1;
    end else begin
        grp_fu_277_ce = 1'b0;
    end
end

always @ (*) begin
    if (((1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        grp_fu_283_ce = 1'b1;
    end else begin
        grp_fu_283_ce = 1'b0;
    end
end

always @ (*) begin
    if (((1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        grp_fu_289_ce = 1'b1;
    end else begin
        grp_fu_289_ce = 1'b0;
    end
end

always @ (*) begin
    if (((1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        grp_fu_295_ce = 1'b1;
    end else begin
        grp_fu_295_ce = 1'b0;
    end
end

always @ (*) begin
    if (((1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        grp_fu_301_ce = 1'b1;
    end else begin
        grp_fu_301_ce = 1'b0;
    end
end

always @ (*) begin
    if (((1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        grp_fu_307_ce = 1'b1;
    end else begin
        grp_fu_307_ce = 1'b0;
    end
end

always @ (*) begin
    if (((1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        grp_fu_330_ce = 1'b1;
    end else begin
        grp_fu_330_ce = 1'b0;
    end
end

always @ (*) begin
    if (((1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        grp_fu_335_ce = 1'b1;
    end else begin
        grp_fu_335_ce = 1'b0;
    end
end

always @ (*) begin
    if (((1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        grp_fu_340_ce = 1'b1;
    end else begin
        grp_fu_340_ce = 1'b0;
    end
end

always @ (*) begin
    if (((tmp_10_i_reg_575_pp0_iter6_reg == 1'd1) & (ap_enable_reg_pp0_iter7 == 1'b1) & (1'b0 == ap_block_pp0_stage0))) begin
        hconv_V_blk_n = hconv_V_full_n;
    end else begin
        hconv_V_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((tmp_10_i_reg_575_pp0_iter6_reg == 1'd1) & (ap_enable_reg_pp0_iter7 == 1'b1) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        hconv_V_write = 1'b1;
    end else begin
        hconv_V_write = 1'b0;
    end
end

always @ (*) begin
    if ((~((ap_start == 1'b0) | (ap_done_reg == 1'b1)) & (1'b1 == ap_CS_fsm_state1))) begin
        height_blk_n = height_empty_n;
    end else begin
        height_blk_n = 1'b1;
    end
end

always @ (*) begin
    if ((~((width_empty_n == 1'b0) | (height_empty_n == 1'b0) | (ap_start == 1'b0) | (ap_done_reg == 1'b1)) & (1'b1 == ap_CS_fsm_state1))) begin
        height_read = 1'b1;
    end else begin
        height_read = 1'b0;
    end
end

always @ (*) begin
    if (((exitcond_flatten_fu_210_p2 == 1'd0) & (ap_enable_reg_pp0_iter0 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        src_V_0_ack_out = 1'b1;
    end else begin
        src_V_0_ack_out = 1'b0;
    end
end

always @ (*) begin
    if ((src_V_0_sel == 1'b1)) begin
        src_V_0_data_out = src_V_0_payload_B;
    end else begin
        src_V_0_data_out = src_V_0_payload_A;
    end
end

always @ (*) begin
    if (((exitcond_flatten_fu_210_p2 == 1'd0) & (ap_enable_reg_pp0_iter0 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0))) begin
        src_V_TDATA_blk_n = src_V_0_state[1'd0];
    end else begin
        src_V_TDATA_blk_n = 1'b1;
    end
end

always @ (*) begin
    if ((~((ap_start == 1'b0) | (ap_done_reg == 1'b1)) & (1'b1 == ap_CS_fsm_state1))) begin
        width_blk_n = width_empty_n;
    end else begin
        width_blk_n = 1'b1;
    end
end

always @ (*) begin
    if ((~((width_empty_n == 1'b0) | (height_empty_n == 1'b0) | (ap_start == 1'b0) | (ap_done_reg == 1'b1)) & (1'b1 == ap_CS_fsm_state1))) begin
        width_read = 1'b1;
    end else begin
        width_read = 1'b0;
    end
end

always @ (*) begin
    case (ap_CS_fsm)
        ap_ST_fsm_state1 : begin
            if ((~((width_empty_n == 1'b0) | (height_empty_n == 1'b0) | (ap_start == 1'b0) | (ap_done_reg == 1'b1)) & (1'b1 == ap_CS_fsm_state1))) begin
                ap_NS_fsm = ap_ST_fsm_state2;
            end else begin
                ap_NS_fsm = ap_ST_fsm_state1;
            end
        end
        ap_ST_fsm_state2 : begin
            ap_NS_fsm = ap_ST_fsm_state3;
        end
        ap_ST_fsm_state3 : begin
            ap_NS_fsm = ap_ST_fsm_state4;
        end
        ap_ST_fsm_state4 : begin
            ap_NS_fsm = ap_ST_fsm_pp0_stage0;
        end
        ap_ST_fsm_pp0_stage0 : begin
            if ((~((exitcond_flatten_fu_210_p2 == 1'd1) & (ap_enable_reg_pp0_iter1 == 1'b0) & (1'b0 == ap_block_pp0_stage0_subdone) & (ap_enable_reg_pp0_iter0 == 1'b1)) & ~((ap_enable_reg_pp0_iter6 == 1'b0) & (1'b0 == ap_block_pp0_stage0_subdone) & (ap_enable_reg_pp0_iter7 == 1'b1)))) begin
                ap_NS_fsm = ap_ST_fsm_pp0_stage0;
            end else if ((((ap_enable_reg_pp0_iter6 == 1'b0) & (1'b0 == ap_block_pp0_stage0_subdone) & (ap_enable_reg_pp0_iter7 == 1'b1)) | ((exitcond_flatten_fu_210_p2 == 1'd1) & (ap_enable_reg_pp0_iter1 == 1'b0) & (1'b0 == ap_block_pp0_stage0_subdone) & (ap_enable_reg_pp0_iter0 == 1'b1)))) begin
                ap_NS_fsm = ap_ST_fsm_state13;
            end else begin
                ap_NS_fsm = ap_ST_fsm_pp0_stage0;
            end
        end
        ap_ST_fsm_state13 : begin
            ap_NS_fsm = ap_ST_fsm_state1;
        end
        default : begin
            ap_NS_fsm = 'bx;
        end
    endcase
end

assign ap_CS_fsm_pp0_stage0 = ap_CS_fsm[32'd4];

assign ap_CS_fsm_state1 = ap_CS_fsm[32'd0];

assign ap_CS_fsm_state13 = ap_CS_fsm[32'd5];

assign ap_CS_fsm_state2 = ap_CS_fsm[32'd1];

assign ap_CS_fsm_state4 = ap_CS_fsm[32'd3];

assign ap_block_pp0_stage0 = ~(1'b1 == 1'b1);

always @ (*) begin
    ap_block_pp0_stage0_01001 = (((tmp_10_i_reg_575_pp0_iter6_reg == 1'd1) & (hconv_V_full_n == 1'b0) & (ap_enable_reg_pp0_iter7 == 1'b1)) | ((exitcond_flatten_fu_210_p2 == 1'd0) & (src_V_0_vld_out == 1'b0) & (ap_enable_reg_pp0_iter0 == 1'b1)));
end

always @ (*) begin
    ap_block_pp0_stage0_11001 = (((tmp_10_i_reg_575_pp0_iter6_reg == 1'd1) & (hconv_V_full_n == 1'b0) & (ap_enable_reg_pp0_iter7 == 1'b1)) | ((exitcond_flatten_fu_210_p2 == 1'd0) & (src_V_0_vld_out == 1'b0) & (ap_enable_reg_pp0_iter0 == 1'b1)));
end

always @ (*) begin
    ap_block_pp0_stage0_subdone = (((tmp_10_i_reg_575_pp0_iter6_reg == 1'd1) & (hconv_V_full_n == 1'b0) & (ap_enable_reg_pp0_iter7 == 1'b1)) | ((exitcond_flatten_fu_210_p2 == 1'd0) & (src_V_0_vld_out == 1'b0) & (ap_enable_reg_pp0_iter0 == 1'b1)));
end

always @ (*) begin
    ap_block_state1 = ((width_empty_n == 1'b0) | (height_empty_n == 1'b0) | (ap_start == 1'b0) | (ap_done_reg == 1'b1));
end

assign ap_block_state10_pp0_stage0_iter5 = ~(1'b1 == 1'b1);

assign ap_block_state11_pp0_stage0_iter6 = ~(1'b1 == 1'b1);

always @ (*) begin
    ap_block_state12_pp0_stage0_iter7 = ((tmp_10_i_reg_575_pp0_iter6_reg == 1'd1) & (hconv_V_full_n == 1'b0));
end

always @ (*) begin
    ap_block_state5_pp0_stage0_iter0 = ((exitcond_flatten_fu_210_p2 == 1'd0) & (src_V_0_vld_out == 1'b0));
end

assign ap_block_state6_pp0_stage0_iter1 = ~(1'b1 == 1'b1);

assign ap_block_state7_pp0_stage0_iter2 = ~(1'b1 == 1'b1);

assign ap_block_state8_pp0_stage0_iter3 = ~(1'b1 == 1'b1);

assign ap_block_state9_pp0_stage0_iter4 = ~(1'b1 == 1'b1);

assign ap_enable_pp0 = (ap_idle_pp0 ^ 1'b1);

assign exitcond_flatten_fu_210_p2 = ((indvar_flatten_reg_137 == bound_reg_504) ? 1'b1 : 1'b0);

assign grp_fu_165_p0 = grp_fu_165_p00;

assign grp_fu_165_p00 = width_read_reg_488;

assign grp_fu_165_p1 = grp_fu_165_p10;

assign grp_fu_165_p10 = height_read_reg_483;

assign grp_fu_277_p0 = 32'd111;

assign grp_fu_283_p0 = 32'd266;

assign grp_fu_289_p0 = 32'd724;

assign grp_fu_295_p0 = 32'd821;

assign grp_fu_301_p0 = 32'd266;

assign grp_fu_307_p0 = 32'd111;

assign grp_fu_330_p0 = 32'd724;

assign grp_fu_335_p0 = 32'd498;

assign grp_fu_340_p0 = 32'd498;

assign hconv_V_din = tmp_4_reg_659;

assign indvar_flatten_next_fu_215_p2 = (indvar_flatten_reg_137 + 64'd1);

assign row_0_i_cast_i_fu_201_p1 = row_0_i_i_reg_148;

assign row_0_i_i_mid2_fu_269_p3 = ((tmp_4_i_fu_205_p2[0:0] === 1'b1) ? row_0_i_i_reg_148 : 31'd0);

assign row_fu_324_p2 = (row_0_i_i_mid2_fu_269_p3 + 31'd1);

assign src_V_0_ack_in = src_V_0_state[1'd1];

assign src_V_0_load_A = (src_V_0_state_cmp_full & ~src_V_0_sel_wr);

assign src_V_0_load_B = (src_V_0_state_cmp_full & src_V_0_sel_wr);

assign src_V_0_sel = src_V_0_sel_rd;

assign src_V_0_state_cmp_full = ((src_V_0_state != 2'd1) ? 1'b1 : 1'b0);

assign src_V_0_vld_in = src_V_TVALID;

assign src_V_0_vld_out = src_V_0_state[1'd0];

assign src_V_TREADY = src_V_0_state[1'd1];

assign tmp10_fu_395_p2 = (tmp_23_6_i_reg_614 + tmp_23_7_i_reg_619);

assign tmp11_fu_391_p2 = (tmp_23_8_i_reg_604 + tmp_23_9_i_reg_609);

assign tmp1_fu_413_p2 = (tmp4_reg_629_pp0_iter5_reg + tmp2_reg_624_pp0_iter5_reg);

assign tmp2_fu_371_p2 = (tmp3_fu_365_p2 + tmp_15_fu_360_p2);

assign tmp3_fu_365_p2 = (tmp_14_fu_355_p2 + tmp_fu_345_p2);

assign tmp4_fu_381_p2 = (tmp5_fu_377_p2 + tmp_13_fu_350_p2);

assign tmp5_fu_377_p2 = (tmp_23_1_i_reg_584 + tmp_23_2_i_reg_589);

assign tmp6_fu_408_p2 = (tmp9_reg_649 + tmp7_fu_404_p2);

assign tmp7_fu_404_p2 = (tmp8_reg_634_pp0_iter4_reg + tmp_23_3_i_reg_644);

assign tmp8_fu_387_p2 = (tmp_23_4_i_reg_594 + tmp_23_5_i_reg_599);

assign tmp9_fu_399_p2 = (tmp11_reg_639 + tmp10_fu_395_p2);

assign tmp_10_i_fu_313_p2 = ((row_0_i_i_mid2_fu_269_p3 > 31'd9) ? 1'b1 : 1'b0);

assign tmp_13_fu_350_p2 = hwin_1_1_i_load_reg_509_pp0_iter2_reg << 32'd2;

assign tmp_14_fu_355_p2 = tmp_16_reg_569_pp0_iter2_reg << 32'd5;

assign tmp_15_fu_360_p2 = tmp_16_reg_569_pp0_iter2_reg << 32'd2;

assign tmp_4_fu_417_p2 = (tmp6_reg_654 + tmp1_fu_413_p2);

assign tmp_4_i_fu_205_p2 = (($signed(row_0_i_cast_i_fu_201_p1) < $signed(width_read_reg_488)) ? 1'b1 : 1'b0);

assign tmp_fu_345_p2 = hwin_1_1_i_load_reg_509_pp0_iter2_reg << 32'd5;

always @ (posedge ap_clk) begin
    tmp2_reg_624[1:0] <= 2'b00;
    tmp2_reg_624_pp0_iter4_reg[1:0] <= 2'b00;
    tmp2_reg_624_pp0_iter5_reg[1:0] <= 2'b00;
end

endmodule //Loop_HConvH_proc6
