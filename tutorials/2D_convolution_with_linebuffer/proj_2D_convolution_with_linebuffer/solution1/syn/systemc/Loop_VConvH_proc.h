// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2018.3
// Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _Loop_VConvH_proc_HH_
#define _Loop_VConvH_proc_HH_

#include "systemc.h"
#include "AESL_pkg.h"

#include "filter11x11_strm_bkb.h"
#include "filter11x11_strm_cud.h"
#include "filter11x11_strm_dEe.h"
#include "filter11x11_strm_eOg.h"
#include "Loop_VConvH_proc_fYi.h"

namespace ap_rtl {

struct Loop_VConvH_proc : public sc_module {
    // Port declarations 25
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<32> > height_dout;
    sc_in< sc_logic > height_empty_n;
    sc_out< sc_logic > height_read;
    sc_in< sc_lv<32> > vconv_xlim_loc_dout;
    sc_in< sc_logic > vconv_xlim_loc_empty_n;
    sc_out< sc_logic > vconv_xlim_loc_read;
    sc_in< sc_lv<32> > hconv_V_dout;
    sc_in< sc_logic > hconv_V_empty_n;
    sc_out< sc_logic > hconv_V_read;
    sc_out< sc_lv<32> > vconv_V_din;
    sc_in< sc_logic > vconv_V_full_n;
    sc_out< sc_logic > vconv_V_write;
    sc_out< sc_lv<32> > height_out_din;
    sc_in< sc_logic > height_out_full_n;
    sc_out< sc_logic > height_out_write;
    sc_out< sc_lv<32> > vconv_xlim_loc_out_din;
    sc_in< sc_logic > vconv_xlim_loc_out_full_n;
    sc_out< sc_logic > vconv_xlim_loc_out_write;
    sc_signal< sc_logic > ap_var_for_const0;


    // Module declarations
    Loop_VConvH_proc(sc_module_name name);
    SC_HAS_PROCESS(Loop_VConvH_proc);

    ~Loop_VConvH_proc();

    sc_trace_file* mVcdFile;

    Loop_VConvH_proc_fYi* linebuf_0_U;
    Loop_VConvH_proc_fYi* linebuf_1_U;
    Loop_VConvH_proc_fYi* linebuf_2_U;
    Loop_VConvH_proc_fYi* linebuf_3_U;
    Loop_VConvH_proc_fYi* linebuf_4_U;
    Loop_VConvH_proc_fYi* linebuf_5_U;
    Loop_VConvH_proc_fYi* linebuf_6_U;
    Loop_VConvH_proc_fYi* linebuf_7_U;
    Loop_VConvH_proc_fYi* linebuf_8_U;
    Loop_VConvH_proc_fYi* linebuf_9_U;
    filter11x11_strm_bkb<1,3,32,32,64>* filter11x11_strm_bkb_U28;
    filter11x11_strm_cud<1,3,8,32,32>* filter11x11_strm_cud_U29;
    filter11x11_strm_dEe<1,3,10,32,32>* filter11x11_strm_dEe_U30;
    filter11x11_strm_eOg<1,3,11,32,32>* filter11x11_strm_eOg_U31;
    filter11x11_strm_eOg<1,3,11,32,32>* filter11x11_strm_eOg_U32;
    filter11x11_strm_dEe<1,3,10,32,32>* filter11x11_strm_dEe_U33;
    filter11x11_strm_cud<1,3,8,32,32>* filter11x11_strm_cud_U34;
    filter11x11_strm_eOg<1,3,11,32,32>* filter11x11_strm_eOg_U35;
    filter11x11_strm_dEe<1,3,10,32,32>* filter11x11_strm_dEe_U36;
    filter11x11_strm_dEe<1,3,10,32,32>* filter11x11_strm_dEe_U37;
    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<6> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_state1;
    sc_signal< sc_lv<11> > linebuf_0_address0;
    sc_signal< sc_logic > linebuf_0_ce0;
    sc_signal< sc_lv<32> > linebuf_0_q0;
    sc_signal< sc_lv<11> > linebuf_0_address1;
    sc_signal< sc_logic > linebuf_0_ce1;
    sc_signal< sc_logic > linebuf_0_we1;
    sc_signal< sc_lv<11> > linebuf_1_address0;
    sc_signal< sc_logic > linebuf_1_ce0;
    sc_signal< sc_lv<32> > linebuf_1_q0;
    sc_signal< sc_lv<11> > linebuf_1_address1;
    sc_signal< sc_logic > linebuf_1_ce1;
    sc_signal< sc_logic > linebuf_1_we1;
    sc_signal< sc_lv<11> > linebuf_2_address0;
    sc_signal< sc_logic > linebuf_2_ce0;
    sc_signal< sc_lv<32> > linebuf_2_q0;
    sc_signal< sc_lv<11> > linebuf_2_address1;
    sc_signal< sc_logic > linebuf_2_ce1;
    sc_signal< sc_logic > linebuf_2_we1;
    sc_signal< sc_lv<11> > linebuf_3_address0;
    sc_signal< sc_logic > linebuf_3_ce0;
    sc_signal< sc_lv<32> > linebuf_3_q0;
    sc_signal< sc_lv<11> > linebuf_3_address1;
    sc_signal< sc_logic > linebuf_3_ce1;
    sc_signal< sc_logic > linebuf_3_we1;
    sc_signal< sc_lv<11> > linebuf_4_address0;
    sc_signal< sc_logic > linebuf_4_ce0;
    sc_signal< sc_lv<32> > linebuf_4_q0;
    sc_signal< sc_lv<11> > linebuf_4_address1;
    sc_signal< sc_logic > linebuf_4_ce1;
    sc_signal< sc_logic > linebuf_4_we1;
    sc_signal< sc_lv<11> > linebuf_5_address0;
    sc_signal< sc_logic > linebuf_5_ce0;
    sc_signal< sc_lv<32> > linebuf_5_q0;
    sc_signal< sc_lv<11> > linebuf_5_address1;
    sc_signal< sc_logic > linebuf_5_ce1;
    sc_signal< sc_logic > linebuf_5_we1;
    sc_signal< sc_lv<11> > linebuf_6_address0;
    sc_signal< sc_logic > linebuf_6_ce0;
    sc_signal< sc_lv<32> > linebuf_6_q0;
    sc_signal< sc_lv<11> > linebuf_6_address1;
    sc_signal< sc_logic > linebuf_6_ce1;
    sc_signal< sc_logic > linebuf_6_we1;
    sc_signal< sc_lv<11> > linebuf_7_address0;
    sc_signal< sc_logic > linebuf_7_ce0;
    sc_signal< sc_lv<32> > linebuf_7_q0;
    sc_signal< sc_lv<11> > linebuf_7_address1;
    sc_signal< sc_logic > linebuf_7_ce1;
    sc_signal< sc_logic > linebuf_7_we1;
    sc_signal< sc_lv<11> > linebuf_8_address0;
    sc_signal< sc_logic > linebuf_8_ce0;
    sc_signal< sc_lv<32> > linebuf_8_q0;
    sc_signal< sc_lv<11> > linebuf_8_address1;
    sc_signal< sc_logic > linebuf_8_ce1;
    sc_signal< sc_logic > linebuf_8_we1;
    sc_signal< sc_lv<11> > linebuf_9_address0;
    sc_signal< sc_logic > linebuf_9_ce0;
    sc_signal< sc_lv<32> > linebuf_9_q0;
    sc_signal< sc_lv<11> > linebuf_9_address1;
    sc_signal< sc_logic > linebuf_9_ce1;
    sc_signal< sc_logic > linebuf_9_we1;
    sc_signal< sc_logic > height_blk_n;
    sc_signal< sc_logic > vconv_xlim_loc_blk_n;
    sc_signal< sc_logic > hconv_V_blk_n;
    sc_signal< sc_logic > ap_CS_fsm_pp0_stage0;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter1;
    sc_signal< bool > ap_block_pp0_stage0;
    sc_signal< sc_lv<1> > exitcond_flatten_reg_580;
    sc_signal< sc_logic > vconv_V_blk_n;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter10;
    sc_signal< sc_lv<1> > tmp_8_i_i_mid2_reg_614;
    sc_signal< sc_lv<1> > tmp_8_i_i_mid2_reg_614_pp0_iter9_reg;
    sc_signal< sc_logic > height_out_blk_n;
    sc_signal< sc_logic > vconv_xlim_loc_out_blk_n;
    sc_signal< sc_lv<64> > indvar_flatten_reg_304;
    sc_signal< sc_lv<31> > col1_0_i_i_i_reg_315;
    sc_signal< sc_lv<31> > row2_0_i_i_i_reg_326;
    sc_signal< sc_lv<32> > height_read_reg_549;
    sc_signal< bool > ap_block_state1;
    sc_signal< sc_lv<32> > vconv_xlim_loc_read_reg_554;
    sc_signal< sc_logic > ap_CS_fsm_state2;
    sc_signal< sc_lv<64> > grp_fu_343_p2;
    sc_signal< sc_lv<64> > bound_reg_570;
    sc_signal< sc_logic > ap_CS_fsm_state4;
    sc_signal< sc_lv<1> > tmp_11_i_i_fu_353_p2;
    sc_signal< sc_lv<1> > tmp_11_i_i_reg_575;
    sc_signal< bool > ap_block_state5_pp0_stage0_iter0;
    sc_signal< bool > ap_block_state6_pp0_stage0_iter1;
    sc_signal< bool > ap_block_state7_pp0_stage0_iter2;
    sc_signal< bool > ap_block_state8_pp0_stage0_iter3;
    sc_signal< bool > ap_block_state9_pp0_stage0_iter4;
    sc_signal< bool > ap_block_state10_pp0_stage0_iter5;
    sc_signal< bool > ap_block_state11_pp0_stage0_iter6;
    sc_signal< bool > ap_block_state12_pp0_stage0_iter7;
    sc_signal< bool > ap_block_state13_pp0_stage0_iter8;
    sc_signal< bool > ap_block_state14_pp0_stage0_iter9;
    sc_signal< bool > ap_block_state15_pp0_stage0_iter10;
    sc_signal< bool > ap_block_pp0_stage0_11001;
    sc_signal< sc_lv<1> > exitcond_flatten_fu_358_p2;
    sc_signal< sc_lv<1> > exitcond_flatten_reg_580_pp0_iter1_reg;
    sc_signal< sc_lv<1> > exitcond_flatten_reg_580_pp0_iter2_reg;
    sc_signal< sc_lv<1> > exitcond_flatten_reg_580_pp0_iter3_reg;
    sc_signal< sc_lv<1> > exitcond_flatten_reg_580_pp0_iter4_reg;
    sc_signal< sc_lv<1> > exitcond_flatten_reg_580_pp0_iter5_reg;
    sc_signal< sc_lv<1> > exitcond_flatten_reg_580_pp0_iter6_reg;
    sc_signal< sc_lv<1> > exitcond_flatten_reg_580_pp0_iter7_reg;
    sc_signal< sc_lv<1> > exitcond_flatten_reg_580_pp0_iter8_reg;
    sc_signal< sc_lv<64> > indvar_flatten_next_fu_363_p2;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter0;
    sc_signal< sc_lv<31> > row2_0_i_i_i_mid2_fu_369_p3;
    sc_signal< sc_lv<31> > row2_0_i_i_i_mid2_reg_589;
    sc_signal< sc_lv<31> > col_fu_377_p2;
    sc_signal< sc_lv<31> > col_reg_594;
    sc_signal< sc_lv<1> > tmp_8_i_i_fu_383_p2;
    sc_signal< sc_lv<1> > tmp_8_i_i_reg_599;
    sc_signal< sc_lv<31> > col1_0_i_i_i_mid2_fu_389_p3;
    sc_signal< sc_lv<31> > row_fu_397_p2;
    sc_signal< sc_lv<1> > tmp_8_i_i_mid2_fu_408_p3;
    sc_signal< sc_lv<1> > tmp_8_i_i_mid2_reg_614_pp0_iter2_reg;
    sc_signal< sc_lv<1> > tmp_8_i_i_mid2_reg_614_pp0_iter3_reg;
    sc_signal< sc_lv<1> > tmp_8_i_i_mid2_reg_614_pp0_iter4_reg;
    sc_signal< sc_lv<1> > tmp_8_i_i_mid2_reg_614_pp0_iter5_reg;
    sc_signal< sc_lv<1> > tmp_8_i_i_mid2_reg_614_pp0_iter6_reg;
    sc_signal< sc_lv<1> > tmp_8_i_i_mid2_reg_614_pp0_iter7_reg;
    sc_signal< sc_lv<1> > tmp_8_i_i_mid2_reg_614_pp0_iter8_reg;
    sc_signal< sc_lv<32> > tmp_12_reg_618;
    sc_signal< sc_lv<32> > tmp_12_reg_618_pp0_iter2_reg;
    sc_signal< sc_lv<32> > tmp_12_reg_618_pp0_iter3_reg;
    sc_signal< sc_lv<32> > tmp_12_reg_618_pp0_iter4_reg;
    sc_signal< sc_lv<32> > tmp_12_reg_618_pp0_iter5_reg;
    sc_signal< sc_lv<11> > linebuf_0_addr_reg_625;
    sc_signal< sc_lv<11> > linebuf_0_addr_reg_625_pp0_iter2_reg;
    sc_signal< sc_lv<11> > linebuf_1_addr_reg_631;
    sc_signal< sc_lv<11> > linebuf_1_addr_reg_631_pp0_iter2_reg;
    sc_signal< sc_lv<11> > linebuf_2_addr_reg_637;
    sc_signal< sc_lv<11> > linebuf_2_addr_reg_637_pp0_iter2_reg;
    sc_signal< sc_lv<11> > linebuf_3_addr_reg_643;
    sc_signal< sc_lv<11> > linebuf_3_addr_reg_643_pp0_iter2_reg;
    sc_signal< sc_lv<11> > linebuf_4_addr_reg_649;
    sc_signal< sc_lv<11> > linebuf_4_addr_reg_649_pp0_iter2_reg;
    sc_signal< sc_lv<11> > linebuf_5_addr_reg_655;
    sc_signal< sc_lv<11> > linebuf_5_addr_reg_655_pp0_iter2_reg;
    sc_signal< sc_lv<11> > linebuf_6_addr_reg_661;
    sc_signal< sc_lv<11> > linebuf_6_addr_reg_661_pp0_iter2_reg;
    sc_signal< sc_lv<11> > linebuf_7_addr_reg_667;
    sc_signal< sc_lv<11> > linebuf_7_addr_reg_667_pp0_iter2_reg;
    sc_signal< sc_lv<11> > linebuf_8_addr_reg_673;
    sc_signal< sc_lv<11> > linebuf_8_addr_reg_673_pp0_iter2_reg;
    sc_signal< sc_lv<11> > linebuf_9_addr_reg_679;
    sc_signal< sc_lv<32> > linebuf_1_load_reg_685;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter2;
    sc_signal< sc_lv<32> > linebuf_2_load_reg_691;
    sc_signal< sc_lv<32> > linebuf_3_load_reg_697;
    sc_signal< sc_lv<32> > linebuf_3_load_reg_697_pp0_iter3_reg;
    sc_signal< sc_lv<32> > linebuf_3_load_reg_697_pp0_iter4_reg;
    sc_signal< sc_lv<32> > linebuf_4_load_reg_703;
    sc_signal< sc_lv<32> > linebuf_5_load_reg_709;
    sc_signal< sc_lv<32> > linebuf_6_load_reg_715;
    sc_signal< sc_lv<32> > linebuf_6_load_reg_715_pp0_iter3_reg;
    sc_signal< sc_lv<32> > linebuf_7_load_reg_721;
    sc_signal< sc_lv<32> > linebuf_7_load_reg_721_pp0_iter3_reg;
    sc_signal< sc_lv<32> > linebuf_8_load_reg_727;
    sc_signal< sc_lv<32> > linebuf_9_load_reg_733;
    sc_signal< sc_lv<32> > linebuf_0_load_reg_739;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter3;
    sc_signal< sc_lv<32> > linebuf_0_load_reg_739_pp0_iter4_reg;
    sc_signal< sc_lv<32> > linebuf_0_load_reg_739_pp0_iter5_reg;
    sc_signal< sc_lv<32> > grp_fu_427_p2;
    sc_signal< sc_lv<32> > tmp_30_1_i_i_reg_745;
    sc_signal< sc_lv<32> > grp_fu_432_p2;
    sc_signal< sc_lv<32> > tmp_30_2_i_i_reg_750;
    sc_signal< sc_lv<32> > grp_fu_437_p2;
    sc_signal< sc_lv<32> > tmp_30_4_i_i_reg_755;
    sc_signal< sc_lv<32> > grp_fu_442_p2;
    sc_signal< sc_lv<32> > tmp_30_5_i_i_reg_760;
    sc_signal< sc_lv<32> > grp_fu_447_p2;
    sc_signal< sc_lv<32> > tmp_30_8_i_i_reg_765;
    sc_signal< sc_lv<32> > grp_fu_452_p2;
    sc_signal< sc_lv<32> > tmp_30_9_i_i_reg_770;
    sc_signal< sc_lv<32> > grp_fu_457_p2;
    sc_signal< sc_lv<32> > tmp_30_6_i_i_reg_775;
    sc_signal< sc_lv<32> > grp_fu_462_p2;
    sc_signal< sc_lv<32> > tmp_30_7_i_i_reg_780;
    sc_signal< sc_lv<32> > tmp2_fu_498_p2;
    sc_signal< sc_lv<32> > tmp2_reg_785;
    sc_signal< sc_lv<32> > tmp2_reg_785_pp0_iter7_reg;
    sc_signal< sc_lv<32> > tmp2_reg_785_pp0_iter8_reg;
    sc_signal< sc_lv<32> > tmp4_fu_508_p2;
    sc_signal< sc_lv<32> > tmp4_reg_790;
    sc_signal< sc_lv<32> > tmp4_reg_790_pp0_iter7_reg;
    sc_signal< sc_lv<32> > tmp4_reg_790_pp0_iter8_reg;
    sc_signal< sc_lv<32> > tmp8_fu_514_p2;
    sc_signal< sc_lv<32> > tmp8_reg_795;
    sc_signal< sc_lv<32> > tmp8_reg_795_pp0_iter7_reg;
    sc_signal< sc_lv<32> > tmp11_fu_518_p2;
    sc_signal< sc_lv<32> > tmp11_reg_800;
    sc_signal< sc_lv<32> > grp_fu_467_p2;
    sc_signal< sc_lv<32> > tmp_30_3_i_i_reg_805;
    sc_signal< sc_lv<32> > tmp9_fu_526_p2;
    sc_signal< sc_lv<32> > tmp9_reg_810;
    sc_signal< sc_lv<32> > tmp6_fu_535_p2;
    sc_signal< sc_lv<32> > tmp6_reg_815;
    sc_signal< sc_lv<32> > tmp_2_fu_544_p2;
    sc_signal< sc_lv<32> > tmp_2_reg_820;
    sc_signal< bool > ap_block_pp0_stage0_subdone;
    sc_signal< sc_logic > ap_condition_pp0_exit_iter0_state5;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter4;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter5;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter6;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter7;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter8;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter9;
    sc_signal< sc_lv<64> > tmp_16_i_i_fu_414_p1;
    sc_signal< bool > ap_block_pp0_stage0_01001;
    sc_signal< sc_lv<32> > grp_fu_343_p0;
    sc_signal< sc_lv<32> > grp_fu_343_p1;
    sc_signal< sc_lv<32> > row2_0_i_cast_i_i_fu_349_p1;
    sc_signal< sc_lv<1> > tmp_8_i_i_mid1_fu_403_p2;
    sc_signal< sc_lv<8> > grp_fu_427_p0;
    sc_signal< sc_lv<10> > grp_fu_432_p0;
    sc_signal< sc_lv<11> > grp_fu_437_p0;
    sc_signal< sc_lv<11> > grp_fu_442_p0;
    sc_signal< sc_lv<10> > grp_fu_447_p0;
    sc_signal< sc_lv<8> > grp_fu_452_p0;
    sc_signal< sc_lv<11> > grp_fu_457_p0;
    sc_signal< sc_lv<10> > grp_fu_462_p0;
    sc_signal< sc_lv<10> > grp_fu_467_p0;
    sc_signal< sc_lv<32> > tmp_3_fu_482_p2;
    sc_signal< sc_lv<32> > tmp_fu_472_p2;
    sc_signal< sc_lv<32> > tmp3_fu_492_p2;
    sc_signal< sc_lv<32> > tmp_4_fu_487_p2;
    sc_signal< sc_lv<32> > tmp5_fu_504_p2;
    sc_signal< sc_lv<32> > tmp_1_fu_477_p2;
    sc_signal< sc_lv<32> > tmp10_fu_522_p2;
    sc_signal< sc_lv<32> > tmp7_fu_531_p2;
    sc_signal< sc_lv<32> > tmp1_fu_540_p2;
    sc_signal< sc_logic > grp_fu_427_ce;
    sc_signal< sc_logic > grp_fu_432_ce;
    sc_signal< sc_logic > grp_fu_437_ce;
    sc_signal< sc_logic > grp_fu_442_ce;
    sc_signal< sc_logic > grp_fu_447_ce;
    sc_signal< sc_logic > grp_fu_452_ce;
    sc_signal< sc_logic > grp_fu_457_ce;
    sc_signal< sc_logic > grp_fu_462_ce;
    sc_signal< sc_logic > grp_fu_467_ce;
    sc_signal< sc_logic > ap_CS_fsm_state16;
    sc_signal< sc_lv<6> > ap_NS_fsm;
    sc_signal< bool > ap_block_pp0;
    sc_signal< bool > ap_enable_operation_52;
    sc_signal< bool > ap_enable_state6_pp0_iter1_stage0;
    sc_signal< bool > ap_enable_operation_71;
    sc_signal< bool > ap_enable_state7_pp0_iter2_stage0;
    sc_signal< bool > ap_enable_operation_85;
    sc_signal< bool > ap_enable_state8_pp0_iter3_stage0;
    sc_signal< bool > ap_enable_operation_54;
    sc_signal< bool > ap_enable_operation_72;
    sc_signal< bool > ap_enable_operation_86;
    sc_signal< bool > ap_enable_operation_56;
    sc_signal< bool > ap_enable_operation_73;
    sc_signal< bool > ap_enable_operation_88;
    sc_signal< bool > ap_enable_operation_58;
    sc_signal< bool > ap_enable_operation_74;
    sc_signal< bool > ap_enable_operation_90;
    sc_signal< bool > ap_enable_operation_60;
    sc_signal< bool > ap_enable_operation_75;
    sc_signal< bool > ap_enable_operation_91;
    sc_signal< bool > ap_enable_operation_62;
    sc_signal< bool > ap_enable_operation_76;
    sc_signal< bool > ap_enable_operation_92;
    sc_signal< bool > ap_enable_operation_64;
    sc_signal< bool > ap_enable_operation_77;
    sc_signal< bool > ap_enable_operation_94;
    sc_signal< bool > ap_enable_operation_66;
    sc_signal< bool > ap_enable_operation_78;
    sc_signal< bool > ap_enable_operation_96;
    sc_signal< bool > ap_enable_operation_68;
    sc_signal< bool > ap_enable_operation_79;
    sc_signal< bool > ap_enable_operation_80;
    sc_signal< bool > ap_enable_operation_70;
    sc_signal< bool > ap_enable_operation_81;
    sc_signal< bool > ap_enable_operation_83;
    sc_signal< sc_logic > ap_idle_pp0;
    sc_signal< sc_logic > ap_enable_pp0;
    sc_signal< sc_lv<64> > grp_fu_343_p00;
    sc_signal< sc_lv<64> > grp_fu_343_p10;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<6> ap_ST_fsm_state1;
    static const sc_lv<6> ap_ST_fsm_state2;
    static const sc_lv<6> ap_ST_fsm_state3;
    static const sc_lv<6> ap_ST_fsm_state4;
    static const sc_lv<6> ap_ST_fsm_pp0_stage0;
    static const sc_lv<6> ap_ST_fsm_state16;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const sc_lv<32> ap_const_lv32_4;
    static const bool ap_const_boolean_0;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<32> ap_const_lv32_1;
    static const sc_lv<32> ap_const_lv32_3;
    static const sc_lv<64> ap_const_lv64_0;
    static const sc_lv<31> ap_const_lv31_0;
    static const sc_lv<64> ap_const_lv64_1;
    static const sc_lv<31> ap_const_lv31_1;
    static const sc_lv<31> ap_const_lv31_9;
    static const sc_lv<32> ap_const_lv32_6F;
    static const sc_lv<32> ap_const_lv32_10A;
    static const sc_lv<32> ap_const_lv32_2D4;
    static const sc_lv<32> ap_const_lv32_335;
    static const sc_lv<32> ap_const_lv32_1F2;
    static const sc_lv<32> ap_const_lv32_5;
    static const sc_lv<32> ap_const_lv32_2;
    // Thread declarations
    void thread_ap_var_for_const0();
    void thread_ap_clk_no_reset_();
    void thread_ap_CS_fsm_pp0_stage0();
    void thread_ap_CS_fsm_state1();
    void thread_ap_CS_fsm_state16();
    void thread_ap_CS_fsm_state2();
    void thread_ap_CS_fsm_state4();
    void thread_ap_block_pp0();
    void thread_ap_block_pp0_stage0();
    void thread_ap_block_pp0_stage0_01001();
    void thread_ap_block_pp0_stage0_11001();
    void thread_ap_block_pp0_stage0_subdone();
    void thread_ap_block_state1();
    void thread_ap_block_state10_pp0_stage0_iter5();
    void thread_ap_block_state11_pp0_stage0_iter6();
    void thread_ap_block_state12_pp0_stage0_iter7();
    void thread_ap_block_state13_pp0_stage0_iter8();
    void thread_ap_block_state14_pp0_stage0_iter9();
    void thread_ap_block_state15_pp0_stage0_iter10();
    void thread_ap_block_state5_pp0_stage0_iter0();
    void thread_ap_block_state6_pp0_stage0_iter1();
    void thread_ap_block_state7_pp0_stage0_iter2();
    void thread_ap_block_state8_pp0_stage0_iter3();
    void thread_ap_block_state9_pp0_stage0_iter4();
    void thread_ap_condition_pp0_exit_iter0_state5();
    void thread_ap_done();
    void thread_ap_enable_operation_52();
    void thread_ap_enable_operation_54();
    void thread_ap_enable_operation_56();
    void thread_ap_enable_operation_58();
    void thread_ap_enable_operation_60();
    void thread_ap_enable_operation_62();
    void thread_ap_enable_operation_64();
    void thread_ap_enable_operation_66();
    void thread_ap_enable_operation_68();
    void thread_ap_enable_operation_70();
    void thread_ap_enable_operation_71();
    void thread_ap_enable_operation_72();
    void thread_ap_enable_operation_73();
    void thread_ap_enable_operation_74();
    void thread_ap_enable_operation_75();
    void thread_ap_enable_operation_76();
    void thread_ap_enable_operation_77();
    void thread_ap_enable_operation_78();
    void thread_ap_enable_operation_79();
    void thread_ap_enable_operation_80();
    void thread_ap_enable_operation_81();
    void thread_ap_enable_operation_83();
    void thread_ap_enable_operation_85();
    void thread_ap_enable_operation_86();
    void thread_ap_enable_operation_88();
    void thread_ap_enable_operation_90();
    void thread_ap_enable_operation_91();
    void thread_ap_enable_operation_92();
    void thread_ap_enable_operation_94();
    void thread_ap_enable_operation_96();
    void thread_ap_enable_pp0();
    void thread_ap_enable_state6_pp0_iter1_stage0();
    void thread_ap_enable_state7_pp0_iter2_stage0();
    void thread_ap_enable_state8_pp0_iter3_stage0();
    void thread_ap_idle();
    void thread_ap_idle_pp0();
    void thread_ap_ready();
    void thread_col1_0_i_i_i_mid2_fu_389_p3();
    void thread_col_fu_377_p2();
    void thread_exitcond_flatten_fu_358_p2();
    void thread_grp_fu_343_p0();
    void thread_grp_fu_343_p00();
    void thread_grp_fu_343_p1();
    void thread_grp_fu_343_p10();
    void thread_grp_fu_427_ce();
    void thread_grp_fu_427_p0();
    void thread_grp_fu_432_ce();
    void thread_grp_fu_432_p0();
    void thread_grp_fu_437_ce();
    void thread_grp_fu_437_p0();
    void thread_grp_fu_442_ce();
    void thread_grp_fu_442_p0();
    void thread_grp_fu_447_ce();
    void thread_grp_fu_447_p0();
    void thread_grp_fu_452_ce();
    void thread_grp_fu_452_p0();
    void thread_grp_fu_457_ce();
    void thread_grp_fu_457_p0();
    void thread_grp_fu_462_ce();
    void thread_grp_fu_462_p0();
    void thread_grp_fu_467_ce();
    void thread_grp_fu_467_p0();
    void thread_hconv_V_blk_n();
    void thread_hconv_V_read();
    void thread_height_blk_n();
    void thread_height_out_blk_n();
    void thread_height_out_din();
    void thread_height_out_write();
    void thread_height_read();
    void thread_indvar_flatten_next_fu_363_p2();
    void thread_linebuf_0_address0();
    void thread_linebuf_0_address1();
    void thread_linebuf_0_ce0();
    void thread_linebuf_0_ce1();
    void thread_linebuf_0_we1();
    void thread_linebuf_1_address0();
    void thread_linebuf_1_address1();
    void thread_linebuf_1_ce0();
    void thread_linebuf_1_ce1();
    void thread_linebuf_1_we1();
    void thread_linebuf_2_address0();
    void thread_linebuf_2_address1();
    void thread_linebuf_2_ce0();
    void thread_linebuf_2_ce1();
    void thread_linebuf_2_we1();
    void thread_linebuf_3_address0();
    void thread_linebuf_3_address1();
    void thread_linebuf_3_ce0();
    void thread_linebuf_3_ce1();
    void thread_linebuf_3_we1();
    void thread_linebuf_4_address0();
    void thread_linebuf_4_address1();
    void thread_linebuf_4_ce0();
    void thread_linebuf_4_ce1();
    void thread_linebuf_4_we1();
    void thread_linebuf_5_address0();
    void thread_linebuf_5_address1();
    void thread_linebuf_5_ce0();
    void thread_linebuf_5_ce1();
    void thread_linebuf_5_we1();
    void thread_linebuf_6_address0();
    void thread_linebuf_6_address1();
    void thread_linebuf_6_ce0();
    void thread_linebuf_6_ce1();
    void thread_linebuf_6_we1();
    void thread_linebuf_7_address0();
    void thread_linebuf_7_address1();
    void thread_linebuf_7_ce0();
    void thread_linebuf_7_ce1();
    void thread_linebuf_7_we1();
    void thread_linebuf_8_address0();
    void thread_linebuf_8_address1();
    void thread_linebuf_8_ce0();
    void thread_linebuf_8_ce1();
    void thread_linebuf_8_we1();
    void thread_linebuf_9_address0();
    void thread_linebuf_9_address1();
    void thread_linebuf_9_ce0();
    void thread_linebuf_9_ce1();
    void thread_linebuf_9_we1();
    void thread_row2_0_i_cast_i_i_fu_349_p1();
    void thread_row2_0_i_i_i_mid2_fu_369_p3();
    void thread_row_fu_397_p2();
    void thread_tmp10_fu_522_p2();
    void thread_tmp11_fu_518_p2();
    void thread_tmp1_fu_540_p2();
    void thread_tmp2_fu_498_p2();
    void thread_tmp3_fu_492_p2();
    void thread_tmp4_fu_508_p2();
    void thread_tmp5_fu_504_p2();
    void thread_tmp6_fu_535_p2();
    void thread_tmp7_fu_531_p2();
    void thread_tmp8_fu_514_p2();
    void thread_tmp9_fu_526_p2();
    void thread_tmp_11_i_i_fu_353_p2();
    void thread_tmp_16_i_i_fu_414_p1();
    void thread_tmp_1_fu_477_p2();
    void thread_tmp_2_fu_544_p2();
    void thread_tmp_3_fu_482_p2();
    void thread_tmp_4_fu_487_p2();
    void thread_tmp_8_i_i_fu_383_p2();
    void thread_tmp_8_i_i_mid1_fu_403_p2();
    void thread_tmp_8_i_i_mid2_fu_408_p3();
    void thread_tmp_fu_472_p2();
    void thread_vconv_V_blk_n();
    void thread_vconv_V_din();
    void thread_vconv_V_write();
    void thread_vconv_xlim_loc_blk_n();
    void thread_vconv_xlim_loc_out_blk_n();
    void thread_vconv_xlim_loc_out_din();
    void thread_vconv_xlim_loc_out_write();
    void thread_vconv_xlim_loc_read();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
