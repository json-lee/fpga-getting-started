# 2D convolution with linebuffer

参考Vivado自带的[卷积示例](https://github.com/Xilinx/HLS-Tiny-Tutorials/tree/master/algorithm_2D_convolution_linebuffer)。
采样出来的性能数据如下：

![](./assets/hls-synthesis.png)

![](./assets/perf-info.png)

## Refs

- <https://github.com/Xilinx/HLS-Tiny-Tutorials>