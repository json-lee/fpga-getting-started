############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
############################################################
open_project matrixmul.prj
set_top matrixmul
add_files matrixmul.cpp
add_files -tb ../../High-Level-Synthesis-Flow-on-Zynq-using-Vivado-HLS-master/source/lab1/matrixmul_test.cpp
open_solution "solution1"
set_part {xc7z020clg400-1} -tool vivado
create_clock -period 10 -name default
#source "./matrixmul.prj/solution1/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -format ip_catalog
