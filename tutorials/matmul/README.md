## MatMul

该示例参考了lab中的[示例1](https://github.com/xupgit/High-Level-Synthesis-Flow-on-Zynq-using-Vivado-HLS/tree/master/source/lab1)，仅仅用于熟悉开发流程。
生成的verilog代码见[lab1/matrixmul.prj/solution1/syn/verilog](lab1/matrixmul.prj/solution1/syn/verilog)，采样出来的性能信息如下：

![](./assets/perf-info.png)
