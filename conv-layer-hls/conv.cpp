//*****************************************************************************
//
// Concolution layer hardware implementation with HLS.
//
//*****************************************************************************

#include "conv.h"

/**
 * Load input feature maps from PS to PL using FIFO, implemented at PL.
 * The ifm_buff is a cycle buffer, we put padding directly to the buffer.
 * cifm_counter will trace the current position of loaded feature map.
 **/
void load_cifm_data(IPACK *cifm, DATA_T ifm_buff0[N][(IC + 2 * P)],
                    DATA_T ifm_buff1[N][(IC + 2 * P)],
                    DATA_T ifm_buff2[N][(IC + 2 * P)], int *cifm_counter) {
  for (int j = 0; j < (IC + 2 * P); j++) {
#pragma HLS PIPELINE
    ifm_buff0[0][j] = cifm[*cifm_counter].a0;
    ifm_buff0[1][j] = cifm[*cifm_counter].a1;
    ifm_buff0[2][j] = cifm[*cifm_counter].a2;
    ifm_buff0[3][j] = cifm[*cifm_counter].a3;
    ifm_buff0[4][j] = cifm[*cifm_counter].a4;
    ifm_buff0[5][j] = cifm[*cifm_counter].a5;
    ifm_buff0[6][j] = cifm[*cifm_counter].a6;
    ifm_buff0[7][j] = cifm[*cifm_counter].a7;
    ifm_buff0[8][j] = cifm[*cifm_counter].a8;
    ifm_buff0[9][j] = cifm[*cifm_counter].a9;
    ifm_buff0[10][j] = cifm[*cifm_counter].a10;
    ifm_buff0[11][j] = cifm[*cifm_counter].a11;
    ifm_buff0[12][j] = cifm[*cifm_counter].a12;
    ifm_buff0[13][j] = cifm[*cifm_counter].a13;
    ifm_buff0[14][j] = cifm[*cifm_counter].a14;
    ifm_buff0[15][j] = cifm[*cifm_counter].a15;
    (*cifm_counter)++;
  }

  for (int j = 0; j < (IC + 2 * P); j++) {
#pragma HLS PIPELINE
    ifm_buff1[0][j] = cifm[*cifm_counter].a0;
    ifm_buff1[1][j] = cifm[*cifm_counter].a1;
    ifm_buff1[2][j] = cifm[*cifm_counter].a2;
    ifm_buff1[3][j] = cifm[*cifm_counter].a3;
    ifm_buff1[4][j] = cifm[*cifm_counter].a4;
    ifm_buff1[5][j] = cifm[*cifm_counter].a5;
    ifm_buff1[6][j] = cifm[*cifm_counter].a6;
    ifm_buff1[7][j] = cifm[*cifm_counter].a7;
    ifm_buff1[8][j] = cifm[*cifm_counter].a8;
    ifm_buff1[9][j] = cifm[*cifm_counter].a9;
    ifm_buff1[10][j] = cifm[*cifm_counter].a10;
    ifm_buff1[11][j] = cifm[*cifm_counter].a11;
    ifm_buff1[12][j] = cifm[*cifm_counter].a12;
    ifm_buff1[13][j] = cifm[*cifm_counter].a13;
    ifm_buff1[14][j] = cifm[*cifm_counter].a14;
    ifm_buff1[15][j] = cifm[*cifm_counter].a15;
    (*cifm_counter)++;
  }

  for (int j = 0; j < (IC + 2 * P); j++) {
#pragma HLS PIPELINE
    ifm_buff2[0][j] = cifm[*cifm_counter].a0;
    ifm_buff2[1][j] = cifm[*cifm_counter].a1;
    ifm_buff2[2][j] = cifm[*cifm_counter].a2;
    ifm_buff2[3][j] = cifm[*cifm_counter].a3;
    ifm_buff2[4][j] = cifm[*cifm_counter].a4;
    ifm_buff2[5][j] = cifm[*cifm_counter].a5;
    ifm_buff2[6][j] = cifm[*cifm_counter].a6;
    ifm_buff2[7][j] = cifm[*cifm_counter].a7;
    ifm_buff2[8][j] = cifm[*cifm_counter].a8;
    ifm_buff2[9][j] = cifm[*cifm_counter].a9;
    ifm_buff2[10][j] = cifm[*cifm_counter].a10;
    ifm_buff2[11][j] = cifm[*cifm_counter].a11;
    ifm_buff2[12][j] = cifm[*cifm_counter].a12;
    ifm_buff2[13][j] = cifm[*cifm_counter].a13;
    ifm_buff2[14][j] = cifm[*cifm_counter].a14;
    ifm_buff2[15][j] = cifm[*cifm_counter].a15;
    (*cifm_counter)++;
  }
}

void write_row_ifm(IPACK *cifm, DATA_T ifm_buff0[N][(IC + 2 * P)],
		int *cifm_counter, bool enable) {
  if (enable) {
#pragma HLS interface ap_bus port=cifm
	  for (int j = 0; j < (IC + 2 * P); j++) {
#pragma HLS PIPELINE
      ifm_buff0[0][j] = cifm[*cifm_counter].a0;
      ifm_buff0[1][j] = cifm[*cifm_counter].a1;
      ifm_buff0[2][j] = cifm[*cifm_counter].a2;
      ifm_buff0[3][j] = cifm[*cifm_counter].a3;
      ifm_buff0[4][j] = cifm[*cifm_counter].a4;
      ifm_buff0[5][j] = cifm[*cifm_counter].a5;
      ifm_buff0[6][j] = cifm[*cifm_counter].a6;
      ifm_buff0[7][j] = cifm[*cifm_counter].a7;
      ifm_buff0[8][j] = cifm[*cifm_counter].a8;
      ifm_buff0[9][j] = cifm[*cifm_counter].a9;
      ifm_buff0[10][j] = cifm[*cifm_counter].a10;
      ifm_buff0[11][j] = cifm[*cifm_counter].a11;
      ifm_buff0[12][j] = cifm[*cifm_counter].a12;
      ifm_buff0[13][j] = cifm[*cifm_counter].a13;
      ifm_buff0[14][j] = cifm[*cifm_counter].a14;
      ifm_buff0[15][j] = cifm[*cifm_counter].a15;
      (*cifm_counter)++;
    }
  }
}

/**
 * Load the transformed filter weight to filter buffer. Implemented at PS.
 **/
void load_filter_buffer(FPACK *wgt, DATA_T filter_buff[M][N][ROW_G][ROW_G]) {
  int count = 0;
#pragma HLS interface ap_bus port=wgt
  for (int m = 0; m < M; m++) {
    for (int n = 0; n < N; n++) {
#pragma HLS PIPELINE
      filter_buff[m][n][0][0] = wgt[count].f0;
      filter_buff[m][n][0][1] = wgt[count].f1;
      filter_buff[m][n][0][2] = wgt[count].f2;
      filter_buff[m][n][1][0] = wgt[count].f3;
      filter_buff[m][n][1][1] = wgt[count].f4;
      filter_buff[m][n][1][2] = wgt[count].f5;
      filter_buff[m][n][2][0] = wgt[count].f6;
      filter_buff[m][n][2][1] = wgt[count].f7;
      filter_buff[m][n][2][2] = wgt[count].f8;
      count++;
    }
  }
}

void conv_write(DATA_T filter_buff[M][N][ROW_G][ROW_G],
                DATA_T ifm_buff0[N][(IC + 2 * P)],
                DATA_T ifm_buff1[N][(IC + 2 * P)],
                DATA_T ifm_buff2[N][(IC + 2 * P)],
                DATA_T ofm_buff0[M][OC]) {
  for (int col = 0; col < IC; col += w_m) // Column loop with stride w_m
  {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = 56
    for (int ti = 0; ti < M; ti++) // Output channel loop
    {
#pragma HLS PIPELINE
#pragma HLS LOOP_TRIPCOUNT min = 1 max = 16
#pragma HLS UNROLL FACTOR = 2
      DATA_T Y = 0;
      for (int to = 0; to < N; to++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = 16
#pragma HLS UNROLL FACTOR = 16
        DATA_T mut000 = ifm_buff0[to][col] * filter_buff[ti][to][0][0];
        DATA_T mut100 = ifm_buff1[to][col] * filter_buff[ti][to][1][0];
        DATA_T mut200 = ifm_buff2[to][col] * filter_buff[ti][to][2][0];
        DATA_T mut010 = ifm_buff0[to][col + 1] * filter_buff[ti][to][0][1];
        DATA_T mut110 = ifm_buff1[to][col + 1] * filter_buff[ti][to][1][1];
        DATA_T mut210 = ifm_buff2[to][col + 1] * filter_buff[ti][to][2][1];
        DATA_T mut020 = ifm_buff0[to][col + 2] * filter_buff[ti][to][0][2];
        DATA_T mut120 = ifm_buff1[to][col + 2] * filter_buff[ti][to][1][2];
        DATA_T mut220 = ifm_buff2[to][col + 2] * filter_buff[ti][to][2][2];
        DATA_T acc000 = mut000 + mut100;
        DATA_T acc010 = mut200 + mut010;
        DATA_T acc020 = mut110 + mut210;
        DATA_T acc030 = mut020 + mut120;
        DATA_T acc040 = acc000 + acc010;
        DATA_T acc050 = acc020 + acc030;
        DATA_T acc060 = acc040 + acc050;
        Y += (acc060 + mut220);
      }
      ofm_buff0[ti][col] = Y;
    }
  }
}

void conv_read(OPACK *cofm, DATA_T ofm_buff0[M][OC], int *cofm_counter,
               bool enable) {
  if (enable) {
#pragma HLS interface ap_bus port=cofm
    for (int j = 0; j < OC; j++) {
#pragma HLS PIPELINE
      cofm[(*cofm_counter)].b0 = ofm_buff0[0][j];
      cofm[(*cofm_counter)].b1 = ofm_buff0[1][j];
      cofm[(*cofm_counter)].b2 = ofm_buff0[2][j];
      cofm[(*cofm_counter)].b3 = ofm_buff0[3][j];
      cofm[(*cofm_counter)].b4 = ofm_buff0[4][j];
      cofm[(*cofm_counter)].b5 = ofm_buff0[5][j];
      cofm[(*cofm_counter)].b6 = ofm_buff0[6][j];
      cofm[(*cofm_counter)].b7 = ofm_buff0[7][j];
      cofm[(*cofm_counter)].b8 = ofm_buff0[8][j];
      cofm[(*cofm_counter)].b9 = ofm_buff0[9][j];
      cofm[(*cofm_counter)].b10 = ofm_buff0[10][j];
      cofm[(*cofm_counter)].b11 = ofm_buff0[11][j];
      cofm[(*cofm_counter)].b12 = ofm_buff0[12][j];
      cofm[(*cofm_counter)].b13 = ofm_buff0[13][j];
      cofm[(*cofm_counter)].b14 = ofm_buff0[14][j];
      cofm[(*cofm_counter)].b15 = ofm_buff0[15][j];
      (*cofm_counter)++;
    }
  }
}

#pragma SDS data zero_copy(cifm [0:(IR + 2 * P) * (IC + 2 * P)],               \
                           cofm [0:OR * OC], tran_wgt [0:N * M])
#pragma SDS data access_pattern(cifm                                           \
                                : SEQUENTIAL, cofm                             \
                                : SEQUENTIAL, tran_wgt                         \
                                : SEQUENTIAL)
void conv(IPACK *cifm, OPACK *cofm, FPACK *tran_wgt) {
/**
 * In the first part, input feature maps, transformed weight maps will be loaded
 * from PS to PL using AXI interface (FIFO).
 **/
#pragma HLS data_pack variable = cifm struct_level
#pragma HLS data_pack variable = cofm struct_level
#pragma HLS data_pack variable = tran_wgt struct_level
  // Define ifm, ofm, wgt buffer and the partition way.
  DATA_T ifm_buff0[N][IC + 2 * P];
#pragma HLS ARRAY_PARTITION variable = ifm_buff0 dim = 1 complete
  DATA_T ifm_buff1[N][IC + 2 * P];
#pragma HLS ARRAY_PARTITION variable = ifm_buff1 dim = 1 complete
  DATA_T ifm_buff2[N][IC + 2 * P];
#pragma HLS ARRAY_PARTITION variable = ifm_buff2 dim = 1 complete
  DATA_T ifm_buff3[N][IC + 2 * P];
#pragma HLS ARRAY_PARTITION variable = ifm_buff3 dim = 1 complete

  DATA_T filter_buff[M][N][ROW_G][ROW_G];
//#pragma HLS ARRAY_PARTITION variable=filter_buff dim=1 factor=4
#pragma HLS ARRAY_PARTITION variable = filter_buff dim = 2 complete
#pragma HLS ARRAY_PARTITION variable = filter_buff dim = 3 complete
#pragma HLS ARRAY_PARTITION variable = filter_buff dim = 4 complete

  DATA_T ofm_buff0[M][OC];
  DATA_T ofm_buff1[M][OC];
#pragma HLS ARRAY_PARTITION variable = ofm_buff0 dim = 1 factor = 16
#pragma HLS ARRAY_PARTITION variable = ofm_buff1 dim = 1 factor = 16
  int cifm_counter = 0;
  int cofm_counter = 0;
  // These variable represent readable lines of line buffer.
  short unsigned int t0, t1, t2;
  // These varibale represent writable lines of line buffer.
  short unsigned int s1;
  short unsigned int rotate_counter = 0;

  /**Load data from PS. Here we can use dataflow pragma since filter weight and
   *cifm can be load simutaneously. load_filter_buffer may be one bottleneck of
   *this design since we load all data from PS, actually, one can load part of
   *filter data by using bath method which beyond of this design's scope.
   **/

  load_cifm_data(cifm, ifm_buff1, ifm_buff2, ifm_buff3, &cifm_counter);
  load_filter_buffer(tran_wgt, filter_buff);

  /**
   * In the second part, caculating convolution using winograde method with line
   *buffer for data reuse.
   **/
  // Define flag of ofm buffer, when flag = 0, writing to buffer1 and read from
  // buffer2.
  //#pragma HLS DATAFLOW
  for (int row = 0; row < IR; row += w_m) // Row loop with stride w_m
  {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = 16
    // In this subpart we need to define where is the legal line buffer, and
    // rotate the line of line buffer.
    if (rotate_counter == 0) {
      write_row_ifm(cifm, ifm_buff0, &cifm_counter, 1);
      conv_write(filter_buff, ifm_buff1, ifm_buff2, ifm_buff3, ofm_buff0);
      conv_read(cofm, ofm_buff1, &cofm_counter, row != 0);
      // t0 = 1;
      // t1 = 2;
      // t2 = 3;
      // s1 = 0;
    } else if (rotate_counter == 1) {
      write_row_ifm(cifm, ifm_buff1, &cifm_counter, 1);
      conv_write(filter_buff, ifm_buff2, ifm_buff3, ifm_buff0, ofm_buff1);
      conv_read(cofm, ofm_buff0, &cofm_counter, 1);
      // t0 = 2;
      // t1 = 3;
      // t2 = 0;
      // s1 = 1;
    } else if (rotate_counter == 2) {
      write_row_ifm(cifm, ifm_buff2, &cifm_counter, 1);
      conv_write(filter_buff, ifm_buff3, ifm_buff0, ifm_buff1, ofm_buff0);
      conv_read(cofm, ofm_buff1, &cofm_counter, 1);
      // t0 = 3;
      // t1 = 0;
      // t2 = 1;
      // s1 = 2;
    } else if (rotate_counter == 3) {
      write_row_ifm(cifm, ifm_buff3, &cifm_counter, row != IR - 1);
      conv_write(filter_buff, ifm_buff0, ifm_buff1, ifm_buff2, ofm_buff1);
      conv_read(cofm, ofm_buff0, &cofm_counter, 1);
      // t0 = 0;
      // t1 = 1;
      // t2 = 2;
      // s1 = 3;
    }
    rotate_counter += 1;
    if (rotate_counter == 4) {
      rotate_counter = 0;
    }
  }
  conv_read(cofm, ofm_buff1, &cofm_counter, 1);
}
