# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 196 \
    name ifm_buff0_0 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_0 \
    op interface \
    ports { ifm_buff0_0_address0 { O 6 vector } ifm_buff0_0_ce0 { O 1 bit } ifm_buff0_0_we0 { O 1 bit } ifm_buff0_0_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 197 \
    name ifm_buff0_1 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_1 \
    op interface \
    ports { ifm_buff0_1_address0 { O 6 vector } ifm_buff0_1_ce0 { O 1 bit } ifm_buff0_1_we0 { O 1 bit } ifm_buff0_1_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 198 \
    name ifm_buff0_2 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_2 \
    op interface \
    ports { ifm_buff0_2_address0 { O 6 vector } ifm_buff0_2_ce0 { O 1 bit } ifm_buff0_2_we0 { O 1 bit } ifm_buff0_2_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 199 \
    name ifm_buff0_3 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_3 \
    op interface \
    ports { ifm_buff0_3_address0 { O 6 vector } ifm_buff0_3_ce0 { O 1 bit } ifm_buff0_3_we0 { O 1 bit } ifm_buff0_3_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 200 \
    name ifm_buff0_4 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_4 \
    op interface \
    ports { ifm_buff0_4_address0 { O 6 vector } ifm_buff0_4_ce0 { O 1 bit } ifm_buff0_4_we0 { O 1 bit } ifm_buff0_4_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_4'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 201 \
    name ifm_buff0_5 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_5 \
    op interface \
    ports { ifm_buff0_5_address0 { O 6 vector } ifm_buff0_5_ce0 { O 1 bit } ifm_buff0_5_we0 { O 1 bit } ifm_buff0_5_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_5'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 202 \
    name ifm_buff0_6 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_6 \
    op interface \
    ports { ifm_buff0_6_address0 { O 6 vector } ifm_buff0_6_ce0 { O 1 bit } ifm_buff0_6_we0 { O 1 bit } ifm_buff0_6_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_6'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 203 \
    name ifm_buff0_7 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_7 \
    op interface \
    ports { ifm_buff0_7_address0 { O 6 vector } ifm_buff0_7_ce0 { O 1 bit } ifm_buff0_7_we0 { O 1 bit } ifm_buff0_7_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_7'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 204 \
    name ifm_buff0_8 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_8 \
    op interface \
    ports { ifm_buff0_8_address0 { O 6 vector } ifm_buff0_8_ce0 { O 1 bit } ifm_buff0_8_we0 { O 1 bit } ifm_buff0_8_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_8'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 205 \
    name ifm_buff0_9 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_9 \
    op interface \
    ports { ifm_buff0_9_address0 { O 6 vector } ifm_buff0_9_ce0 { O 1 bit } ifm_buff0_9_we0 { O 1 bit } ifm_buff0_9_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_9'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 206 \
    name ifm_buff0_10 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_10 \
    op interface \
    ports { ifm_buff0_10_address0 { O 6 vector } ifm_buff0_10_ce0 { O 1 bit } ifm_buff0_10_we0 { O 1 bit } ifm_buff0_10_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_10'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 207 \
    name ifm_buff0_11 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_11 \
    op interface \
    ports { ifm_buff0_11_address0 { O 6 vector } ifm_buff0_11_ce0 { O 1 bit } ifm_buff0_11_we0 { O 1 bit } ifm_buff0_11_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_11'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 208 \
    name ifm_buff0_12 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_12 \
    op interface \
    ports { ifm_buff0_12_address0 { O 6 vector } ifm_buff0_12_ce0 { O 1 bit } ifm_buff0_12_we0 { O 1 bit } ifm_buff0_12_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_12'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 209 \
    name ifm_buff0_13 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_13 \
    op interface \
    ports { ifm_buff0_13_address0 { O 6 vector } ifm_buff0_13_ce0 { O 1 bit } ifm_buff0_13_we0 { O 1 bit } ifm_buff0_13_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_13'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 210 \
    name ifm_buff0_14 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_14 \
    op interface \
    ports { ifm_buff0_14_address0 { O 6 vector } ifm_buff0_14_ce0 { O 1 bit } ifm_buff0_14_we0 { O 1 bit } ifm_buff0_14_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_14'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 211 \
    name ifm_buff0_15 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_15 \
    op interface \
    ports { ifm_buff0_15_address0 { O 6 vector } ifm_buff0_15_ce0 { O 1 bit } ifm_buff0_15_we0 { O 1 bit } ifm_buff0_15_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_15'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 195 \
    name cifm \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_cifm \
    op interface \
    ports { cifm_req_din { O 1 bit } cifm_req_full_n { I 1 bit } cifm_req_write { O 1 bit } cifm_rsp_empty_n { I 1 bit } cifm_rsp_read { O 1 bit } cifm_address { O 32 vector } cifm_datain { I 512 vector } cifm_dataout { O 512 vector } cifm_size { O 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 212 \
    name cifm_counter_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_cifm_counter_read \
    op interface \
    ports { cifm_counter_read { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 213 \
    name enable \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_enable \
    op interface \
    ports { enable { I 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -2 \
    name ap_return \
    type ap_return \
    reset_level 1 \
    sync_rst true \
    corename ap_return \
    op interface \
    ports { ap_return { O 32 vector } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -4 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


