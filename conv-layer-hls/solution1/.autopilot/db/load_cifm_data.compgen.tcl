# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 2 \
    name ifm_buff0_0 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_0 \
    op interface \
    ports { ifm_buff0_0_address0 { O 6 vector } ifm_buff0_0_ce0 { O 1 bit } ifm_buff0_0_we0 { O 1 bit } ifm_buff0_0_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 3 \
    name ifm_buff0_1 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_1 \
    op interface \
    ports { ifm_buff0_1_address0 { O 6 vector } ifm_buff0_1_ce0 { O 1 bit } ifm_buff0_1_we0 { O 1 bit } ifm_buff0_1_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 4 \
    name ifm_buff0_2 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_2 \
    op interface \
    ports { ifm_buff0_2_address0 { O 6 vector } ifm_buff0_2_ce0 { O 1 bit } ifm_buff0_2_we0 { O 1 bit } ifm_buff0_2_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 5 \
    name ifm_buff0_3 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_3 \
    op interface \
    ports { ifm_buff0_3_address0 { O 6 vector } ifm_buff0_3_ce0 { O 1 bit } ifm_buff0_3_we0 { O 1 bit } ifm_buff0_3_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 6 \
    name ifm_buff0_4 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_4 \
    op interface \
    ports { ifm_buff0_4_address0 { O 6 vector } ifm_buff0_4_ce0 { O 1 bit } ifm_buff0_4_we0 { O 1 bit } ifm_buff0_4_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_4'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 7 \
    name ifm_buff0_5 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_5 \
    op interface \
    ports { ifm_buff0_5_address0 { O 6 vector } ifm_buff0_5_ce0 { O 1 bit } ifm_buff0_5_we0 { O 1 bit } ifm_buff0_5_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_5'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 8 \
    name ifm_buff0_6 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_6 \
    op interface \
    ports { ifm_buff0_6_address0 { O 6 vector } ifm_buff0_6_ce0 { O 1 bit } ifm_buff0_6_we0 { O 1 bit } ifm_buff0_6_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_6'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 9 \
    name ifm_buff0_7 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_7 \
    op interface \
    ports { ifm_buff0_7_address0 { O 6 vector } ifm_buff0_7_ce0 { O 1 bit } ifm_buff0_7_we0 { O 1 bit } ifm_buff0_7_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_7'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 10 \
    name ifm_buff0_8 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_8 \
    op interface \
    ports { ifm_buff0_8_address0 { O 6 vector } ifm_buff0_8_ce0 { O 1 bit } ifm_buff0_8_we0 { O 1 bit } ifm_buff0_8_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_8'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 11 \
    name ifm_buff0_9 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_9 \
    op interface \
    ports { ifm_buff0_9_address0 { O 6 vector } ifm_buff0_9_ce0 { O 1 bit } ifm_buff0_9_we0 { O 1 bit } ifm_buff0_9_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_9'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 12 \
    name ifm_buff0_10 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_10 \
    op interface \
    ports { ifm_buff0_10_address0 { O 6 vector } ifm_buff0_10_ce0 { O 1 bit } ifm_buff0_10_we0 { O 1 bit } ifm_buff0_10_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_10'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 13 \
    name ifm_buff0_11 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_11 \
    op interface \
    ports { ifm_buff0_11_address0 { O 6 vector } ifm_buff0_11_ce0 { O 1 bit } ifm_buff0_11_we0 { O 1 bit } ifm_buff0_11_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_11'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 14 \
    name ifm_buff0_12 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_12 \
    op interface \
    ports { ifm_buff0_12_address0 { O 6 vector } ifm_buff0_12_ce0 { O 1 bit } ifm_buff0_12_we0 { O 1 bit } ifm_buff0_12_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_12'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 15 \
    name ifm_buff0_13 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_13 \
    op interface \
    ports { ifm_buff0_13_address0 { O 6 vector } ifm_buff0_13_ce0 { O 1 bit } ifm_buff0_13_we0 { O 1 bit } ifm_buff0_13_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_13'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 16 \
    name ifm_buff0_14 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_14 \
    op interface \
    ports { ifm_buff0_14_address0 { O 6 vector } ifm_buff0_14_ce0 { O 1 bit } ifm_buff0_14_we0 { O 1 bit } ifm_buff0_14_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_14'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 17 \
    name ifm_buff0_15 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff0_15 \
    op interface \
    ports { ifm_buff0_15_address0 { O 6 vector } ifm_buff0_15_ce0 { O 1 bit } ifm_buff0_15_we0 { O 1 bit } ifm_buff0_15_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff0_15'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 18 \
    name ifm_buff1_0 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff1_0 \
    op interface \
    ports { ifm_buff1_0_address0 { O 6 vector } ifm_buff1_0_ce0 { O 1 bit } ifm_buff1_0_we0 { O 1 bit } ifm_buff1_0_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff1_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 19 \
    name ifm_buff1_1 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff1_1 \
    op interface \
    ports { ifm_buff1_1_address0 { O 6 vector } ifm_buff1_1_ce0 { O 1 bit } ifm_buff1_1_we0 { O 1 bit } ifm_buff1_1_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff1_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 20 \
    name ifm_buff1_2 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff1_2 \
    op interface \
    ports { ifm_buff1_2_address0 { O 6 vector } ifm_buff1_2_ce0 { O 1 bit } ifm_buff1_2_we0 { O 1 bit } ifm_buff1_2_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff1_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 21 \
    name ifm_buff1_3 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff1_3 \
    op interface \
    ports { ifm_buff1_3_address0 { O 6 vector } ifm_buff1_3_ce0 { O 1 bit } ifm_buff1_3_we0 { O 1 bit } ifm_buff1_3_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff1_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 22 \
    name ifm_buff1_4 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff1_4 \
    op interface \
    ports { ifm_buff1_4_address0 { O 6 vector } ifm_buff1_4_ce0 { O 1 bit } ifm_buff1_4_we0 { O 1 bit } ifm_buff1_4_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff1_4'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 23 \
    name ifm_buff1_5 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff1_5 \
    op interface \
    ports { ifm_buff1_5_address0 { O 6 vector } ifm_buff1_5_ce0 { O 1 bit } ifm_buff1_5_we0 { O 1 bit } ifm_buff1_5_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff1_5'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 24 \
    name ifm_buff1_6 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff1_6 \
    op interface \
    ports { ifm_buff1_6_address0 { O 6 vector } ifm_buff1_6_ce0 { O 1 bit } ifm_buff1_6_we0 { O 1 bit } ifm_buff1_6_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff1_6'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 25 \
    name ifm_buff1_7 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff1_7 \
    op interface \
    ports { ifm_buff1_7_address0 { O 6 vector } ifm_buff1_7_ce0 { O 1 bit } ifm_buff1_7_we0 { O 1 bit } ifm_buff1_7_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff1_7'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 26 \
    name ifm_buff1_8 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff1_8 \
    op interface \
    ports { ifm_buff1_8_address0 { O 6 vector } ifm_buff1_8_ce0 { O 1 bit } ifm_buff1_8_we0 { O 1 bit } ifm_buff1_8_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff1_8'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 27 \
    name ifm_buff1_9 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff1_9 \
    op interface \
    ports { ifm_buff1_9_address0 { O 6 vector } ifm_buff1_9_ce0 { O 1 bit } ifm_buff1_9_we0 { O 1 bit } ifm_buff1_9_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff1_9'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 28 \
    name ifm_buff1_10 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff1_10 \
    op interface \
    ports { ifm_buff1_10_address0 { O 6 vector } ifm_buff1_10_ce0 { O 1 bit } ifm_buff1_10_we0 { O 1 bit } ifm_buff1_10_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff1_10'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 29 \
    name ifm_buff1_11 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff1_11 \
    op interface \
    ports { ifm_buff1_11_address0 { O 6 vector } ifm_buff1_11_ce0 { O 1 bit } ifm_buff1_11_we0 { O 1 bit } ifm_buff1_11_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff1_11'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 30 \
    name ifm_buff1_12 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff1_12 \
    op interface \
    ports { ifm_buff1_12_address0 { O 6 vector } ifm_buff1_12_ce0 { O 1 bit } ifm_buff1_12_we0 { O 1 bit } ifm_buff1_12_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff1_12'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 31 \
    name ifm_buff1_13 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff1_13 \
    op interface \
    ports { ifm_buff1_13_address0 { O 6 vector } ifm_buff1_13_ce0 { O 1 bit } ifm_buff1_13_we0 { O 1 bit } ifm_buff1_13_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff1_13'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 32 \
    name ifm_buff1_14 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff1_14 \
    op interface \
    ports { ifm_buff1_14_address0 { O 6 vector } ifm_buff1_14_ce0 { O 1 bit } ifm_buff1_14_we0 { O 1 bit } ifm_buff1_14_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff1_14'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 33 \
    name ifm_buff1_15 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff1_15 \
    op interface \
    ports { ifm_buff1_15_address0 { O 6 vector } ifm_buff1_15_ce0 { O 1 bit } ifm_buff1_15_we0 { O 1 bit } ifm_buff1_15_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff1_15'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 34 \
    name ifm_buff2_0 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff2_0 \
    op interface \
    ports { ifm_buff2_0_address0 { O 6 vector } ifm_buff2_0_ce0 { O 1 bit } ifm_buff2_0_we0 { O 1 bit } ifm_buff2_0_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff2_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 35 \
    name ifm_buff2_1 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff2_1 \
    op interface \
    ports { ifm_buff2_1_address0 { O 6 vector } ifm_buff2_1_ce0 { O 1 bit } ifm_buff2_1_we0 { O 1 bit } ifm_buff2_1_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff2_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 36 \
    name ifm_buff2_2 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff2_2 \
    op interface \
    ports { ifm_buff2_2_address0 { O 6 vector } ifm_buff2_2_ce0 { O 1 bit } ifm_buff2_2_we0 { O 1 bit } ifm_buff2_2_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff2_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 37 \
    name ifm_buff2_3 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff2_3 \
    op interface \
    ports { ifm_buff2_3_address0 { O 6 vector } ifm_buff2_3_ce0 { O 1 bit } ifm_buff2_3_we0 { O 1 bit } ifm_buff2_3_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff2_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 38 \
    name ifm_buff2_4 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff2_4 \
    op interface \
    ports { ifm_buff2_4_address0 { O 6 vector } ifm_buff2_4_ce0 { O 1 bit } ifm_buff2_4_we0 { O 1 bit } ifm_buff2_4_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff2_4'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 39 \
    name ifm_buff2_5 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff2_5 \
    op interface \
    ports { ifm_buff2_5_address0 { O 6 vector } ifm_buff2_5_ce0 { O 1 bit } ifm_buff2_5_we0 { O 1 bit } ifm_buff2_5_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff2_5'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 40 \
    name ifm_buff2_6 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff2_6 \
    op interface \
    ports { ifm_buff2_6_address0 { O 6 vector } ifm_buff2_6_ce0 { O 1 bit } ifm_buff2_6_we0 { O 1 bit } ifm_buff2_6_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff2_6'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 41 \
    name ifm_buff2_7 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff2_7 \
    op interface \
    ports { ifm_buff2_7_address0 { O 6 vector } ifm_buff2_7_ce0 { O 1 bit } ifm_buff2_7_we0 { O 1 bit } ifm_buff2_7_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff2_7'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 42 \
    name ifm_buff2_8 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff2_8 \
    op interface \
    ports { ifm_buff2_8_address0 { O 6 vector } ifm_buff2_8_ce0 { O 1 bit } ifm_buff2_8_we0 { O 1 bit } ifm_buff2_8_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff2_8'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 43 \
    name ifm_buff2_9 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff2_9 \
    op interface \
    ports { ifm_buff2_9_address0 { O 6 vector } ifm_buff2_9_ce0 { O 1 bit } ifm_buff2_9_we0 { O 1 bit } ifm_buff2_9_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff2_9'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 44 \
    name ifm_buff2_10 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff2_10 \
    op interface \
    ports { ifm_buff2_10_address0 { O 6 vector } ifm_buff2_10_ce0 { O 1 bit } ifm_buff2_10_we0 { O 1 bit } ifm_buff2_10_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff2_10'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 45 \
    name ifm_buff2_11 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff2_11 \
    op interface \
    ports { ifm_buff2_11_address0 { O 6 vector } ifm_buff2_11_ce0 { O 1 bit } ifm_buff2_11_we0 { O 1 bit } ifm_buff2_11_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff2_11'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 46 \
    name ifm_buff2_12 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff2_12 \
    op interface \
    ports { ifm_buff2_12_address0 { O 6 vector } ifm_buff2_12_ce0 { O 1 bit } ifm_buff2_12_we0 { O 1 bit } ifm_buff2_12_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff2_12'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 47 \
    name ifm_buff2_13 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff2_13 \
    op interface \
    ports { ifm_buff2_13_address0 { O 6 vector } ifm_buff2_13_ce0 { O 1 bit } ifm_buff2_13_we0 { O 1 bit } ifm_buff2_13_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff2_13'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 48 \
    name ifm_buff2_14 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff2_14 \
    op interface \
    ports { ifm_buff2_14_address0 { O 6 vector } ifm_buff2_14_ce0 { O 1 bit } ifm_buff2_14_we0 { O 1 bit } ifm_buff2_14_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff2_14'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 49 \
    name ifm_buff2_15 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename ifm_buff2_15 \
    op interface \
    ports { ifm_buff2_15_address0 { O 6 vector } ifm_buff2_15_ce0 { O 1 bit } ifm_buff2_15_we0 { O 1 bit } ifm_buff2_15_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'ifm_buff2_15'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 1 \
    name cifm \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_cifm \
    op interface \
    ports { cifm_req_din { O 1 bit } cifm_req_full_n { I 1 bit } cifm_req_write { O 1 bit } cifm_rsp_empty_n { I 1 bit } cifm_rsp_read { O 1 bit } cifm_address { O 32 vector } cifm_datain { I 512 vector } cifm_dataout { O 512 vector } cifm_size { O 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


