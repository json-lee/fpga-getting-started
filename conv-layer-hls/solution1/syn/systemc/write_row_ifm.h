// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2018.3
// Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _write_row_ifm_HH_
#define _write_row_ifm_HH_

#include "systemc.h"
#include "AESL_pkg.h"


namespace ap_rtl {

struct write_row_ifm : public sc_module {
    // Port declarations 82
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_out< sc_logic > cifm_req_din;
    sc_in< sc_logic > cifm_req_full_n;
    sc_out< sc_logic > cifm_req_write;
    sc_in< sc_logic > cifm_rsp_empty_n;
    sc_out< sc_logic > cifm_rsp_read;
    sc_out< sc_lv<32> > cifm_address;
    sc_in< sc_lv<512> > cifm_datain;
    sc_out< sc_lv<512> > cifm_dataout;
    sc_out< sc_lv<32> > cifm_size;
    sc_out< sc_lv<6> > ifm_buff0_0_address0;
    sc_out< sc_logic > ifm_buff0_0_ce0;
    sc_out< sc_logic > ifm_buff0_0_we0;
    sc_out< sc_lv<32> > ifm_buff0_0_d0;
    sc_out< sc_lv<6> > ifm_buff0_1_address0;
    sc_out< sc_logic > ifm_buff0_1_ce0;
    sc_out< sc_logic > ifm_buff0_1_we0;
    sc_out< sc_lv<32> > ifm_buff0_1_d0;
    sc_out< sc_lv<6> > ifm_buff0_2_address0;
    sc_out< sc_logic > ifm_buff0_2_ce0;
    sc_out< sc_logic > ifm_buff0_2_we0;
    sc_out< sc_lv<32> > ifm_buff0_2_d0;
    sc_out< sc_lv<6> > ifm_buff0_3_address0;
    sc_out< sc_logic > ifm_buff0_3_ce0;
    sc_out< sc_logic > ifm_buff0_3_we0;
    sc_out< sc_lv<32> > ifm_buff0_3_d0;
    sc_out< sc_lv<6> > ifm_buff0_4_address0;
    sc_out< sc_logic > ifm_buff0_4_ce0;
    sc_out< sc_logic > ifm_buff0_4_we0;
    sc_out< sc_lv<32> > ifm_buff0_4_d0;
    sc_out< sc_lv<6> > ifm_buff0_5_address0;
    sc_out< sc_logic > ifm_buff0_5_ce0;
    sc_out< sc_logic > ifm_buff0_5_we0;
    sc_out< sc_lv<32> > ifm_buff0_5_d0;
    sc_out< sc_lv<6> > ifm_buff0_6_address0;
    sc_out< sc_logic > ifm_buff0_6_ce0;
    sc_out< sc_logic > ifm_buff0_6_we0;
    sc_out< sc_lv<32> > ifm_buff0_6_d0;
    sc_out< sc_lv<6> > ifm_buff0_7_address0;
    sc_out< sc_logic > ifm_buff0_7_ce0;
    sc_out< sc_logic > ifm_buff0_7_we0;
    sc_out< sc_lv<32> > ifm_buff0_7_d0;
    sc_out< sc_lv<6> > ifm_buff0_8_address0;
    sc_out< sc_logic > ifm_buff0_8_ce0;
    sc_out< sc_logic > ifm_buff0_8_we0;
    sc_out< sc_lv<32> > ifm_buff0_8_d0;
    sc_out< sc_lv<6> > ifm_buff0_9_address0;
    sc_out< sc_logic > ifm_buff0_9_ce0;
    sc_out< sc_logic > ifm_buff0_9_we0;
    sc_out< sc_lv<32> > ifm_buff0_9_d0;
    sc_out< sc_lv<6> > ifm_buff0_10_address0;
    sc_out< sc_logic > ifm_buff0_10_ce0;
    sc_out< sc_logic > ifm_buff0_10_we0;
    sc_out< sc_lv<32> > ifm_buff0_10_d0;
    sc_out< sc_lv<6> > ifm_buff0_11_address0;
    sc_out< sc_logic > ifm_buff0_11_ce0;
    sc_out< sc_logic > ifm_buff0_11_we0;
    sc_out< sc_lv<32> > ifm_buff0_11_d0;
    sc_out< sc_lv<6> > ifm_buff0_12_address0;
    sc_out< sc_logic > ifm_buff0_12_ce0;
    sc_out< sc_logic > ifm_buff0_12_we0;
    sc_out< sc_lv<32> > ifm_buff0_12_d0;
    sc_out< sc_lv<6> > ifm_buff0_13_address0;
    sc_out< sc_logic > ifm_buff0_13_ce0;
    sc_out< sc_logic > ifm_buff0_13_we0;
    sc_out< sc_lv<32> > ifm_buff0_13_d0;
    sc_out< sc_lv<6> > ifm_buff0_14_address0;
    sc_out< sc_logic > ifm_buff0_14_ce0;
    sc_out< sc_logic > ifm_buff0_14_we0;
    sc_out< sc_lv<32> > ifm_buff0_14_d0;
    sc_out< sc_lv<6> > ifm_buff0_15_address0;
    sc_out< sc_logic > ifm_buff0_15_ce0;
    sc_out< sc_logic > ifm_buff0_15_we0;
    sc_out< sc_lv<32> > ifm_buff0_15_d0;
    sc_in< sc_lv<32> > cifm_counter_read;
    sc_in< sc_logic > enable;
    sc_out< sc_lv<32> > ap_return;


    // Module declarations
    write_row_ifm(sc_module_name name);
    SC_HAS_PROCESS(write_row_ifm);

    ~write_row_ifm();

    sc_trace_file* mVcdFile;

    sc_signal< sc_lv<4> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_state1;
    sc_signal< sc_lv<32> > cifm_counter_reg_376;
    sc_signal< sc_lv<6> > j_reg_386;
    sc_signal< sc_lv<6> > j_reg_386_pp0_iter1_reg;
    sc_signal< sc_logic > ap_CS_fsm_pp0_stage0;
    sc_signal< bool > ap_block_state2_pp0_stage0_iter0;
    sc_signal< bool > ap_block_state3_pp0_stage0_iter1;
    sc_signal< bool > ap_block_state4_pp0_stage0_iter2;
    sc_signal< sc_lv<1> > exitcond_reg_695;
    sc_signal< sc_lv<1> > exitcond_reg_695_pp0_iter2_reg;
    sc_signal< bool > ap_block_state5_pp0_stage0_iter3;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter3;
    sc_signal< bool > ap_block_state6_pp0_stage0_iter4;
    sc_signal< bool > ap_block_pp0_stage0_11001;
    sc_signal< sc_lv<6> > j_reg_386_pp0_iter2_reg;
    sc_signal< sc_lv<6> > j_reg_386_pp0_iter3_reg;
    sc_signal< sc_lv<1> > enable_read_read_fu_144_p2;
    sc_signal< sc_lv<32> > tmp_3_fu_407_p2;
    sc_signal< sc_lv<32> > tmp_3_reg_690;
    sc_signal< sc_lv<1> > exitcond_fu_413_p2;
    sc_signal< sc_lv<1> > exitcond_reg_695_pp0_iter1_reg;
    sc_signal< sc_lv<1> > exitcond_reg_695_pp0_iter3_reg;
    sc_signal< sc_lv<6> > j_1_fu_419_p2;
    sc_signal< sc_lv<6> > j_1_reg_699;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter0;
    sc_signal< sc_lv<32> > tmp_s_fu_436_p2;
    sc_signal< sc_lv<32> > tmp_s_reg_710;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter1;
    sc_signal< sc_lv<32> > tmp_5_fu_442_p1;
    sc_signal< sc_lv<32> > tmp_5_reg_715;
    sc_signal< sc_lv<32> > cifm_a1_load_new6_reg_720;
    sc_signal< sc_lv<32> > cifm_a2_load_new_reg_725;
    sc_signal< sc_lv<32> > cifm_a3_load_new_reg_730;
    sc_signal< sc_lv<32> > cifm_a4_load_new_reg_735;
    sc_signal< sc_lv<32> > cifm_a5_load_new_reg_740;
    sc_signal< sc_lv<32> > cifm_a6_load_new_reg_745;
    sc_signal< sc_lv<32> > cifm_a7_load_new_reg_750;
    sc_signal< sc_lv<32> > cifm_a8_load_new_reg_755;
    sc_signal< sc_lv<32> > cifm_a9_load_new_reg_760;
    sc_signal< sc_lv<32> > cifm_a10_load_new_reg_765;
    sc_signal< sc_lv<32> > cifm_a11_load_new_reg_770;
    sc_signal< sc_lv<32> > cifm_a12_load_new_reg_775;
    sc_signal< sc_lv<32> > cifm_a13_load_new_reg_780;
    sc_signal< sc_lv<32> > cifm_a14_load_new_reg_785;
    sc_signal< sc_lv<32> > cifm_a15_load_new_reg_790;
    sc_signal< bool > ap_block_pp0_stage0_subdone;
    sc_signal< sc_logic > ap_condition_pp0_exit_iter0_state2;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter2;
    sc_signal< sc_logic > ap_enable_reg_pp0_iter4;
    sc_signal< sc_lv<32> > ap_phi_mux_cifm_counter_phi_fu_379_p4;
    sc_signal< bool > ap_block_pp0_stage0;
    sc_signal< sc_lv<6> > ap_phi_mux_j_phi_fu_390_p4;
    sc_signal< sc_lv<32> > cifm_counter_1_reg_398;
    sc_signal< sc_logic > ap_CS_fsm_state7;
    sc_signal< sc_lv<64> > tmp_1_fu_600_p1;
    sc_signal< sc_lv<64> > tmp_fu_425_p1;
    sc_signal< sc_lv<32> > ap_return_preg;
    sc_signal< sc_logic > ap_CS_fsm_state8;
    sc_signal< sc_lv<4> > ap_NS_fsm;
    sc_signal< sc_logic > ap_idle_pp0;
    sc_signal< sc_logic > ap_enable_pp0;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<4> ap_ST_fsm_state1;
    static const sc_lv<4> ap_ST_fsm_pp0_stage0;
    static const sc_lv<4> ap_ST_fsm_state7;
    static const sc_lv<4> ap_ST_fsm_state8;
    static const bool ap_const_boolean_1;
    static const sc_lv<32> ap_const_lv32_0;
    static const sc_lv<32> ap_const_lv32_1;
    static const sc_lv<1> ap_const_lv1_0;
    static const bool ap_const_boolean_0;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<6> ap_const_lv6_0;
    static const sc_lv<32> ap_const_lv32_2;
    static const sc_lv<32> ap_const_lv32_3A;
    static const sc_lv<6> ap_const_lv6_3A;
    static const sc_lv<6> ap_const_lv6_1;
    static const sc_lv<32> ap_const_lv32_20;
    static const sc_lv<32> ap_const_lv32_3F;
    static const sc_lv<32> ap_const_lv32_40;
    static const sc_lv<32> ap_const_lv32_5F;
    static const sc_lv<32> ap_const_lv32_60;
    static const sc_lv<32> ap_const_lv32_7F;
    static const sc_lv<32> ap_const_lv32_80;
    static const sc_lv<32> ap_const_lv32_9F;
    static const sc_lv<32> ap_const_lv32_A0;
    static const sc_lv<32> ap_const_lv32_BF;
    static const sc_lv<32> ap_const_lv32_C0;
    static const sc_lv<32> ap_const_lv32_DF;
    static const sc_lv<32> ap_const_lv32_E0;
    static const sc_lv<32> ap_const_lv32_FF;
    static const sc_lv<32> ap_const_lv32_100;
    static const sc_lv<32> ap_const_lv32_11F;
    static const sc_lv<32> ap_const_lv32_120;
    static const sc_lv<32> ap_const_lv32_13F;
    static const sc_lv<32> ap_const_lv32_140;
    static const sc_lv<32> ap_const_lv32_15F;
    static const sc_lv<32> ap_const_lv32_160;
    static const sc_lv<32> ap_const_lv32_17F;
    static const sc_lv<32> ap_const_lv32_180;
    static const sc_lv<32> ap_const_lv32_19F;
    static const sc_lv<32> ap_const_lv32_1A0;
    static const sc_lv<32> ap_const_lv32_1BF;
    static const sc_lv<32> ap_const_lv32_1C0;
    static const sc_lv<32> ap_const_lv32_1DF;
    static const sc_lv<32> ap_const_lv32_1E0;
    static const sc_lv<32> ap_const_lv32_1FF;
    static const sc_lv<32> ap_const_lv32_3;
    static const sc_lv<512> ap_const_lv512_lc_1;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_ap_CS_fsm_pp0_stage0();
    void thread_ap_CS_fsm_state1();
    void thread_ap_CS_fsm_state7();
    void thread_ap_CS_fsm_state8();
    void thread_ap_block_pp0_stage0();
    void thread_ap_block_pp0_stage0_11001();
    void thread_ap_block_pp0_stage0_subdone();
    void thread_ap_block_state2_pp0_stage0_iter0();
    void thread_ap_block_state3_pp0_stage0_iter1();
    void thread_ap_block_state4_pp0_stage0_iter2();
    void thread_ap_block_state5_pp0_stage0_iter3();
    void thread_ap_block_state6_pp0_stage0_iter4();
    void thread_ap_condition_pp0_exit_iter0_state2();
    void thread_ap_done();
    void thread_ap_enable_pp0();
    void thread_ap_idle();
    void thread_ap_idle_pp0();
    void thread_ap_phi_mux_cifm_counter_phi_fu_379_p4();
    void thread_ap_phi_mux_j_phi_fu_390_p4();
    void thread_ap_ready();
    void thread_ap_return();
    void thread_cifm_address();
    void thread_cifm_dataout();
    void thread_cifm_req_din();
    void thread_cifm_req_write();
    void thread_cifm_rsp_read();
    void thread_cifm_size();
    void thread_enable_read_read_fu_144_p2();
    void thread_exitcond_fu_413_p2();
    void thread_ifm_buff0_0_address0();
    void thread_ifm_buff0_0_ce0();
    void thread_ifm_buff0_0_d0();
    void thread_ifm_buff0_0_we0();
    void thread_ifm_buff0_10_address0();
    void thread_ifm_buff0_10_ce0();
    void thread_ifm_buff0_10_d0();
    void thread_ifm_buff0_10_we0();
    void thread_ifm_buff0_11_address0();
    void thread_ifm_buff0_11_ce0();
    void thread_ifm_buff0_11_d0();
    void thread_ifm_buff0_11_we0();
    void thread_ifm_buff0_12_address0();
    void thread_ifm_buff0_12_ce0();
    void thread_ifm_buff0_12_d0();
    void thread_ifm_buff0_12_we0();
    void thread_ifm_buff0_13_address0();
    void thread_ifm_buff0_13_ce0();
    void thread_ifm_buff0_13_d0();
    void thread_ifm_buff0_13_we0();
    void thread_ifm_buff0_14_address0();
    void thread_ifm_buff0_14_ce0();
    void thread_ifm_buff0_14_d0();
    void thread_ifm_buff0_14_we0();
    void thread_ifm_buff0_15_address0();
    void thread_ifm_buff0_15_ce0();
    void thread_ifm_buff0_15_d0();
    void thread_ifm_buff0_15_we0();
    void thread_ifm_buff0_1_address0();
    void thread_ifm_buff0_1_ce0();
    void thread_ifm_buff0_1_d0();
    void thread_ifm_buff0_1_we0();
    void thread_ifm_buff0_2_address0();
    void thread_ifm_buff0_2_ce0();
    void thread_ifm_buff0_2_d0();
    void thread_ifm_buff0_2_we0();
    void thread_ifm_buff0_3_address0();
    void thread_ifm_buff0_3_ce0();
    void thread_ifm_buff0_3_d0();
    void thread_ifm_buff0_3_we0();
    void thread_ifm_buff0_4_address0();
    void thread_ifm_buff0_4_ce0();
    void thread_ifm_buff0_4_d0();
    void thread_ifm_buff0_4_we0();
    void thread_ifm_buff0_5_address0();
    void thread_ifm_buff0_5_ce0();
    void thread_ifm_buff0_5_d0();
    void thread_ifm_buff0_5_we0();
    void thread_ifm_buff0_6_address0();
    void thread_ifm_buff0_6_ce0();
    void thread_ifm_buff0_6_d0();
    void thread_ifm_buff0_6_we0();
    void thread_ifm_buff0_7_address0();
    void thread_ifm_buff0_7_ce0();
    void thread_ifm_buff0_7_d0();
    void thread_ifm_buff0_7_we0();
    void thread_ifm_buff0_8_address0();
    void thread_ifm_buff0_8_ce0();
    void thread_ifm_buff0_8_d0();
    void thread_ifm_buff0_8_we0();
    void thread_ifm_buff0_9_address0();
    void thread_ifm_buff0_9_ce0();
    void thread_ifm_buff0_9_d0();
    void thread_ifm_buff0_9_we0();
    void thread_j_1_fu_419_p2();
    void thread_tmp_1_fu_600_p1();
    void thread_tmp_3_fu_407_p2();
    void thread_tmp_5_fu_442_p1();
    void thread_tmp_fu_425_p1();
    void thread_tmp_s_fu_436_p2();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
