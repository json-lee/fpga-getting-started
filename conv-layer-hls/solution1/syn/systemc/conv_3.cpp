#include "conv.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void conv::thread_hdltv_gen() {
    const char* dump_tv = std::getenv("AP_WRITE_TV");
    if (!(dump_tv && string(dump_tv) == "on")) return;

    wait();

    mHdltvinHandle << "[ " << endl;
    mHdltvoutHandle << "[ " << endl;
    int ap_cycleNo = 0;
    while (1) {
        wait();
        const char* mComma = ap_cycleNo == 0 ? " " : ", " ;
        mHdltvinHandle << mComma << "{"  <<  " \"ap_rst\" :  \"" << ap_rst.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"ap_start\" :  \"" << ap_start.read() << "\" ";
        mHdltvoutHandle << mComma << "{"  <<  " \"ap_done\" :  \"" << ap_done.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"ap_idle\" :  \"" << ap_idle.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"ap_ready\" :  \"" << ap_ready.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"cifm_req_din\" :  \"" << cifm_req_din.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"cifm_req_full_n\" :  \"" << cifm_req_full_n.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"cifm_req_write\" :  \"" << cifm_req_write.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"cifm_rsp_empty_n\" :  \"" << cifm_rsp_empty_n.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"cifm_rsp_read\" :  \"" << cifm_rsp_read.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"cifm_address\" :  \"" << cifm_address.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"cifm_datain\" :  \"" << cifm_datain.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"cifm_dataout\" :  \"" << cifm_dataout.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"cifm_size\" :  \"" << cifm_size.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"cofm_req_din\" :  \"" << cofm_req_din.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"cofm_req_full_n\" :  \"" << cofm_req_full_n.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"cofm_req_write\" :  \"" << cofm_req_write.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"cofm_rsp_empty_n\" :  \"" << cofm_rsp_empty_n.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"cofm_rsp_read\" :  \"" << cofm_rsp_read.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"cofm_address\" :  \"" << cofm_address.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"cofm_datain\" :  \"" << cofm_datain.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"cofm_dataout\" :  \"" << cofm_dataout.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"cofm_size\" :  \"" << cofm_size.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"tran_wgt_req_din\" :  \"" << tran_wgt_req_din.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"tran_wgt_req_full_n\" :  \"" << tran_wgt_req_full_n.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"tran_wgt_req_write\" :  \"" << tran_wgt_req_write.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"tran_wgt_rsp_empty_n\" :  \"" << tran_wgt_rsp_empty_n.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"tran_wgt_rsp_read\" :  \"" << tran_wgt_rsp_read.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"tran_wgt_address\" :  \"" << tran_wgt_address.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"tran_wgt_datain\" :  \"" << tran_wgt_datain.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"tran_wgt_dataout\" :  \"" << tran_wgt_dataout.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"tran_wgt_size\" :  \"" << tran_wgt_size.read() << "\" ";
        mHdltvinHandle << "}" << std::endl;
        mHdltvoutHandle << "}" << std::endl;
        ap_cycleNo++;
    }
}

}

