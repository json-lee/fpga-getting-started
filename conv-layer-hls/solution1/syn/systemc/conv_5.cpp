#include "conv.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void conv::thread_ifm_buff2_9_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op306_call_state4.read()))) {
        ifm_buff2_9_we0 = grp_write_row_ifm_fu_1365_ifm_buff0_9_we0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff2_9_we0 = grp_load_cifm_data_fu_1263_ifm_buff1_9_we0.read();
    } else {
        ifm_buff2_9_we0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_0_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_0_address0 = grp_write_row_ifm_fu_1365_ifm_buff0_0_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_0_address0 = grp_load_cifm_data_fu_1263_ifm_buff2_0_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_0_address0 = grp_conv_write_fu_1051_ifm_buff2_0_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_0_address0 = grp_conv_write_fu_1051_ifm_buff1_0_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_0_address0 = grp_conv_write_fu_1051_ifm_buff0_0_address0.read();
    } else {
        ifm_buff3_0_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_0_address1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_0_address1 = grp_conv_write_fu_1051_ifm_buff2_0_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_0_address1 = grp_conv_write_fu_1051_ifm_buff1_0_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_0_address1 = grp_conv_write_fu_1051_ifm_buff0_0_address1.read();
        } else {
            ifm_buff3_0_address1 =  (sc_lv<6>) ("XXXXXX");
        }
    } else {
        ifm_buff3_0_address1 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_0_ce0 = grp_write_row_ifm_fu_1365_ifm_buff0_0_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_0_ce0 = grp_load_cifm_data_fu_1263_ifm_buff2_0_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_0_ce0 = grp_conv_write_fu_1051_ifm_buff2_0_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_0_ce0 = grp_conv_write_fu_1051_ifm_buff1_0_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_0_ce0 = grp_conv_write_fu_1051_ifm_buff0_0_ce0.read();
    } else {
        ifm_buff3_0_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_0_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_0_ce1 = grp_conv_write_fu_1051_ifm_buff2_0_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_0_ce1 = grp_conv_write_fu_1051_ifm_buff1_0_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_0_ce1 = grp_conv_write_fu_1051_ifm_buff0_0_ce1.read();
        } else {
            ifm_buff3_0_ce1 = ap_const_logic_0;
        }
    } else {
        ifm_buff3_0_ce1 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_0_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_0_d0 = grp_write_row_ifm_fu_1365_ifm_buff0_0_d0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_0_d0 = grp_load_cifm_data_fu_1263_ifm_buff2_0_d0.read();
    } else {
        ifm_buff3_0_d0 = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    }
}

void conv::thread_ifm_buff3_0_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_0_we0 = grp_write_row_ifm_fu_1365_ifm_buff0_0_we0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_0_we0 = grp_load_cifm_data_fu_1263_ifm_buff2_0_we0.read();
    } else {
        ifm_buff3_0_we0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_10_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_10_address0 = grp_write_row_ifm_fu_1365_ifm_buff0_10_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_10_address0 = grp_load_cifm_data_fu_1263_ifm_buff2_10_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_10_address0 = grp_conv_write_fu_1051_ifm_buff2_10_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_10_address0 = grp_conv_write_fu_1051_ifm_buff1_10_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_10_address0 = grp_conv_write_fu_1051_ifm_buff0_10_address0.read();
    } else {
        ifm_buff3_10_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_10_address1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_10_address1 = grp_conv_write_fu_1051_ifm_buff2_10_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_10_address1 = grp_conv_write_fu_1051_ifm_buff1_10_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_10_address1 = grp_conv_write_fu_1051_ifm_buff0_10_address1.read();
        } else {
            ifm_buff3_10_address1 =  (sc_lv<6>) ("XXXXXX");
        }
    } else {
        ifm_buff3_10_address1 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_10_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_10_ce0 = grp_write_row_ifm_fu_1365_ifm_buff0_10_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_10_ce0 = grp_load_cifm_data_fu_1263_ifm_buff2_10_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_10_ce0 = grp_conv_write_fu_1051_ifm_buff2_10_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_10_ce0 = grp_conv_write_fu_1051_ifm_buff1_10_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_10_ce0 = grp_conv_write_fu_1051_ifm_buff0_10_ce0.read();
    } else {
        ifm_buff3_10_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_10_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_10_ce1 = grp_conv_write_fu_1051_ifm_buff2_10_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_10_ce1 = grp_conv_write_fu_1051_ifm_buff1_10_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_10_ce1 = grp_conv_write_fu_1051_ifm_buff0_10_ce1.read();
        } else {
            ifm_buff3_10_ce1 = ap_const_logic_0;
        }
    } else {
        ifm_buff3_10_ce1 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_10_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_10_d0 = grp_write_row_ifm_fu_1365_ifm_buff0_10_d0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_10_d0 = grp_load_cifm_data_fu_1263_ifm_buff2_10_d0.read();
    } else {
        ifm_buff3_10_d0 = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    }
}

void conv::thread_ifm_buff3_10_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_10_we0 = grp_write_row_ifm_fu_1365_ifm_buff0_10_we0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_10_we0 = grp_load_cifm_data_fu_1263_ifm_buff2_10_we0.read();
    } else {
        ifm_buff3_10_we0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_11_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_11_address0 = grp_write_row_ifm_fu_1365_ifm_buff0_11_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_11_address0 = grp_load_cifm_data_fu_1263_ifm_buff2_11_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_11_address0 = grp_conv_write_fu_1051_ifm_buff2_11_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_11_address0 = grp_conv_write_fu_1051_ifm_buff1_11_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_11_address0 = grp_conv_write_fu_1051_ifm_buff0_11_address0.read();
    } else {
        ifm_buff3_11_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_11_address1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_11_address1 = grp_conv_write_fu_1051_ifm_buff2_11_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_11_address1 = grp_conv_write_fu_1051_ifm_buff1_11_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_11_address1 = grp_conv_write_fu_1051_ifm_buff0_11_address1.read();
        } else {
            ifm_buff3_11_address1 =  (sc_lv<6>) ("XXXXXX");
        }
    } else {
        ifm_buff3_11_address1 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_11_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_11_ce0 = grp_write_row_ifm_fu_1365_ifm_buff0_11_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_11_ce0 = grp_load_cifm_data_fu_1263_ifm_buff2_11_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_11_ce0 = grp_conv_write_fu_1051_ifm_buff2_11_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_11_ce0 = grp_conv_write_fu_1051_ifm_buff1_11_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_11_ce0 = grp_conv_write_fu_1051_ifm_buff0_11_ce0.read();
    } else {
        ifm_buff3_11_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_11_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_11_ce1 = grp_conv_write_fu_1051_ifm_buff2_11_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_11_ce1 = grp_conv_write_fu_1051_ifm_buff1_11_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_11_ce1 = grp_conv_write_fu_1051_ifm_buff0_11_ce1.read();
        } else {
            ifm_buff3_11_ce1 = ap_const_logic_0;
        }
    } else {
        ifm_buff3_11_ce1 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_11_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_11_d0 = grp_write_row_ifm_fu_1365_ifm_buff0_11_d0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_11_d0 = grp_load_cifm_data_fu_1263_ifm_buff2_11_d0.read();
    } else {
        ifm_buff3_11_d0 = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    }
}

void conv::thread_ifm_buff3_11_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_11_we0 = grp_write_row_ifm_fu_1365_ifm_buff0_11_we0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_11_we0 = grp_load_cifm_data_fu_1263_ifm_buff2_11_we0.read();
    } else {
        ifm_buff3_11_we0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_12_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_12_address0 = grp_write_row_ifm_fu_1365_ifm_buff0_12_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_12_address0 = grp_load_cifm_data_fu_1263_ifm_buff2_12_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_12_address0 = grp_conv_write_fu_1051_ifm_buff2_12_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_12_address0 = grp_conv_write_fu_1051_ifm_buff1_12_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_12_address0 = grp_conv_write_fu_1051_ifm_buff0_12_address0.read();
    } else {
        ifm_buff3_12_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_12_address1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_12_address1 = grp_conv_write_fu_1051_ifm_buff2_12_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_12_address1 = grp_conv_write_fu_1051_ifm_buff1_12_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_12_address1 = grp_conv_write_fu_1051_ifm_buff0_12_address1.read();
        } else {
            ifm_buff3_12_address1 =  (sc_lv<6>) ("XXXXXX");
        }
    } else {
        ifm_buff3_12_address1 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_12_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_12_ce0 = grp_write_row_ifm_fu_1365_ifm_buff0_12_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_12_ce0 = grp_load_cifm_data_fu_1263_ifm_buff2_12_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_12_ce0 = grp_conv_write_fu_1051_ifm_buff2_12_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_12_ce0 = grp_conv_write_fu_1051_ifm_buff1_12_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_12_ce0 = grp_conv_write_fu_1051_ifm_buff0_12_ce0.read();
    } else {
        ifm_buff3_12_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_12_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_12_ce1 = grp_conv_write_fu_1051_ifm_buff2_12_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_12_ce1 = grp_conv_write_fu_1051_ifm_buff1_12_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_12_ce1 = grp_conv_write_fu_1051_ifm_buff0_12_ce1.read();
        } else {
            ifm_buff3_12_ce1 = ap_const_logic_0;
        }
    } else {
        ifm_buff3_12_ce1 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_12_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_12_d0 = grp_write_row_ifm_fu_1365_ifm_buff0_12_d0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_12_d0 = grp_load_cifm_data_fu_1263_ifm_buff2_12_d0.read();
    } else {
        ifm_buff3_12_d0 = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    }
}

void conv::thread_ifm_buff3_12_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_12_we0 = grp_write_row_ifm_fu_1365_ifm_buff0_12_we0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_12_we0 = grp_load_cifm_data_fu_1263_ifm_buff2_12_we0.read();
    } else {
        ifm_buff3_12_we0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_13_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_13_address0 = grp_write_row_ifm_fu_1365_ifm_buff0_13_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_13_address0 = grp_load_cifm_data_fu_1263_ifm_buff2_13_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_13_address0 = grp_conv_write_fu_1051_ifm_buff2_13_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_13_address0 = grp_conv_write_fu_1051_ifm_buff1_13_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_13_address0 = grp_conv_write_fu_1051_ifm_buff0_13_address0.read();
    } else {
        ifm_buff3_13_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_13_address1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_13_address1 = grp_conv_write_fu_1051_ifm_buff2_13_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_13_address1 = grp_conv_write_fu_1051_ifm_buff1_13_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_13_address1 = grp_conv_write_fu_1051_ifm_buff0_13_address1.read();
        } else {
            ifm_buff3_13_address1 =  (sc_lv<6>) ("XXXXXX");
        }
    } else {
        ifm_buff3_13_address1 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_13_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_13_ce0 = grp_write_row_ifm_fu_1365_ifm_buff0_13_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_13_ce0 = grp_load_cifm_data_fu_1263_ifm_buff2_13_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_13_ce0 = grp_conv_write_fu_1051_ifm_buff2_13_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_13_ce0 = grp_conv_write_fu_1051_ifm_buff1_13_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_13_ce0 = grp_conv_write_fu_1051_ifm_buff0_13_ce0.read();
    } else {
        ifm_buff3_13_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_13_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_13_ce1 = grp_conv_write_fu_1051_ifm_buff2_13_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_13_ce1 = grp_conv_write_fu_1051_ifm_buff1_13_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_13_ce1 = grp_conv_write_fu_1051_ifm_buff0_13_ce1.read();
        } else {
            ifm_buff3_13_ce1 = ap_const_logic_0;
        }
    } else {
        ifm_buff3_13_ce1 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_13_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_13_d0 = grp_write_row_ifm_fu_1365_ifm_buff0_13_d0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_13_d0 = grp_load_cifm_data_fu_1263_ifm_buff2_13_d0.read();
    } else {
        ifm_buff3_13_d0 = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    }
}

void conv::thread_ifm_buff3_13_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_13_we0 = grp_write_row_ifm_fu_1365_ifm_buff0_13_we0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_13_we0 = grp_load_cifm_data_fu_1263_ifm_buff2_13_we0.read();
    } else {
        ifm_buff3_13_we0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_14_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_14_address0 = grp_write_row_ifm_fu_1365_ifm_buff0_14_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_14_address0 = grp_load_cifm_data_fu_1263_ifm_buff2_14_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_14_address0 = grp_conv_write_fu_1051_ifm_buff2_14_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_14_address0 = grp_conv_write_fu_1051_ifm_buff1_14_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_14_address0 = grp_conv_write_fu_1051_ifm_buff0_14_address0.read();
    } else {
        ifm_buff3_14_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_14_address1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_14_address1 = grp_conv_write_fu_1051_ifm_buff2_14_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_14_address1 = grp_conv_write_fu_1051_ifm_buff1_14_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_14_address1 = grp_conv_write_fu_1051_ifm_buff0_14_address1.read();
        } else {
            ifm_buff3_14_address1 =  (sc_lv<6>) ("XXXXXX");
        }
    } else {
        ifm_buff3_14_address1 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_14_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_14_ce0 = grp_write_row_ifm_fu_1365_ifm_buff0_14_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_14_ce0 = grp_load_cifm_data_fu_1263_ifm_buff2_14_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_14_ce0 = grp_conv_write_fu_1051_ifm_buff2_14_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_14_ce0 = grp_conv_write_fu_1051_ifm_buff1_14_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_14_ce0 = grp_conv_write_fu_1051_ifm_buff0_14_ce0.read();
    } else {
        ifm_buff3_14_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_14_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_14_ce1 = grp_conv_write_fu_1051_ifm_buff2_14_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_14_ce1 = grp_conv_write_fu_1051_ifm_buff1_14_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_14_ce1 = grp_conv_write_fu_1051_ifm_buff0_14_ce1.read();
        } else {
            ifm_buff3_14_ce1 = ap_const_logic_0;
        }
    } else {
        ifm_buff3_14_ce1 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_14_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_14_d0 = grp_write_row_ifm_fu_1365_ifm_buff0_14_d0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_14_d0 = grp_load_cifm_data_fu_1263_ifm_buff2_14_d0.read();
    } else {
        ifm_buff3_14_d0 = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    }
}

void conv::thread_ifm_buff3_14_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_14_we0 = grp_write_row_ifm_fu_1365_ifm_buff0_14_we0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_14_we0 = grp_load_cifm_data_fu_1263_ifm_buff2_14_we0.read();
    } else {
        ifm_buff3_14_we0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_15_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_15_address0 = grp_write_row_ifm_fu_1365_ifm_buff0_15_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_15_address0 = grp_load_cifm_data_fu_1263_ifm_buff2_15_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_15_address0 = grp_conv_write_fu_1051_ifm_buff2_15_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_15_address0 = grp_conv_write_fu_1051_ifm_buff1_15_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_15_address0 = grp_conv_write_fu_1051_ifm_buff0_15_address0.read();
    } else {
        ifm_buff3_15_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_15_address1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_15_address1 = grp_conv_write_fu_1051_ifm_buff2_15_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_15_address1 = grp_conv_write_fu_1051_ifm_buff1_15_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_15_address1 = grp_conv_write_fu_1051_ifm_buff0_15_address1.read();
        } else {
            ifm_buff3_15_address1 =  (sc_lv<6>) ("XXXXXX");
        }
    } else {
        ifm_buff3_15_address1 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_15_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_15_ce0 = grp_write_row_ifm_fu_1365_ifm_buff0_15_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_15_ce0 = grp_load_cifm_data_fu_1263_ifm_buff2_15_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_15_ce0 = grp_conv_write_fu_1051_ifm_buff2_15_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_15_ce0 = grp_conv_write_fu_1051_ifm_buff1_15_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_15_ce0 = grp_conv_write_fu_1051_ifm_buff0_15_ce0.read();
    } else {
        ifm_buff3_15_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_15_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_15_ce1 = grp_conv_write_fu_1051_ifm_buff2_15_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_15_ce1 = grp_conv_write_fu_1051_ifm_buff1_15_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_15_ce1 = grp_conv_write_fu_1051_ifm_buff0_15_ce1.read();
        } else {
            ifm_buff3_15_ce1 = ap_const_logic_0;
        }
    } else {
        ifm_buff3_15_ce1 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_15_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_15_d0 = grp_write_row_ifm_fu_1365_ifm_buff0_15_d0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_15_d0 = grp_load_cifm_data_fu_1263_ifm_buff2_15_d0.read();
    } else {
        ifm_buff3_15_d0 = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    }
}

void conv::thread_ifm_buff3_15_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_15_we0 = grp_write_row_ifm_fu_1365_ifm_buff0_15_we0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_15_we0 = grp_load_cifm_data_fu_1263_ifm_buff2_15_we0.read();
    } else {
        ifm_buff3_15_we0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_1_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_1_address0 = grp_write_row_ifm_fu_1365_ifm_buff0_1_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_1_address0 = grp_load_cifm_data_fu_1263_ifm_buff2_1_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_1_address0 = grp_conv_write_fu_1051_ifm_buff2_1_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_1_address0 = grp_conv_write_fu_1051_ifm_buff1_1_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_1_address0 = grp_conv_write_fu_1051_ifm_buff0_1_address0.read();
    } else {
        ifm_buff3_1_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_1_address1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_1_address1 = grp_conv_write_fu_1051_ifm_buff2_1_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_1_address1 = grp_conv_write_fu_1051_ifm_buff1_1_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_1_address1 = grp_conv_write_fu_1051_ifm_buff0_1_address1.read();
        } else {
            ifm_buff3_1_address1 =  (sc_lv<6>) ("XXXXXX");
        }
    } else {
        ifm_buff3_1_address1 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_1_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_1_ce0 = grp_write_row_ifm_fu_1365_ifm_buff0_1_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_1_ce0 = grp_load_cifm_data_fu_1263_ifm_buff2_1_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_1_ce0 = grp_conv_write_fu_1051_ifm_buff2_1_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_1_ce0 = grp_conv_write_fu_1051_ifm_buff1_1_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_1_ce0 = grp_conv_write_fu_1051_ifm_buff0_1_ce0.read();
    } else {
        ifm_buff3_1_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_1_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_1_ce1 = grp_conv_write_fu_1051_ifm_buff2_1_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_1_ce1 = grp_conv_write_fu_1051_ifm_buff1_1_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_1_ce1 = grp_conv_write_fu_1051_ifm_buff0_1_ce1.read();
        } else {
            ifm_buff3_1_ce1 = ap_const_logic_0;
        }
    } else {
        ifm_buff3_1_ce1 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_1_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_1_d0 = grp_write_row_ifm_fu_1365_ifm_buff0_1_d0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_1_d0 = grp_load_cifm_data_fu_1263_ifm_buff2_1_d0.read();
    } else {
        ifm_buff3_1_d0 = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    }
}

void conv::thread_ifm_buff3_1_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_1_we0 = grp_write_row_ifm_fu_1365_ifm_buff0_1_we0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_1_we0 = grp_load_cifm_data_fu_1263_ifm_buff2_1_we0.read();
    } else {
        ifm_buff3_1_we0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_2_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_2_address0 = grp_write_row_ifm_fu_1365_ifm_buff0_2_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_2_address0 = grp_load_cifm_data_fu_1263_ifm_buff2_2_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_2_address0 = grp_conv_write_fu_1051_ifm_buff2_2_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_2_address0 = grp_conv_write_fu_1051_ifm_buff1_2_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_2_address0 = grp_conv_write_fu_1051_ifm_buff0_2_address0.read();
    } else {
        ifm_buff3_2_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_2_address1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_2_address1 = grp_conv_write_fu_1051_ifm_buff2_2_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_2_address1 = grp_conv_write_fu_1051_ifm_buff1_2_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_2_address1 = grp_conv_write_fu_1051_ifm_buff0_2_address1.read();
        } else {
            ifm_buff3_2_address1 =  (sc_lv<6>) ("XXXXXX");
        }
    } else {
        ifm_buff3_2_address1 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_2_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_2_ce0 = grp_write_row_ifm_fu_1365_ifm_buff0_2_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_2_ce0 = grp_load_cifm_data_fu_1263_ifm_buff2_2_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_2_ce0 = grp_conv_write_fu_1051_ifm_buff2_2_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_2_ce0 = grp_conv_write_fu_1051_ifm_buff1_2_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_2_ce0 = grp_conv_write_fu_1051_ifm_buff0_2_ce0.read();
    } else {
        ifm_buff3_2_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_2_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_2_ce1 = grp_conv_write_fu_1051_ifm_buff2_2_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_2_ce1 = grp_conv_write_fu_1051_ifm_buff1_2_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_2_ce1 = grp_conv_write_fu_1051_ifm_buff0_2_ce1.read();
        } else {
            ifm_buff3_2_ce1 = ap_const_logic_0;
        }
    } else {
        ifm_buff3_2_ce1 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_2_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_2_d0 = grp_write_row_ifm_fu_1365_ifm_buff0_2_d0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_2_d0 = grp_load_cifm_data_fu_1263_ifm_buff2_2_d0.read();
    } else {
        ifm_buff3_2_d0 = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    }
}

void conv::thread_ifm_buff3_2_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_2_we0 = grp_write_row_ifm_fu_1365_ifm_buff0_2_we0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_2_we0 = grp_load_cifm_data_fu_1263_ifm_buff2_2_we0.read();
    } else {
        ifm_buff3_2_we0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_3_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_3_address0 = grp_write_row_ifm_fu_1365_ifm_buff0_3_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_3_address0 = grp_load_cifm_data_fu_1263_ifm_buff2_3_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_3_address0 = grp_conv_write_fu_1051_ifm_buff2_3_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_3_address0 = grp_conv_write_fu_1051_ifm_buff1_3_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_3_address0 = grp_conv_write_fu_1051_ifm_buff0_3_address0.read();
    } else {
        ifm_buff3_3_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_3_address1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_3_address1 = grp_conv_write_fu_1051_ifm_buff2_3_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_3_address1 = grp_conv_write_fu_1051_ifm_buff1_3_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_3_address1 = grp_conv_write_fu_1051_ifm_buff0_3_address1.read();
        } else {
            ifm_buff3_3_address1 =  (sc_lv<6>) ("XXXXXX");
        }
    } else {
        ifm_buff3_3_address1 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_3_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_3_ce0 = grp_write_row_ifm_fu_1365_ifm_buff0_3_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_3_ce0 = grp_load_cifm_data_fu_1263_ifm_buff2_3_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_3_ce0 = grp_conv_write_fu_1051_ifm_buff2_3_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_3_ce0 = grp_conv_write_fu_1051_ifm_buff1_3_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_3_ce0 = grp_conv_write_fu_1051_ifm_buff0_3_ce0.read();
    } else {
        ifm_buff3_3_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_3_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_3_ce1 = grp_conv_write_fu_1051_ifm_buff2_3_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_3_ce1 = grp_conv_write_fu_1051_ifm_buff1_3_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_3_ce1 = grp_conv_write_fu_1051_ifm_buff0_3_ce1.read();
        } else {
            ifm_buff3_3_ce1 = ap_const_logic_0;
        }
    } else {
        ifm_buff3_3_ce1 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_3_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_3_d0 = grp_write_row_ifm_fu_1365_ifm_buff0_3_d0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_3_d0 = grp_load_cifm_data_fu_1263_ifm_buff2_3_d0.read();
    } else {
        ifm_buff3_3_d0 = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    }
}

void conv::thread_ifm_buff3_3_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_3_we0 = grp_write_row_ifm_fu_1365_ifm_buff0_3_we0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_3_we0 = grp_load_cifm_data_fu_1263_ifm_buff2_3_we0.read();
    } else {
        ifm_buff3_3_we0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_4_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_4_address0 = grp_write_row_ifm_fu_1365_ifm_buff0_4_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_4_address0 = grp_load_cifm_data_fu_1263_ifm_buff2_4_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_4_address0 = grp_conv_write_fu_1051_ifm_buff2_4_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_4_address0 = grp_conv_write_fu_1051_ifm_buff1_4_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_4_address0 = grp_conv_write_fu_1051_ifm_buff0_4_address0.read();
    } else {
        ifm_buff3_4_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_4_address1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_4_address1 = grp_conv_write_fu_1051_ifm_buff2_4_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_4_address1 = grp_conv_write_fu_1051_ifm_buff1_4_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_4_address1 = grp_conv_write_fu_1051_ifm_buff0_4_address1.read();
        } else {
            ifm_buff3_4_address1 =  (sc_lv<6>) ("XXXXXX");
        }
    } else {
        ifm_buff3_4_address1 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_4_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_4_ce0 = grp_write_row_ifm_fu_1365_ifm_buff0_4_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_4_ce0 = grp_load_cifm_data_fu_1263_ifm_buff2_4_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_4_ce0 = grp_conv_write_fu_1051_ifm_buff2_4_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_4_ce0 = grp_conv_write_fu_1051_ifm_buff1_4_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_4_ce0 = grp_conv_write_fu_1051_ifm_buff0_4_ce0.read();
    } else {
        ifm_buff3_4_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_4_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_4_ce1 = grp_conv_write_fu_1051_ifm_buff2_4_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_4_ce1 = grp_conv_write_fu_1051_ifm_buff1_4_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_4_ce1 = grp_conv_write_fu_1051_ifm_buff0_4_ce1.read();
        } else {
            ifm_buff3_4_ce1 = ap_const_logic_0;
        }
    } else {
        ifm_buff3_4_ce1 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_4_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_4_d0 = grp_write_row_ifm_fu_1365_ifm_buff0_4_d0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_4_d0 = grp_load_cifm_data_fu_1263_ifm_buff2_4_d0.read();
    } else {
        ifm_buff3_4_d0 = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    }
}

void conv::thread_ifm_buff3_4_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_4_we0 = grp_write_row_ifm_fu_1365_ifm_buff0_4_we0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_4_we0 = grp_load_cifm_data_fu_1263_ifm_buff2_4_we0.read();
    } else {
        ifm_buff3_4_we0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_5_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_5_address0 = grp_write_row_ifm_fu_1365_ifm_buff0_5_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_5_address0 = grp_load_cifm_data_fu_1263_ifm_buff2_5_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_5_address0 = grp_conv_write_fu_1051_ifm_buff2_5_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_5_address0 = grp_conv_write_fu_1051_ifm_buff1_5_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_5_address0 = grp_conv_write_fu_1051_ifm_buff0_5_address0.read();
    } else {
        ifm_buff3_5_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_5_address1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_5_address1 = grp_conv_write_fu_1051_ifm_buff2_5_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_5_address1 = grp_conv_write_fu_1051_ifm_buff1_5_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_5_address1 = grp_conv_write_fu_1051_ifm_buff0_5_address1.read();
        } else {
            ifm_buff3_5_address1 =  (sc_lv<6>) ("XXXXXX");
        }
    } else {
        ifm_buff3_5_address1 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_5_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_5_ce0 = grp_write_row_ifm_fu_1365_ifm_buff0_5_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_5_ce0 = grp_load_cifm_data_fu_1263_ifm_buff2_5_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_5_ce0 = grp_conv_write_fu_1051_ifm_buff2_5_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_5_ce0 = grp_conv_write_fu_1051_ifm_buff1_5_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_5_ce0 = grp_conv_write_fu_1051_ifm_buff0_5_ce0.read();
    } else {
        ifm_buff3_5_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_5_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_5_ce1 = grp_conv_write_fu_1051_ifm_buff2_5_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_5_ce1 = grp_conv_write_fu_1051_ifm_buff1_5_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_5_ce1 = grp_conv_write_fu_1051_ifm_buff0_5_ce1.read();
        } else {
            ifm_buff3_5_ce1 = ap_const_logic_0;
        }
    } else {
        ifm_buff3_5_ce1 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_5_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_5_d0 = grp_write_row_ifm_fu_1365_ifm_buff0_5_d0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_5_d0 = grp_load_cifm_data_fu_1263_ifm_buff2_5_d0.read();
    } else {
        ifm_buff3_5_d0 = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    }
}

void conv::thread_ifm_buff3_5_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_5_we0 = grp_write_row_ifm_fu_1365_ifm_buff0_5_we0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_5_we0 = grp_load_cifm_data_fu_1263_ifm_buff2_5_we0.read();
    } else {
        ifm_buff3_5_we0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_6_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_6_address0 = grp_write_row_ifm_fu_1365_ifm_buff0_6_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_6_address0 = grp_load_cifm_data_fu_1263_ifm_buff2_6_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_6_address0 = grp_conv_write_fu_1051_ifm_buff2_6_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_6_address0 = grp_conv_write_fu_1051_ifm_buff1_6_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_6_address0 = grp_conv_write_fu_1051_ifm_buff0_6_address0.read();
    } else {
        ifm_buff3_6_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_6_address1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_6_address1 = grp_conv_write_fu_1051_ifm_buff2_6_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_6_address1 = grp_conv_write_fu_1051_ifm_buff1_6_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_6_address1 = grp_conv_write_fu_1051_ifm_buff0_6_address1.read();
        } else {
            ifm_buff3_6_address1 =  (sc_lv<6>) ("XXXXXX");
        }
    } else {
        ifm_buff3_6_address1 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_6_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_6_ce0 = grp_write_row_ifm_fu_1365_ifm_buff0_6_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_6_ce0 = grp_load_cifm_data_fu_1263_ifm_buff2_6_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_6_ce0 = grp_conv_write_fu_1051_ifm_buff2_6_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_6_ce0 = grp_conv_write_fu_1051_ifm_buff1_6_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_6_ce0 = grp_conv_write_fu_1051_ifm_buff0_6_ce0.read();
    } else {
        ifm_buff3_6_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_6_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_6_ce1 = grp_conv_write_fu_1051_ifm_buff2_6_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_6_ce1 = grp_conv_write_fu_1051_ifm_buff1_6_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_6_ce1 = grp_conv_write_fu_1051_ifm_buff0_6_ce1.read();
        } else {
            ifm_buff3_6_ce1 = ap_const_logic_0;
        }
    } else {
        ifm_buff3_6_ce1 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_6_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_6_d0 = grp_write_row_ifm_fu_1365_ifm_buff0_6_d0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_6_d0 = grp_load_cifm_data_fu_1263_ifm_buff2_6_d0.read();
    } else {
        ifm_buff3_6_d0 = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    }
}

void conv::thread_ifm_buff3_6_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_6_we0 = grp_write_row_ifm_fu_1365_ifm_buff0_6_we0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_6_we0 = grp_load_cifm_data_fu_1263_ifm_buff2_6_we0.read();
    } else {
        ifm_buff3_6_we0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_7_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_7_address0 = grp_write_row_ifm_fu_1365_ifm_buff0_7_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_7_address0 = grp_load_cifm_data_fu_1263_ifm_buff2_7_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_7_address0 = grp_conv_write_fu_1051_ifm_buff2_7_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_7_address0 = grp_conv_write_fu_1051_ifm_buff1_7_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_7_address0 = grp_conv_write_fu_1051_ifm_buff0_7_address0.read();
    } else {
        ifm_buff3_7_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_7_address1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_7_address1 = grp_conv_write_fu_1051_ifm_buff2_7_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_7_address1 = grp_conv_write_fu_1051_ifm_buff1_7_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_7_address1 = grp_conv_write_fu_1051_ifm_buff0_7_address1.read();
        } else {
            ifm_buff3_7_address1 =  (sc_lv<6>) ("XXXXXX");
        }
    } else {
        ifm_buff3_7_address1 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_7_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_7_ce0 = grp_write_row_ifm_fu_1365_ifm_buff0_7_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_7_ce0 = grp_load_cifm_data_fu_1263_ifm_buff2_7_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_7_ce0 = grp_conv_write_fu_1051_ifm_buff2_7_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_7_ce0 = grp_conv_write_fu_1051_ifm_buff1_7_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_7_ce0 = grp_conv_write_fu_1051_ifm_buff0_7_ce0.read();
    } else {
        ifm_buff3_7_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_7_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_7_ce1 = grp_conv_write_fu_1051_ifm_buff2_7_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_7_ce1 = grp_conv_write_fu_1051_ifm_buff1_7_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_7_ce1 = grp_conv_write_fu_1051_ifm_buff0_7_ce1.read();
        } else {
            ifm_buff3_7_ce1 = ap_const_logic_0;
        }
    } else {
        ifm_buff3_7_ce1 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_7_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_7_d0 = grp_write_row_ifm_fu_1365_ifm_buff0_7_d0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_7_d0 = grp_load_cifm_data_fu_1263_ifm_buff2_7_d0.read();
    } else {
        ifm_buff3_7_d0 = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    }
}

void conv::thread_ifm_buff3_7_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_7_we0 = grp_write_row_ifm_fu_1365_ifm_buff0_7_we0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_7_we0 = grp_load_cifm_data_fu_1263_ifm_buff2_7_we0.read();
    } else {
        ifm_buff3_7_we0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_8_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_8_address0 = grp_write_row_ifm_fu_1365_ifm_buff0_8_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_8_address0 = grp_load_cifm_data_fu_1263_ifm_buff2_8_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_8_address0 = grp_conv_write_fu_1051_ifm_buff2_8_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_8_address0 = grp_conv_write_fu_1051_ifm_buff1_8_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_8_address0 = grp_conv_write_fu_1051_ifm_buff0_8_address0.read();
    } else {
        ifm_buff3_8_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_8_address1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_8_address1 = grp_conv_write_fu_1051_ifm_buff2_8_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_8_address1 = grp_conv_write_fu_1051_ifm_buff1_8_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_8_address1 = grp_conv_write_fu_1051_ifm_buff0_8_address1.read();
        } else {
            ifm_buff3_8_address1 =  (sc_lv<6>) ("XXXXXX");
        }
    } else {
        ifm_buff3_8_address1 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_8_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_8_ce0 = grp_write_row_ifm_fu_1365_ifm_buff0_8_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_8_ce0 = grp_load_cifm_data_fu_1263_ifm_buff2_8_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_8_ce0 = grp_conv_write_fu_1051_ifm_buff2_8_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_8_ce0 = grp_conv_write_fu_1051_ifm_buff1_8_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_8_ce0 = grp_conv_write_fu_1051_ifm_buff0_8_ce0.read();
    } else {
        ifm_buff3_8_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_8_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_8_ce1 = grp_conv_write_fu_1051_ifm_buff2_8_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_8_ce1 = grp_conv_write_fu_1051_ifm_buff1_8_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_8_ce1 = grp_conv_write_fu_1051_ifm_buff0_8_ce1.read();
        } else {
            ifm_buff3_8_ce1 = ap_const_logic_0;
        }
    } else {
        ifm_buff3_8_ce1 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_8_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_8_d0 = grp_write_row_ifm_fu_1365_ifm_buff0_8_d0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_8_d0 = grp_load_cifm_data_fu_1263_ifm_buff2_8_d0.read();
    } else {
        ifm_buff3_8_d0 = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    }
}

void conv::thread_ifm_buff3_8_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_8_we0 = grp_write_row_ifm_fu_1365_ifm_buff0_8_we0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_8_we0 = grp_load_cifm_data_fu_1263_ifm_buff2_8_we0.read();
    } else {
        ifm_buff3_8_we0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_9_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_9_address0 = grp_write_row_ifm_fu_1365_ifm_buff0_9_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_9_address0 = grp_load_cifm_data_fu_1263_ifm_buff2_9_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_9_address0 = grp_conv_write_fu_1051_ifm_buff2_9_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_9_address0 = grp_conv_write_fu_1051_ifm_buff1_9_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_9_address0 = grp_conv_write_fu_1051_ifm_buff0_9_address0.read();
    } else {
        ifm_buff3_9_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_9_address1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_9_address1 = grp_conv_write_fu_1051_ifm_buff2_9_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_9_address1 = grp_conv_write_fu_1051_ifm_buff1_9_address1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_9_address1 = grp_conv_write_fu_1051_ifm_buff0_9_address1.read();
        } else {
            ifm_buff3_9_address1 =  (sc_lv<6>) ("XXXXXX");
        }
    } else {
        ifm_buff3_9_address1 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ifm_buff3_9_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_9_ce0 = grp_write_row_ifm_fu_1365_ifm_buff0_9_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_9_ce0 = grp_load_cifm_data_fu_1263_ifm_buff2_9_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read()))) {
        ifm_buff3_9_ce0 = grp_conv_write_fu_1051_ifm_buff2_9_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read()))) {
        ifm_buff3_9_ce0 = grp_conv_write_fu_1051_ifm_buff1_9_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read()))) {
        ifm_buff3_9_ce0 = grp_conv_write_fu_1051_ifm_buff0_9_ce0.read();
    } else {
        ifm_buff3_9_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_9_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read())) {
        if (esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) {
            ifm_buff3_9_ce1 = grp_conv_write_fu_1051_ifm_buff2_9_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())) {
            ifm_buff3_9_ce1 = grp_conv_write_fu_1051_ifm_buff1_9_ce1.read();
        } else if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) {
            ifm_buff3_9_ce1 = grp_conv_write_fu_1051_ifm_buff0_9_ce1.read();
        } else {
            ifm_buff3_9_ce1 = ap_const_logic_0;
        }
    } else {
        ifm_buff3_9_ce1 = ap_const_logic_0;
    }
}

void conv::thread_ifm_buff3_9_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_9_d0 = grp_write_row_ifm_fu_1365_ifm_buff0_9_d0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_9_d0 = grp_load_cifm_data_fu_1263_ifm_buff2_9_d0.read();
    } else {
        ifm_buff3_9_d0 = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    }
}

void conv::thread_ifm_buff3_9_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
         esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op300_call_state4.read()))) {
        ifm_buff3_9_we0 = grp_write_row_ifm_fu_1365_ifm_buff0_9_we0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read())) {
        ifm_buff3_9_we0 = grp_load_cifm_data_fu_1263_ifm_buff2_9_we0.read();
    } else {
        ifm_buff3_9_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_0_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_0_address0 = grp_conv_read_fu_1684_ofm_buff0_0_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_0_address0 = grp_conv_write_fu_1051_ofm_buff0_0_address0.read();
    } else {
        ofm_buff0_0_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff0_0_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_0_ce0 = grp_conv_read_fu_1684_ofm_buff0_0_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_0_ce0 = grp_conv_write_fu_1051_ofm_buff0_0_ce0.read();
    } else {
        ofm_buff0_0_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_0_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_0_we0 = grp_conv_write_fu_1051_ofm_buff0_0_we0.read();
    } else {
        ofm_buff0_0_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_10_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_10_address0 = grp_conv_read_fu_1684_ofm_buff0_10_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_10_address0 = grp_conv_write_fu_1051_ofm_buff0_10_address0.read();
    } else {
        ofm_buff0_10_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff0_10_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_10_ce0 = grp_conv_read_fu_1684_ofm_buff0_10_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_10_ce0 = grp_conv_write_fu_1051_ofm_buff0_10_ce0.read();
    } else {
        ofm_buff0_10_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_10_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_10_we0 = grp_conv_write_fu_1051_ofm_buff0_10_we0.read();
    } else {
        ofm_buff0_10_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_11_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_11_address0 = grp_conv_read_fu_1684_ofm_buff0_11_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_11_address0 = grp_conv_write_fu_1051_ofm_buff0_11_address0.read();
    } else {
        ofm_buff0_11_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff0_11_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_11_ce0 = grp_conv_read_fu_1684_ofm_buff0_11_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_11_ce0 = grp_conv_write_fu_1051_ofm_buff0_11_ce0.read();
    } else {
        ofm_buff0_11_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_11_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_11_we0 = grp_conv_write_fu_1051_ofm_buff0_11_we0.read();
    } else {
        ofm_buff0_11_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_12_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_12_address0 = grp_conv_read_fu_1684_ofm_buff0_12_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_12_address0 = grp_conv_write_fu_1051_ofm_buff0_12_address0.read();
    } else {
        ofm_buff0_12_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff0_12_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_12_ce0 = grp_conv_read_fu_1684_ofm_buff0_12_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_12_ce0 = grp_conv_write_fu_1051_ofm_buff0_12_ce0.read();
    } else {
        ofm_buff0_12_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_12_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_12_we0 = grp_conv_write_fu_1051_ofm_buff0_12_we0.read();
    } else {
        ofm_buff0_12_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_13_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_13_address0 = grp_conv_read_fu_1684_ofm_buff0_13_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_13_address0 = grp_conv_write_fu_1051_ofm_buff0_13_address0.read();
    } else {
        ofm_buff0_13_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff0_13_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_13_ce0 = grp_conv_read_fu_1684_ofm_buff0_13_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_13_ce0 = grp_conv_write_fu_1051_ofm_buff0_13_ce0.read();
    } else {
        ofm_buff0_13_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_13_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_13_we0 = grp_conv_write_fu_1051_ofm_buff0_13_we0.read();
    } else {
        ofm_buff0_13_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_14_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_14_address0 = grp_conv_read_fu_1684_ofm_buff0_14_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_14_address0 = grp_conv_write_fu_1051_ofm_buff0_14_address0.read();
    } else {
        ofm_buff0_14_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff0_14_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_14_ce0 = grp_conv_read_fu_1684_ofm_buff0_14_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_14_ce0 = grp_conv_write_fu_1051_ofm_buff0_14_ce0.read();
    } else {
        ofm_buff0_14_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_14_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_14_we0 = grp_conv_write_fu_1051_ofm_buff0_14_we0.read();
    } else {
        ofm_buff0_14_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_15_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_15_address0 = grp_conv_read_fu_1684_ofm_buff0_15_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_15_address0 = grp_conv_write_fu_1051_ofm_buff0_15_address0.read();
    } else {
        ofm_buff0_15_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff0_15_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_15_ce0 = grp_conv_read_fu_1684_ofm_buff0_15_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_15_ce0 = grp_conv_write_fu_1051_ofm_buff0_15_ce0.read();
    } else {
        ofm_buff0_15_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_15_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_15_we0 = grp_conv_write_fu_1051_ofm_buff0_15_we0.read();
    } else {
        ofm_buff0_15_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_1_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_1_address0 = grp_conv_read_fu_1684_ofm_buff0_1_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_1_address0 = grp_conv_write_fu_1051_ofm_buff0_1_address0.read();
    } else {
        ofm_buff0_1_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff0_1_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_1_ce0 = grp_conv_read_fu_1684_ofm_buff0_1_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_1_ce0 = grp_conv_write_fu_1051_ofm_buff0_1_ce0.read();
    } else {
        ofm_buff0_1_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_1_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_1_we0 = grp_conv_write_fu_1051_ofm_buff0_1_we0.read();
    } else {
        ofm_buff0_1_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_2_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_2_address0 = grp_conv_read_fu_1684_ofm_buff0_2_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_2_address0 = grp_conv_write_fu_1051_ofm_buff0_2_address0.read();
    } else {
        ofm_buff0_2_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff0_2_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_2_ce0 = grp_conv_read_fu_1684_ofm_buff0_2_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_2_ce0 = grp_conv_write_fu_1051_ofm_buff0_2_ce0.read();
    } else {
        ofm_buff0_2_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_2_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_2_we0 = grp_conv_write_fu_1051_ofm_buff0_2_we0.read();
    } else {
        ofm_buff0_2_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_3_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_3_address0 = grp_conv_read_fu_1684_ofm_buff0_3_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_3_address0 = grp_conv_write_fu_1051_ofm_buff0_3_address0.read();
    } else {
        ofm_buff0_3_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff0_3_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_3_ce0 = grp_conv_read_fu_1684_ofm_buff0_3_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_3_ce0 = grp_conv_write_fu_1051_ofm_buff0_3_ce0.read();
    } else {
        ofm_buff0_3_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_3_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_3_we0 = grp_conv_write_fu_1051_ofm_buff0_3_we0.read();
    } else {
        ofm_buff0_3_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_4_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_4_address0 = grp_conv_read_fu_1684_ofm_buff0_4_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_4_address0 = grp_conv_write_fu_1051_ofm_buff0_4_address0.read();
    } else {
        ofm_buff0_4_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff0_4_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_4_ce0 = grp_conv_read_fu_1684_ofm_buff0_4_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_4_ce0 = grp_conv_write_fu_1051_ofm_buff0_4_ce0.read();
    } else {
        ofm_buff0_4_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_4_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_4_we0 = grp_conv_write_fu_1051_ofm_buff0_4_we0.read();
    } else {
        ofm_buff0_4_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_5_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_5_address0 = grp_conv_read_fu_1684_ofm_buff0_5_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_5_address0 = grp_conv_write_fu_1051_ofm_buff0_5_address0.read();
    } else {
        ofm_buff0_5_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff0_5_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_5_ce0 = grp_conv_read_fu_1684_ofm_buff0_5_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_5_ce0 = grp_conv_write_fu_1051_ofm_buff0_5_ce0.read();
    } else {
        ofm_buff0_5_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_5_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_5_we0 = grp_conv_write_fu_1051_ofm_buff0_5_we0.read();
    } else {
        ofm_buff0_5_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_6_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_6_address0 = grp_conv_read_fu_1684_ofm_buff0_6_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_6_address0 = grp_conv_write_fu_1051_ofm_buff0_6_address0.read();
    } else {
        ofm_buff0_6_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff0_6_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_6_ce0 = grp_conv_read_fu_1684_ofm_buff0_6_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_6_ce0 = grp_conv_write_fu_1051_ofm_buff0_6_ce0.read();
    } else {
        ofm_buff0_6_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_6_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_6_we0 = grp_conv_write_fu_1051_ofm_buff0_6_we0.read();
    } else {
        ofm_buff0_6_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_7_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_7_address0 = grp_conv_read_fu_1684_ofm_buff0_7_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_7_address0 = grp_conv_write_fu_1051_ofm_buff0_7_address0.read();
    } else {
        ofm_buff0_7_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff0_7_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_7_ce0 = grp_conv_read_fu_1684_ofm_buff0_7_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_7_ce0 = grp_conv_write_fu_1051_ofm_buff0_7_ce0.read();
    } else {
        ofm_buff0_7_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_7_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_7_we0 = grp_conv_write_fu_1051_ofm_buff0_7_we0.read();
    } else {
        ofm_buff0_7_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_8_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_8_address0 = grp_conv_read_fu_1684_ofm_buff0_8_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_8_address0 = grp_conv_write_fu_1051_ofm_buff0_8_address0.read();
    } else {
        ofm_buff0_8_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff0_8_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_8_ce0 = grp_conv_read_fu_1684_ofm_buff0_8_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_8_ce0 = grp_conv_write_fu_1051_ofm_buff0_8_ce0.read();
    } else {
        ofm_buff0_8_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_8_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_8_we0 = grp_conv_write_fu_1051_ofm_buff0_8_we0.read();
    } else {
        ofm_buff0_8_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_9_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_9_address0 = grp_conv_read_fu_1684_ofm_buff0_9_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_9_address0 = grp_conv_write_fu_1051_ofm_buff0_9_address0.read();
    } else {
        ofm_buff0_9_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff0_9_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op302_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op314_call_state4.read())))) {
        ofm_buff0_9_ce0 = grp_conv_read_fu_1684_ofm_buff0_9_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_9_ce0 = grp_conv_write_fu_1051_ofm_buff0_9_ce0.read();
    } else {
        ofm_buff0_9_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff0_9_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op307_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())))) {
        ofm_buff0_9_we0 = grp_conv_write_fu_1051_ofm_buff0_9_we0.read();
    } else {
        ofm_buff0_9_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_0_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_0_address0 = grp_conv_read_fu_1684_ofm_buff0_0_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_0_address0 = grp_conv_write_fu_1051_ofm_buff0_0_address0.read();
    } else {
        ofm_buff1_0_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff1_0_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_0_ce0 = grp_conv_read_fu_1684_ofm_buff0_0_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_0_ce0 = grp_conv_write_fu_1051_ofm_buff0_0_ce0.read();
    } else {
        ofm_buff1_0_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_0_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_0_we0 = grp_conv_write_fu_1051_ofm_buff0_0_we0.read();
    } else {
        ofm_buff1_0_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_10_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_10_address0 = grp_conv_read_fu_1684_ofm_buff0_10_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_10_address0 = grp_conv_write_fu_1051_ofm_buff0_10_address0.read();
    } else {
        ofm_buff1_10_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff1_10_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_10_ce0 = grp_conv_read_fu_1684_ofm_buff0_10_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_10_ce0 = grp_conv_write_fu_1051_ofm_buff0_10_ce0.read();
    } else {
        ofm_buff1_10_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_10_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_10_we0 = grp_conv_write_fu_1051_ofm_buff0_10_we0.read();
    } else {
        ofm_buff1_10_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_11_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_11_address0 = grp_conv_read_fu_1684_ofm_buff0_11_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_11_address0 = grp_conv_write_fu_1051_ofm_buff0_11_address0.read();
    } else {
        ofm_buff1_11_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff1_11_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_11_ce0 = grp_conv_read_fu_1684_ofm_buff0_11_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_11_ce0 = grp_conv_write_fu_1051_ofm_buff0_11_ce0.read();
    } else {
        ofm_buff1_11_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_11_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_11_we0 = grp_conv_write_fu_1051_ofm_buff0_11_we0.read();
    } else {
        ofm_buff1_11_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_12_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_12_address0 = grp_conv_read_fu_1684_ofm_buff0_12_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_12_address0 = grp_conv_write_fu_1051_ofm_buff0_12_address0.read();
    } else {
        ofm_buff1_12_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff1_12_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_12_ce0 = grp_conv_read_fu_1684_ofm_buff0_12_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_12_ce0 = grp_conv_write_fu_1051_ofm_buff0_12_ce0.read();
    } else {
        ofm_buff1_12_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_12_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_12_we0 = grp_conv_write_fu_1051_ofm_buff0_12_we0.read();
    } else {
        ofm_buff1_12_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_13_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_13_address0 = grp_conv_read_fu_1684_ofm_buff0_13_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_13_address0 = grp_conv_write_fu_1051_ofm_buff0_13_address0.read();
    } else {
        ofm_buff1_13_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff1_13_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_13_ce0 = grp_conv_read_fu_1684_ofm_buff0_13_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_13_ce0 = grp_conv_write_fu_1051_ofm_buff0_13_ce0.read();
    } else {
        ofm_buff1_13_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_13_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_13_we0 = grp_conv_write_fu_1051_ofm_buff0_13_we0.read();
    } else {
        ofm_buff1_13_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_14_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_14_address0 = grp_conv_read_fu_1684_ofm_buff0_14_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_14_address0 = grp_conv_write_fu_1051_ofm_buff0_14_address0.read();
    } else {
        ofm_buff1_14_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff1_14_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_14_ce0 = grp_conv_read_fu_1684_ofm_buff0_14_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_14_ce0 = grp_conv_write_fu_1051_ofm_buff0_14_ce0.read();
    } else {
        ofm_buff1_14_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_14_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_14_we0 = grp_conv_write_fu_1051_ofm_buff0_14_we0.read();
    } else {
        ofm_buff1_14_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_15_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_15_address0 = grp_conv_read_fu_1684_ofm_buff0_15_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_15_address0 = grp_conv_write_fu_1051_ofm_buff0_15_address0.read();
    } else {
        ofm_buff1_15_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff1_15_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_15_ce0 = grp_conv_read_fu_1684_ofm_buff0_15_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_15_ce0 = grp_conv_write_fu_1051_ofm_buff0_15_ce0.read();
    } else {
        ofm_buff1_15_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_15_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_15_we0 = grp_conv_write_fu_1051_ofm_buff0_15_we0.read();
    } else {
        ofm_buff1_15_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_1_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_1_address0 = grp_conv_read_fu_1684_ofm_buff0_1_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_1_address0 = grp_conv_write_fu_1051_ofm_buff0_1_address0.read();
    } else {
        ofm_buff1_1_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff1_1_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_1_ce0 = grp_conv_read_fu_1684_ofm_buff0_1_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_1_ce0 = grp_conv_write_fu_1051_ofm_buff0_1_ce0.read();
    } else {
        ofm_buff1_1_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_1_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_1_we0 = grp_conv_write_fu_1051_ofm_buff0_1_we0.read();
    } else {
        ofm_buff1_1_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_2_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_2_address0 = grp_conv_read_fu_1684_ofm_buff0_2_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_2_address0 = grp_conv_write_fu_1051_ofm_buff0_2_address0.read();
    } else {
        ofm_buff1_2_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff1_2_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_2_ce0 = grp_conv_read_fu_1684_ofm_buff0_2_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_2_ce0 = grp_conv_write_fu_1051_ofm_buff0_2_ce0.read();
    } else {
        ofm_buff1_2_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_2_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_2_we0 = grp_conv_write_fu_1051_ofm_buff0_2_we0.read();
    } else {
        ofm_buff1_2_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_3_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_3_address0 = grp_conv_read_fu_1684_ofm_buff0_3_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_3_address0 = grp_conv_write_fu_1051_ofm_buff0_3_address0.read();
    } else {
        ofm_buff1_3_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff1_3_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_3_ce0 = grp_conv_read_fu_1684_ofm_buff0_3_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_3_ce0 = grp_conv_write_fu_1051_ofm_buff0_3_ce0.read();
    } else {
        ofm_buff1_3_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_3_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_3_we0 = grp_conv_write_fu_1051_ofm_buff0_3_we0.read();
    } else {
        ofm_buff1_3_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_4_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_4_address0 = grp_conv_read_fu_1684_ofm_buff0_4_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_4_address0 = grp_conv_write_fu_1051_ofm_buff0_4_address0.read();
    } else {
        ofm_buff1_4_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff1_4_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_4_ce0 = grp_conv_read_fu_1684_ofm_buff0_4_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_4_ce0 = grp_conv_write_fu_1051_ofm_buff0_4_ce0.read();
    } else {
        ofm_buff1_4_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_4_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_4_we0 = grp_conv_write_fu_1051_ofm_buff0_4_we0.read();
    } else {
        ofm_buff1_4_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_5_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_5_address0 = grp_conv_read_fu_1684_ofm_buff0_5_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_5_address0 = grp_conv_write_fu_1051_ofm_buff0_5_address0.read();
    } else {
        ofm_buff1_5_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff1_5_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_5_ce0 = grp_conv_read_fu_1684_ofm_buff0_5_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_5_ce0 = grp_conv_write_fu_1051_ofm_buff0_5_ce0.read();
    } else {
        ofm_buff1_5_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_5_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_5_we0 = grp_conv_write_fu_1051_ofm_buff0_5_we0.read();
    } else {
        ofm_buff1_5_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_6_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_6_address0 = grp_conv_read_fu_1684_ofm_buff0_6_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_6_address0 = grp_conv_write_fu_1051_ofm_buff0_6_address0.read();
    } else {
        ofm_buff1_6_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff1_6_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_6_ce0 = grp_conv_read_fu_1684_ofm_buff0_6_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_6_ce0 = grp_conv_write_fu_1051_ofm_buff0_6_ce0.read();
    } else {
        ofm_buff1_6_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_6_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_6_we0 = grp_conv_write_fu_1051_ofm_buff0_6_we0.read();
    } else {
        ofm_buff1_6_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_7_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_7_address0 = grp_conv_read_fu_1684_ofm_buff0_7_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_7_address0 = grp_conv_write_fu_1051_ofm_buff0_7_address0.read();
    } else {
        ofm_buff1_7_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff1_7_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_7_ce0 = grp_conv_read_fu_1684_ofm_buff0_7_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_7_ce0 = grp_conv_write_fu_1051_ofm_buff0_7_ce0.read();
    } else {
        ofm_buff1_7_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_7_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_7_we0 = grp_conv_write_fu_1051_ofm_buff0_7_we0.read();
    } else {
        ofm_buff1_7_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_8_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_8_address0 = grp_conv_read_fu_1684_ofm_buff0_8_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_8_address0 = grp_conv_write_fu_1051_ofm_buff0_8_address0.read();
    } else {
        ofm_buff1_8_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff1_8_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_8_ce0 = grp_conv_read_fu_1684_ofm_buff0_8_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_8_ce0 = grp_conv_write_fu_1051_ofm_buff0_8_ce0.read();
    } else {
        ofm_buff1_8_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_8_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_8_we0 = grp_conv_write_fu_1051_ofm_buff0_8_we0.read();
    } else {
        ofm_buff1_8_we0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_9_address0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_9_address0 = grp_conv_read_fu_1684_ofm_buff0_9_address0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_9_address0 = grp_conv_write_fu_1051_ofm_buff0_9_address0.read();
    } else {
        ofm_buff1_9_address0 =  (sc_lv<6>) ("XXXXXX");
    }
}

void conv::thread_ofm_buff1_9_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, tmp_reg_1839.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op308_call_state4.read())))) {
        ofm_buff1_9_ce0 = grp_conv_read_fu_1684_ofm_buff0_9_ce0.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
                 esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_9_ce0 = grp_conv_write_fu_1051_ofm_buff0_9_ce0.read();
    } else {
        ofm_buff1_9_ce0 = ap_const_logic_0;
    }
}

void conv::thread_ofm_buff1_9_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op301_call_state4.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op313_call_state4.read())))) {
        ofm_buff1_9_we0 = grp_conv_write_fu_1051_ofm_buff0_9_we0.read();
    } else {
        ofm_buff1_9_we0 = ap_const_logic_0;
    }
}

void conv::thread_p_s_fu_1809_p3() {
    p_s_fu_1809_p3 = (!tmp_7_fu_1803_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_7_fu_1803_p2.read()[0].to_bool())? ap_const_lv16_0: rotate_counter_1_fu_1797_p2.read());
}

void conv::thread_rotate_counter_1_fu_1797_p2() {
    rotate_counter_1_fu_1797_p2 = (!rotate_counter_reg_1028.read().is_01() || !ap_const_lv16_1.is_01())? sc_lv<16>(): (sc_biguint<16>(rotate_counter_reg_1028.read()) + sc_biguint<16>(ap_const_lv16_1));
}

void conv::thread_row_1_fu_1753_p2() {
    row_1_fu_1753_p2 = (!row_reg_1040.read().is_01() || !ap_const_lv6_1.is_01())? sc_lv<6>(): (sc_biguint<6>(row_reg_1040.read()) + sc_biguint<6>(ap_const_lv6_1));
}

void conv::thread_tmp_3_fu_1765_p2() {
    tmp_3_fu_1765_p2 = (!rotate_counter_reg_1028.read().is_01() || !ap_const_lv16_1.is_01())? sc_lv<1>(): sc_lv<1>(rotate_counter_reg_1028.read() == ap_const_lv16_1);
}

void conv::thread_tmp_4_fu_1771_p2() {
    tmp_4_fu_1771_p2 = (!rotate_counter_reg_1028.read().is_01() || !ap_const_lv16_2.is_01())? sc_lv<1>(): sc_lv<1>(rotate_counter_reg_1028.read() == ap_const_lv16_2);
}

void conv::thread_tmp_5_fu_1777_p2() {
    tmp_5_fu_1777_p2 = (!rotate_counter_reg_1028.read().is_01() || !ap_const_lv16_3.is_01())? sc_lv<1>(): sc_lv<1>(rotate_counter_reg_1028.read() == ap_const_lv16_3);
}

void conv::thread_tmp_6_fu_1783_p2() {
    tmp_6_fu_1783_p2 = (!row_reg_1040.read().is_01() || !ap_const_lv6_37.is_01())? sc_lv<1>(): sc_lv<1>(row_reg_1040.read() != ap_const_lv6_37);
}

void conv::thread_tmp_7_fu_1803_p2() {
    tmp_7_fu_1803_p2 = (!rotate_counter_1_fu_1797_p2.read().is_01() || !ap_const_lv16_4.is_01())? sc_lv<1>(): sc_lv<1>(rotate_counter_1_fu_1797_p2.read() == ap_const_lv16_4);
}

void conv::thread_tmp_fu_1759_p2() {
    tmp_fu_1759_p2 = (!rotate_counter_reg_1028.read().is_01() || !ap_const_lv16_0.is_01())? sc_lv<1>(): sc_lv<1>(rotate_counter_reg_1028.read() == ap_const_lv16_0);
}

void conv::thread_tmp_s_fu_1790_p2() {
    tmp_s_fu_1790_p2 = (!row_reg_1040.read().is_01() || !ap_const_lv6_0.is_01())? sc_lv<1>(): sc_lv<1>(row_reg_1040.read() != ap_const_lv6_0);
}

void conv::thread_tran_wgt_address() {
    tran_wgt_address = grp_load_filter_buffer_fu_1390_wgt_address.read();
}

void conv::thread_tran_wgt_dataout() {
    tran_wgt_dataout = grp_load_filter_buffer_fu_1390_wgt_dataout.read();
}

void conv::thread_tran_wgt_req_din() {
    tran_wgt_req_din = grp_load_filter_buffer_fu_1390_wgt_req_din.read();
}

void conv::thread_tran_wgt_req_write() {
    tran_wgt_req_write = grp_load_filter_buffer_fu_1390_wgt_req_write.read();
}

void conv::thread_tran_wgt_rsp_read() {
    tran_wgt_rsp_read = grp_load_filter_buffer_fu_1390_wgt_rsp_read.read();
}

void conv::thread_tran_wgt_size() {
    tran_wgt_size = grp_load_filter_buffer_fu_1390_wgt_size.read();
}

}

