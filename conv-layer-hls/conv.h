#ifndef __CONV_LAYER_H__
#define __CONV_LAYER_H__

#include <cmath>
#include <cstring>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>


#define N 16
#define M 16
#define IR 56
#define IC 56
#define OR 56
#define OC 56
#define K 3
#define S 1
#define P 1
#define w_m 1
#define ROW_G 3
#define w_n 3 // w_n = w_m + w_r - 1 = 4 + 3 -1 = 6

typedef float DATA_T;

typedef struct ifm_struct {
  float a0;
  float a1;
  float a2;
  float a3;
  float a4;
  float a5;
  float a6;
  float a7;
  float a8;
  float a9;
  float a10;
  float a11;
  float a12;
  float a13;
  float a14;
  float a15;
} IPACK;

typedef struct filter_struct {
  float f0;
  float f1;
  float f2;
  float f3;
  float f4;
  float f5;
  float f6;
  float f7;
  float f8;
  float f9;
  float f10;
  float f11;
  float f12;
  float f13;
  float f14;
  float f15;

} FPACK;

typedef struct ofm_struct {
  float b0;
  float b1;
  float b2;
  float b3;
  float b4;
  float b5;
  float b6;
  float b7;
  float b8;
  float b9;
  float b10;
  float b11;
  float b12;
  float b13;
  float b14;
  float b15;
} OPACK;

// software convolution for checking;
void convolution_sw(float *ifm, float *ofm, float *weight);

void generate(float *ifm, float *wgt);

void check(float *ofm_sw, float *ofm_hw);

void test_data_gen(float *ifm, float *wgt);

// chage_xxx are used to reshape data layout!!!
void change_ifm(float *ifm, IPACK *ifm_pack);

void change_filter(float *wgt, FPACK *filter_pack);

void load_cifm_data(IPACK *cifm, DATA_T ifm_buff0[N][(IC + 2 * P)],
                    DATA_T ifm_buff1[N][(IC + 2 * P)],
                    DATA_T ifm_buff2[N][(IC + 2 * P)], int *cifm_counter);

void write_row_ifm(IPACK *cifm, DATA_T ifm_buff0[N][(IC + 2 * P)],
                   int *cifm_counter, bool enable);

void load_filter_buffer(FPACK *wgt, DATA_T filter_buff[M][N][ROW_G][ROW_G]);

void conv_write(DATA_T filter_buff[M][N][ROW_G][ROW_G],
                DATA_T ifm_buff0[N][(IC + 2 * P)],
                DATA_T ifm_buff1[N][(IC + 2 * P)],
                DATA_T ifm_buff2[N][(IC + 2 * P)],
                DATA_T ofm_buff0[M][OC]);

void conv_read(OPACK *cofm, DATA_T ofm_buff0[M][OC], int *cofm_counter,
               bool enable);

// convolution hardware
void conv(IPACK *cifm, OPACK *cofm, FPACK *tran_wgt);

void chang_cofm(OPACK *ofm_pack, float *ofm);

#endif // __CONV_LAYER_H__ not defined
