//*****************************************************************************
//
// Concolution layer test bench.
//
//*****************************************************************************

#include "conv.h"

// software convolution
void convolution_sw(float *ifm, float *ofm, float *weight) {
  for (int m = 0; m < M; m++) {
    for (int r = 0; r < OR; r++) {
      for (int c = 0; c < OC; c++) {
        float odata = 0;
        int ofm_index = m * OR * OC + r * OC + c;
        for (int n = 0; n < N; n++) {
          for (int kr = 0; kr < K; kr++) {
            for (int kc = 0; kc < K; kc++) {
              float ret;
              int ic = c * S - P + kc;
              int ir = r * S - P + kr;
              int ifm_index = n * IR * IC + ir * IC + ic;
              int wgt_index = m * N * K * K + n * K * K + kr * K + kc;

              if ((ic < 0) || (ir < 0) || (ic > (IC - 1)) || (ir > (IR - 1)))
                ret = 0;
              else
                ret = ifm[ifm_index];
              ret *= weight[wgt_index];
              odata += ret;
            }
          }
        }
        ofm[ofm_index] = odata;
      }
    }
  }
}

void generate(float *ifm, float *wgt) {
  for (int i = 0; i < N * IR * IC; i++) {
    ifm[i] = (float)i / 3000.0;
  }
  for (int i = 0; i < N * M * K * K; i++) {
    wgt[i] = (float)i / 3000.0;
  }
}

void check(float *ofm_sw, float *ofm_hw) {
  int error = 0;
  for (int i = 0; i < N * OR * OC; i++) {
    if (((ofm_sw[i] - ofm_hw[i]) > 0.01) || ((ofm_sw[i] - ofm_hw[i]) < -0.01))
      error++;
  }
  if (error > 0)
    printf("error count = %d\n", error);
  else
    printf("correct!\n", error);
}

/**
 * This function change the ifm to the standard form:
 * LEN: #of input channels * #of elements for each row, Height: IR
 * ROW ROW ROW .... ROW
 * ROW ROW ROW .... ROW
 *
 **/
void change_ifm(float *ifm, IPACK *ifm_pack) {
  float ifm_temp[N][OR + 2 * P][OC + 2 * P];
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < OR + 2 * P; j++) {
      for (int k = 0; k < OC + 2 * P; k++) {
        ifm_temp[i][j][k] = 0;
      }
    }
  }
  for (int bc = P; bc < OC + P; bc++) {
    for (int br = P; br < OR + P; br++) {
      for (int bn = 0; bn < N; bn++) {
        int temp1 = bn * IR * IC + (br - P) * IC;
        int i_index = temp1 + (bc - P);
        ifm_temp[bn][br][bc] = ifm[i_index];
      }
    }
  }
  int count = 0;
  for (int r = 0; r < OR + 2 * P; r++) {
    for (int c = 0; c < OR + 2 * P; c++) {
      ifm_pack[count].a0 = ifm_temp[0][r][c];
      ifm_pack[count].a1 = ifm_temp[1][r][c];
      ifm_pack[count].a2 = ifm_temp[2][r][c];
      ifm_pack[count].a3 = ifm_temp[3][r][c];
      ifm_pack[count].a4 = ifm_temp[4][r][c];
      ifm_pack[count].a5 = ifm_temp[5][r][c];
      ifm_pack[count].a6 = ifm_temp[6][r][c];
      ifm_pack[count].a7 = ifm_temp[7][r][c];
      ifm_pack[count].a8 = ifm_temp[8][r][c];
      ifm_pack[count].a9 = ifm_temp[9][r][c];
      ifm_pack[count].a10 = ifm_temp[10][r][c];
      ifm_pack[count].a11 = ifm_temp[11][r][c];
      ifm_pack[count].a12 = ifm_temp[12][r][c];
      ifm_pack[count].a13 = ifm_temp[13][r][c];
      ifm_pack[count].a14 = ifm_temp[14][r][c];
      ifm_pack[count].a15 = ifm_temp[15][r][c];
      count++;
    }
  }
}

void change_filter(float *wgt, FPACK *filter_pack) {
  int count = 0;
  DATA_T filter_buff[M][N][ROW_G][ROW_G];
  for (int m = 0; m < M; m++) {
    for (int n = 0; n < N; n++) {
      for (int r = 0; r < ROW_G; r++) {
        for (int c = 0; c < ROW_G; c++) {
          filter_buff[m][n][r][c] = wgt[count];
          count++;
        }
      }
    }
  }
  count = 0;
  for (int m = 0; m < M; m++) {
    for (int n = 0; n < N; n++) {
      filter_pack[count].f0 = filter_buff[m][n][0][0];
      filter_pack[count].f1 = filter_buff[m][n][0][1];
      filter_pack[count].f2 = filter_buff[m][n][0][2];
      filter_pack[count].f3 = filter_buff[m][n][1][0];
      filter_pack[count].f4 = filter_buff[m][n][1][1];
      filter_pack[count].f5 = filter_buff[m][n][1][2];
      filter_pack[count].f6 = filter_buff[m][n][2][0];
      filter_pack[count].f7 = filter_buff[m][n][2][1];
      filter_pack[count].f8 = filter_buff[m][n][2][2];
      count++;
    }
  }
}

void chang_cofm(OPACK *ofm_pack, float *ofm) {
  int count = 0;
  DATA_T ofm_temp[M][OR][OC];
  int counter = 0;
  for (int r = 0; r < OR; r++) {
    for (int c = 0; c < OC; c++) {
      ofm_temp[0][r][c] = ofm_pack[counter].b0;
      ofm_temp[1][r][c] = ofm_pack[counter].b1;
      ofm_temp[2][r][c] = ofm_pack[counter].b2;
      ofm_temp[3][r][c] = ofm_pack[counter].b3;
      ofm_temp[4][r][c] = ofm_pack[counter].b4;
      ofm_temp[5][r][c] = ofm_pack[counter].b5;
      ofm_temp[6][r][c] = ofm_pack[counter].b6;
      ofm_temp[7][r][c] = ofm_pack[counter].b7;
      ofm_temp[8][r][c] = ofm_pack[counter].b8;
      ofm_temp[9][r][c] = ofm_pack[counter].b9;
      ofm_temp[10][r][c] = ofm_pack[counter].b10;
      ofm_temp[11][r][c] = ofm_pack[counter].b11;
      ofm_temp[12][r][c] = ofm_pack[counter].b12;
      ofm_temp[13][r][c] = ofm_pack[counter].b13;
      ofm_temp[14][r][c] = ofm_pack[counter].b14;
      ofm_temp[15][r][c] = ofm_pack[counter].b15;
      counter++;
    }
  }
  counter = 0;
  for (int i = 0; i < M; i++) {
    for (int j = 0; j < OR; j++) {
      for (int k = 0; k < OC; k++) {
        ofm[counter] = ofm_temp[i][j][k];
        counter++;
      }
    }
  }
}

void test_data_gen(float *ifm, float *wgt) {
  printf("\nDebug: set ifm\n");
  int counter = 1;
  for (int bn = 0; bn < N; bn++) {
    for (int br = 0; br < OR; br++) {
      for (int bc = 0; bc < OC; bc++) {
        int temp1 = bn * IR * IC + br * IC;
        int i_index = temp1 + bc;
        ifm[i_index] = counter;
        counter++;
      }
    }
  }

  for (int i = 0; i < N * IR * IC; i++) {
    printf("%1f,", ifm[i]);
  }

  printf("\nDebug: set wgt\n");
  counter = 0;
  for (int wm = 0; wm < M; wm++) {
    for (int wn = 0; wn < N; wn++) {
      for (int wk1 = 0; wk1 < K; wk1++) {
        for (int wk2 = 0; wk2 < K; wk2++) {
          int temp1 = wm * N * K * K + wn * K * K;
          int temp2 = wk1 * K + wk2;
          int w_index = temp1 + temp2;
          wgt[w_index] = counter;
          counter++;
        }
      }
    }
  }
  for (int i = 0; i < N * M * K * K; i++) {
    printf("%1f,", wgt[i]);
  }
}

void printf_result(float *ofm, float *ofm_hw) {
  printf("\nDebug: ofm result:\n");
  for (int i = 0; i < M * OR * OC; i++) {
    printf("[%1f, %1f]", ofm[i], ofm_hw[i]);
  }
}

int main() {
#if 0
  float *ifm = new float[N * IR * IC];
  DATA_T *cifm = new DATA_T[N * (IR + 2 * P) * (IC + 2 * P)];
  float *ofm_sw = new float[M * OR * OC];
  float *ofm_hw = new float[M * OR * OC];
  DATA_T *cofm = new DATA_T[M * OR * OC];
  float *wgt = new float[N * M * K * K];
  IPACK *ifm_pack = new IPACK[(IR + 2 * P) * (IC + 2 * P)];
  FPACK *filter_pack = new FPACK[M * N];
  OPACK *ofm_pack = new OPACK[(IR) * (IC)];
#endif

#if 1
  float ifm[N * IR * IC];
  DATA_T cifm[N * (IR + 2 * P) * (IC + 2 * P)];
  float ofm_sw[M * OR * OC];
  float ofm_hw[M * OR * OC];
  DATA_T cofm[M * OR * OC];
  float wgt[N * M * K * K];
  IPACK ifm_pack[(IR + 2 * P) * (IC + 2 * P)];
  FPACK filter_pack[M * N];
  OPACK ofm_pack[(IR) * (IC)];
#endif

  timeval start, end;
  // test_data_gen(ifm, wgt);

  printf("\nThis is EE216\n");

  generate(ifm, wgt);
  gettimeofday(&start, NULL);
  convolution_sw(ifm, ofm_sw, wgt);
  gettimeofday(&end, NULL);
  printf("\nconvolution_sw %lu us\n",
         (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec));

  // First change ifm to standard input feature map ifm->cifm
  change_ifm(ifm, ifm_pack);
  change_filter(wgt, filter_pack);
  // Calculating the convolution and return cofm in standard form.
  gettimeofday(&start, NULL);
  conv(ifm_pack, ofm_pack, filter_pack);
  gettimeofday(&end, NULL);
  printf("\nconvolution_hw %lu us\n",
         (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec));
  // Change the convolution result to required form. cofm->ofm_hw
  chang_cofm(ofm_pack, ofm_hw);
  // Finally, check if the data is right;
  check(ofm_sw, ofm_hw);
#if 0
  delete [] ifm;
  delete [] cifm;
  delete [] ofm_sw;
  delete [] ofm_hw;
  delete [] cofm;
  delete [] wgt;
  delete [] ifm_pack;
  delete [] filter_pack;
  delete [] ofm_pack;
  return 0;
#endif
}
