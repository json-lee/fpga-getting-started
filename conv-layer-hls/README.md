## Convolution Layer Implementation with Raw HLS

利用手工写HLS实现卷积层，其中`M=16, OR=56, OC=56, N=16, IR=56, IC=56, K=3, S=1, P=1`.


![](./assets/conv-arch.png)

HLS核心实现见文件[conv.cpp](conv.cpp)，详细细节主要参考论文[1]。

### Compilation Overhead Analysis

FPGA端到端simulation可以分为以下几个阶段：

- 预编译阶段，包括：Linking、Checking Pragmas、Standard Transforms、Checking Synthesizability、Pre-synthesis、Architecture Synthesis等阶段；
- Scheduling阶段，主要是为loop做pipeline等优化；
- Micro-architecture genration阶段，主要有performing variable lifetime analysis, exploring resource sharing, binding等；
- Generating RTL阶段.

它们各个阶段的占比分析如下图所示：

![](./assets/e2e-breakdown.png)

![](./assets/stage-overhead-breakdown.png)

从中可以看出预编译阶段预编译开销占比较大。


采样出来的性能数据如下图所示：

```
convolution_sw 45974 us
convolution_hw 19987 us
```

![](./assets/perf.png)

相比于[naive版本](../conv-layer-hls-naive/conv.cpp)有明显的加速效果。

### 基本认识

- 仅卷积层这一种相对trivial的kernel端到端simulation就耗时两分钟多，该时间开销较大。这严重影响了硬件开发效率。因此，有必要针对端到端simulation的各个阶段做瓶颈分析和优化；
- HLS的实现过程跟写C/C++代码区别不大，可以利用[LLVM](https://github.com/llvm/llvm-project)、[CIRCT](https://github.com/llvm/circt)等工具做代码分析和变换，以及源源变换等；
- Vivado HLS的书写虽然是基于C/C++等高级语言的，相比于Verilog已经简化了很多。但是从编程的角度去看，依然不太方便。主要表现为pragma较多，选择空间较大，且需要对硬件有一定的认识以后才能写出高效的代码。所以潜在的研究有：
  - 设计新的DSL，自动生成C/C++代码；
  - 设计自动tuning的代码生成工具，自动挖掘pragma的优化组合空间等；

### References

- [1] L. Lu, Y. Liang, Q. Xiao and S. Yan, "Evaluating Fast Algorithms for Convolutional Neural Networks on FPGAs," 2017 IEEE 25th Annual International Symposium on Field-Programmable Custom Computing Machines (FCCM), Napa, CA, 2017, pp. 101-108.
- <https://github.com/Xilinx/HLS-Tiny-Tutorials>
- <https://github.com/Xilinx/HLS-Tiny-Tutorials/tree/master/algorithm_2D_convolution_linebuffer>
- <https://github.com/liru-shanghaitech/Vivado-HLS>
- <https://gitlab.cs.washington.edu/cse599s>
- <https://gitlab.cs.washington.edu/cse599s/hls-tutorials>