#ifndef __CONV_LAYER_H__
#define __CONV_LAYER_H__

#include <cmath>
#include <cstring>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>


#define N 16
#define M 16
#define IR 56
#define IC 56
#define OR 56
#define OC 56
#define K 3
#define S 1
#define P 1
#define w_m 1
#define ROW_G 3
#define w_n 3 // w_n = w_m + w_r - 1 = 4 + 3 -1 = 6

// software convolution for checking;
void convolution_sw(float *ifm, float *ofm, float *weight);
void generate(float *ifm, float *wgt);
void check(float *ofm_sw, float *ofm_hw);
void test_data_gen(float *ifm, float *wgt);

// convolution hardware
void conv(float ifm[N * IR * IC], float ofm[M * OR * OC], float weight[N * M * K * K]);

#endif // __CONV_LAYER_H__ not defined
