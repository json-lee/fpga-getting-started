//*****************************************************************************
//
// Concolution layer hardware implementation with HLS.
//
//*****************************************************************************

#include "conv.h"

// naive convolution without any optimization
void conv(float ifm[N * IR * IC], float ofm[M * OR * OC], float weight[N * M * K * K]) {
  for (int m = 0; m < M; m++) {
    for (int r = 0; r < OR; r++) {
      for (int c = 0; c < OC; c++) {
        float odata = 0;
        int ofm_index = m * OR * OC + r * OC + c;
        for (int n = 0; n < N; n++) {
          for (int kr = 0; kr < K; kr++) {
            for (int kc = 0; kc < K; kc++) {
              float ret;
              int ic = c * S - P + kc;
              int ir = r * S - P + kr;
              int ifm_index = n * IR * IC + ir * IC + ic;
              int wgt_index = m * N * K * K + n * K * K + kr * K + kc;

              if ((ic < 0) || (ir < 0) || (ic > (IC - 1)) || (ir > (IR - 1)))
                ret = 0;
              else
                ret = ifm[ifm_index];
              ret *= weight[wgt_index];
              odata += ret;
            }
          }
        }
        ofm[ofm_index] = odata;
      }
    }
  }
}
