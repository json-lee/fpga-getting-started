// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2018.3
// Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _conv_HH_
#define _conv_HH_

#include "systemc.h"
#include "AESL_pkg.h"

#include "conv_fadd_32ns_32bkb.h"
#include "conv_fmul_32ns_32cud.h"

namespace ap_rtl {

struct conv : public sc_module {
    // Port declarations 16
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_out< sc_lv<16> > ifm_address0;
    sc_out< sc_logic > ifm_ce0;
    sc_in< sc_lv<32> > ifm_q0;
    sc_out< sc_lv<16> > ofm_address0;
    sc_out< sc_logic > ofm_ce0;
    sc_out< sc_logic > ofm_we0;
    sc_out< sc_lv<32> > ofm_d0;
    sc_out< sc_lv<12> > weight_address0;
    sc_out< sc_logic > weight_ce0;
    sc_in< sc_lv<32> > weight_q0;
    sc_signal< sc_logic > ap_var_for_const0;


    // Module declarations
    conv(sc_module_name name);
    SC_HAS_PROCESS(conv);

    ~conv();

    sc_trace_file* mVcdFile;

    ofstream mHdltvinHandle;
    ofstream mHdltvoutHandle;
    conv_fadd_32ns_32bkb<1,5,32,32,32>* conv_fadd_32ns_32bkb_U1;
    conv_fmul_32ns_32cud<1,4,32,32,32>* conv_fmul_32ns_32cud_U2;
    sc_signal< sc_lv<18> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_state1;
    sc_signal< sc_lv<16> > next_mul2_fu_254_p2;
    sc_signal< sc_lv<16> > next_mul2_reg_659;
    sc_signal< sc_logic > ap_CS_fsm_state2;
    sc_signal< sc_lv<5> > m_1_fu_266_p2;
    sc_signal< sc_lv<5> > m_1_reg_667;
    sc_signal< sc_lv<12> > p_shl_cast_fu_284_p1;
    sc_signal< sc_lv<12> > p_shl_cast_reg_672;
    sc_signal< sc_lv<1> > exitcond5_fu_260_p2;
    sc_signal< sc_lv<8> > p_shl8_fu_288_p3;
    sc_signal< sc_lv<8> > p_shl8_reg_677;
    sc_signal< sc_lv<7> > r_cast_fu_296_p1;
    sc_signal< sc_lv<7> > r_cast_reg_682;
    sc_signal< sc_logic > ap_CS_fsm_state3;
    sc_signal< sc_lv<6> > r_1_fu_306_p2;
    sc_signal< sc_lv<6> > r_1_reg_690;
    sc_signal< sc_lv<17> > tmp_3_cast_fu_342_p1;
    sc_signal< sc_lv<17> > tmp_3_cast_reg_695;
    sc_signal< sc_lv<1> > exitcond4_fu_300_p2;
    sc_signal< sc_lv<7> > c_cast_fu_346_p1;
    sc_signal< sc_lv<7> > c_cast_reg_700;
    sc_signal< sc_logic > ap_CS_fsm_state4;
    sc_signal< sc_lv<6> > c_1_fu_356_p2;
    sc_signal< sc_lv<6> > c_1_reg_708;
    sc_signal< sc_lv<32> > ofm_index_cast_fu_389_p1;
    sc_signal< sc_lv<32> > ofm_index_cast_reg_713;
    sc_signal< sc_lv<1> > exitcond3_fu_350_p2;
    sc_signal< sc_lv<17> > phi_mul_cast_fu_393_p1;
    sc_signal< sc_lv<17> > phi_mul_cast_reg_718;
    sc_signal< sc_logic > ap_CS_fsm_state5;
    sc_signal< sc_lv<16> > next_mul_fu_397_p2;
    sc_signal< sc_lv<16> > next_mul_reg_723;
    sc_signal< sc_lv<5> > n_1_fu_413_p2;
    sc_signal< sc_lv<5> > n_1_reg_731;
    sc_signal< sc_lv<13> > tmp5_cast_fu_450_p1;
    sc_signal< sc_lv<13> > tmp5_cast_reg_736;
    sc_signal< sc_lv<1> > exitcond2_fu_407_p2;
    sc_signal< sc_lv<2> > kr_1_fu_468_p2;
    sc_signal< sc_lv<2> > kr_1_reg_744;
    sc_signal< sc_logic > ap_CS_fsm_state6;
    sc_signal< sc_lv<7> > ir_fu_484_p2;
    sc_signal< sc_lv<7> > ir_reg_749;
    sc_signal< sc_lv<1> > exitcond1_fu_462_p2;
    sc_signal< sc_lv<17> > tmp_2_cast_fu_515_p1;
    sc_signal< sc_lv<17> > tmp_2_cast_reg_754;
    sc_signal< sc_lv<5> > tmp_4_fu_531_p2;
    sc_signal< sc_lv<5> > tmp_4_reg_759;
    sc_signal< sc_lv<1> > tmp_7_fu_537_p2;
    sc_signal< sc_lv<1> > tmp_7_reg_764;
    sc_signal< sc_lv<2> > kc_1_fu_553_p2;
    sc_signal< sc_lv<2> > kc_1_reg_772;
    sc_signal< sc_logic > ap_CS_fsm_state7;
    sc_signal< sc_lv<7> > ic_fu_569_p2;
    sc_signal< sc_lv<7> > ic_reg_777;
    sc_signal< sc_lv<1> > exitcond_fu_547_p2;
    sc_signal< sc_lv<17> > ifm_index_fu_583_p2;
    sc_signal< sc_lv<17> > ifm_index_reg_783;
    sc_signal< sc_lv<13> > wgt_index_fu_606_p2;
    sc_signal< sc_lv<13> > wgt_index_reg_788;
    sc_signal< sc_logic > ap_CS_fsm_state8;
    sc_signal< sc_lv<32> > ret1_fu_651_p3;
    sc_signal< sc_lv<32> > ret1_reg_803;
    sc_signal< sc_logic > ap_CS_fsm_state9;
    sc_signal< sc_lv<32> > weight_load_reg_808;
    sc_signal< sc_lv<32> > grp_fu_250_p2;
    sc_signal< sc_lv<32> > ret_1_reg_813;
    sc_signal< sc_logic > ap_CS_fsm_state13;
    sc_signal< sc_lv<32> > grp_fu_245_p2;
    sc_signal< sc_logic > ap_CS_fsm_state18;
    sc_signal< sc_lv<5> > m_reg_119;
    sc_signal< sc_lv<16> > phi_mul1_reg_130;
    sc_signal< sc_lv<6> > r_reg_142;
    sc_signal< sc_lv<6> > c_reg_153;
    sc_signal< sc_lv<5> > n_reg_164;
    sc_signal< sc_lv<32> > odata_reg_175;
    sc_signal< sc_lv<16> > phi_mul_reg_188;
    sc_signal< sc_lv<32> > odata_1_reg_199;
    sc_signal< sc_lv<2> > kr_reg_211;
    sc_signal< sc_lv<32> > odata_2_reg_222;
    sc_signal< sc_lv<2> > kc_reg_234;
    sc_signal< sc_lv<64> > tmp_6_fu_454_p1;
    sc_signal< sc_lv<64> > tmp_10_fu_614_p1;
    sc_signal< sc_lv<64> > tmp_11_fu_618_p1;
    sc_signal< sc_logic > ap_CS_fsm_state14;
    sc_signal< sc_logic > ap_CS_fsm_state10;
    sc_signal< sc_lv<4> > tmp_fu_272_p1;
    sc_signal< sc_lv<11> > p_shl_fu_276_p3;
    sc_signal< sc_lv<12> > p_shl9_fu_312_p3;
    sc_signal< sc_lv<9> > p_shl1_fu_324_p3;
    sc_signal< sc_lv<13> > p_shl9_cast_fu_320_p1;
    sc_signal< sc_lv<13> > p_shl1_cast_fu_332_p1;
    sc_signal< sc_lv<13> > tmp_3_fu_336_p2;
    sc_signal< sc_lv<10> > tmp_1_fu_362_p4;
    sc_signal< sc_lv<16> > tmp1_fu_372_p3;
    sc_signal< sc_lv<17> > tmp1_cast_fu_380_p1;
    sc_signal< sc_lv<17> > ofm_index_fu_384_p2;
    sc_signal< sc_lv<4> > tmp_5_fu_419_p1;
    sc_signal< sc_lv<7> > p_shl2_fu_423_p3;
    sc_signal< sc_lv<8> > n_cast_fu_403_p1;
    sc_signal< sc_lv<8> > tmp6_fu_435_p2;
    sc_signal< sc_lv<9> > p_shl2_cast_fu_431_p1;
    sc_signal< sc_lv<9> > tmp6_cast_fu_440_p1;
    sc_signal< sc_lv<9> > tmp5_fu_444_p2;
    sc_signal< sc_lv<2> > tmp2_fu_474_p2;
    sc_signal< sc_lv<7> > tmp2_cast_fu_480_p1;
    sc_signal< sc_lv<10> > p_shl4_fu_497_p3;
    sc_signal< sc_lv<13> > p_shl3_fu_489_p3;
    sc_signal< sc_lv<13> > p_shl4_cast_fu_505_p1;
    sc_signal< sc_lv<13> > tmp_2_fu_509_p2;
    sc_signal< sc_lv<4> > p_shl5_fu_519_p3;
    sc_signal< sc_lv<5> > p_shl5_cast_fu_527_p1;
    sc_signal< sc_lv<5> > kr_cast_fu_458_p1;
    sc_signal< sc_lv<2> > tmp3_fu_559_p2;
    sc_signal< sc_lv<7> > tmp3_cast_fu_565_p1;
    sc_signal< sc_lv<17> > ic_cast_fu_574_p1;
    sc_signal< sc_lv<17> > tmp4_fu_578_p2;
    sc_signal< sc_lv<5> > kc_cast_fu_543_p1;
    sc_signal< sc_lv<5> > tmp8_fu_588_p2;
    sc_signal< sc_lv<12> > tmp8_cast_fu_593_p1;
    sc_signal< sc_lv<12> > tmp7_fu_597_p2;
    sc_signal< sc_lv<13> > tmp7_cast_fu_602_p1;
    sc_signal< sc_lv<32> > wgt_index_cast_fu_611_p1;
    sc_signal< sc_lv<7> > tmp_s_fu_623_p2;
    sc_signal< sc_lv<1> > tmp_9_fu_635_p2;
    sc_signal< sc_lv<1> > tmp9_fu_640_p2;
    sc_signal< sc_lv<1> > tmp_8_fu_627_p3;
    sc_signal< sc_lv<1> > or_cond7_fu_645_p2;
    sc_signal< sc_lv<18> > ap_NS_fsm;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<18> ap_ST_fsm_state1;
    static const sc_lv<18> ap_ST_fsm_state2;
    static const sc_lv<18> ap_ST_fsm_state3;
    static const sc_lv<18> ap_ST_fsm_state4;
    static const sc_lv<18> ap_ST_fsm_state5;
    static const sc_lv<18> ap_ST_fsm_state6;
    static const sc_lv<18> ap_ST_fsm_state7;
    static const sc_lv<18> ap_ST_fsm_state8;
    static const sc_lv<18> ap_ST_fsm_state9;
    static const sc_lv<18> ap_ST_fsm_state10;
    static const sc_lv<18> ap_ST_fsm_state11;
    static const sc_lv<18> ap_ST_fsm_state12;
    static const sc_lv<18> ap_ST_fsm_state13;
    static const sc_lv<18> ap_ST_fsm_state14;
    static const sc_lv<18> ap_ST_fsm_state15;
    static const sc_lv<18> ap_ST_fsm_state16;
    static const sc_lv<18> ap_ST_fsm_state17;
    static const sc_lv<18> ap_ST_fsm_state18;
    static const sc_lv<32> ap_const_lv32_0;
    static const sc_lv<32> ap_const_lv32_1;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<32> ap_const_lv32_2;
    static const sc_lv<32> ap_const_lv32_3;
    static const sc_lv<32> ap_const_lv32_4;
    static const sc_lv<32> ap_const_lv32_5;
    static const sc_lv<32> ap_const_lv32_6;
    static const sc_lv<32> ap_const_lv32_7;
    static const sc_lv<32> ap_const_lv32_8;
    static const sc_lv<32> ap_const_lv32_C;
    static const sc_lv<32> ap_const_lv32_11;
    static const sc_lv<5> ap_const_lv5_0;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<16> ap_const_lv16_0;
    static const sc_lv<6> ap_const_lv6_0;
    static const sc_lv<2> ap_const_lv2_0;
    static const sc_lv<32> ap_const_lv32_D;
    static const sc_lv<32> ap_const_lv32_9;
    static const sc_lv<16> ap_const_lv16_C40;
    static const sc_lv<5> ap_const_lv5_10;
    static const sc_lv<5> ap_const_lv5_1;
    static const sc_lv<7> ap_const_lv7_0;
    static const sc_lv<4> ap_const_lv4_0;
    static const sc_lv<6> ap_const_lv6_38;
    static const sc_lv<6> ap_const_lv6_1;
    static const sc_lv<3> ap_const_lv3_0;
    static const sc_lv<32> ap_const_lv32_F;
    static const sc_lv<2> ap_const_lv2_3;
    static const sc_lv<2> ap_const_lv2_1;
    static const sc_lv<7> ap_const_lv7_37;
    static const bool ap_const_boolean_1;
    // Thread declarations
    void thread_ap_var_for_const0();
    void thread_ap_clk_no_reset_();
    void thread_ap_CS_fsm_state1();
    void thread_ap_CS_fsm_state10();
    void thread_ap_CS_fsm_state13();
    void thread_ap_CS_fsm_state14();
    void thread_ap_CS_fsm_state18();
    void thread_ap_CS_fsm_state2();
    void thread_ap_CS_fsm_state3();
    void thread_ap_CS_fsm_state4();
    void thread_ap_CS_fsm_state5();
    void thread_ap_CS_fsm_state6();
    void thread_ap_CS_fsm_state7();
    void thread_ap_CS_fsm_state8();
    void thread_ap_CS_fsm_state9();
    void thread_ap_done();
    void thread_ap_idle();
    void thread_ap_ready();
    void thread_c_1_fu_356_p2();
    void thread_c_cast_fu_346_p1();
    void thread_exitcond1_fu_462_p2();
    void thread_exitcond2_fu_407_p2();
    void thread_exitcond3_fu_350_p2();
    void thread_exitcond4_fu_300_p2();
    void thread_exitcond5_fu_260_p2();
    void thread_exitcond_fu_547_p2();
    void thread_ic_cast_fu_574_p1();
    void thread_ic_fu_569_p2();
    void thread_ifm_address0();
    void thread_ifm_ce0();
    void thread_ifm_index_fu_583_p2();
    void thread_ir_fu_484_p2();
    void thread_kc_1_fu_553_p2();
    void thread_kc_cast_fu_543_p1();
    void thread_kr_1_fu_468_p2();
    void thread_kr_cast_fu_458_p1();
    void thread_m_1_fu_266_p2();
    void thread_n_1_fu_413_p2();
    void thread_n_cast_fu_403_p1();
    void thread_next_mul2_fu_254_p2();
    void thread_next_mul_fu_397_p2();
    void thread_ofm_address0();
    void thread_ofm_ce0();
    void thread_ofm_d0();
    void thread_ofm_index_cast_fu_389_p1();
    void thread_ofm_index_fu_384_p2();
    void thread_ofm_we0();
    void thread_or_cond7_fu_645_p2();
    void thread_p_shl1_cast_fu_332_p1();
    void thread_p_shl1_fu_324_p3();
    void thread_p_shl2_cast_fu_431_p1();
    void thread_p_shl2_fu_423_p3();
    void thread_p_shl3_fu_489_p3();
    void thread_p_shl4_cast_fu_505_p1();
    void thread_p_shl4_fu_497_p3();
    void thread_p_shl5_cast_fu_527_p1();
    void thread_p_shl5_fu_519_p3();
    void thread_p_shl8_fu_288_p3();
    void thread_p_shl9_cast_fu_320_p1();
    void thread_p_shl9_fu_312_p3();
    void thread_p_shl_cast_fu_284_p1();
    void thread_p_shl_fu_276_p3();
    void thread_phi_mul_cast_fu_393_p1();
    void thread_r_1_fu_306_p2();
    void thread_r_cast_fu_296_p1();
    void thread_ret1_fu_651_p3();
    void thread_tmp1_cast_fu_380_p1();
    void thread_tmp1_fu_372_p3();
    void thread_tmp2_cast_fu_480_p1();
    void thread_tmp2_fu_474_p2();
    void thread_tmp3_cast_fu_565_p1();
    void thread_tmp3_fu_559_p2();
    void thread_tmp4_fu_578_p2();
    void thread_tmp5_cast_fu_450_p1();
    void thread_tmp5_fu_444_p2();
    void thread_tmp6_cast_fu_440_p1();
    void thread_tmp6_fu_435_p2();
    void thread_tmp7_cast_fu_602_p1();
    void thread_tmp7_fu_597_p2();
    void thread_tmp8_cast_fu_593_p1();
    void thread_tmp8_fu_588_p2();
    void thread_tmp9_fu_640_p2();
    void thread_tmp_10_fu_614_p1();
    void thread_tmp_11_fu_618_p1();
    void thread_tmp_1_fu_362_p4();
    void thread_tmp_2_cast_fu_515_p1();
    void thread_tmp_2_fu_509_p2();
    void thread_tmp_3_cast_fu_342_p1();
    void thread_tmp_3_fu_336_p2();
    void thread_tmp_4_fu_531_p2();
    void thread_tmp_5_fu_419_p1();
    void thread_tmp_6_fu_454_p1();
    void thread_tmp_7_fu_537_p2();
    void thread_tmp_8_fu_627_p3();
    void thread_tmp_9_fu_635_p2();
    void thread_tmp_fu_272_p1();
    void thread_tmp_s_fu_623_p2();
    void thread_weight_address0();
    void thread_weight_ce0();
    void thread_wgt_index_cast_fu_611_p1();
    void thread_wgt_index_fu_606_p2();
    void thread_ap_NS_fsm();
    void thread_hdltv_gen();
};

}

using namespace ap_rtl;

#endif
