//*****************************************************************************
//
// Concolution layer test bench.
//
//*****************************************************************************

#include "conv.h"

// software convolution
void convolution_sw(float *ifm, float *ofm, float *weight) {
  for (int m = 0; m < M; m++) {
    for (int r = 0; r < OR; r++) {
      for (int c = 0; c < OC; c++) {
        float odata = 0;
        int ofm_index = m * OR * OC + r * OC + c;
        for (int n = 0; n < N; n++) {
          for (int kr = 0; kr < K; kr++) {
            for (int kc = 0; kc < K; kc++) {
              float ret;
              int ic = c * S - P + kc;
              int ir = r * S - P + kr;
              int ifm_index = n * IR * IC + ir * IC + ic;
              int wgt_index = m * N * K * K + n * K * K + kr * K + kc;

              if ((ic < 0) || (ir < 0) || (ic > (IC - 1)) || (ir > (IR - 1)))
                ret = 0;
              else
                ret = ifm[ifm_index];
              ret *= weight[wgt_index];
              odata += ret;
            }
          }
        }
        ofm[ofm_index] = odata;
      }
    }
  }
}

void generate(float *ifm, float *wgt) {
  for (int i = 0; i < N * IR * IC; i++) {
    ifm[i] = (float)i / 3000.0;
  }
  for (int i = 0; i < N * M * K * K; i++) {
    wgt[i] = (float)i / 3000.0;
  }
}

void check(float *ofm_sw, float *ofm_hw) {
  int error = 0;
  for (int i = 0; i < N * OR * OC; i++) {
    if (((ofm_sw[i] - ofm_hw[i]) > 0.01) || ((ofm_sw[i] - ofm_hw[i]) < -0.01))
      error++;
  }
  if (error > 0)
    printf("error count = %d\n", error);
  else
    printf("correct!\n", error);
}


void test_data_gen(float *ifm, float *wgt) {
  printf("Debug: set input feature map\n");
  int counter = 1;
  for (int bn = 0; bn < N; bn++) {
    for (int br = 0; br < OR; br++) {
      for (int bc = 0; bc < OC; bc++) {
        int temp1 = bn * IR * IC + br * IC;
        int i_index = temp1 + bc;
        ifm[i_index] = counter;
        counter++;
      }
    }
  }

  for (int i = 0; i < N * IR * IC; i++) {
    printf("%1f,", ifm[i]);
  }

  printf("Debug: set weight\n");
  counter = 0;
  for (int wm = 0; wm < M; wm++) {
    for (int wn = 0; wn < N; wn++) {
      for (int wk1 = 0; wk1 < K; wk1++) {
        for (int wk2 = 0; wk2 < K; wk2++) {
          int temp1 = wm * N * K * K + wn * K * K;
          int temp2 = wk1 * K + wk2;
          int w_index = temp1 + temp2;
          wgt[w_index] = counter;
          counter++;
        }
      }
    }
  }
  for (int i = 0; i < N * M * K * K; i++) {
    printf("%1f,", wgt[i]);
  }
}

void printf_result(float *ofm, float *ofm_hw) {
  printf("Debug: output feature map result:\n");
  for (int i = 0; i < M * OR * OC; i++) {
    printf("[%1f, %1f]", ofm[i], ofm_hw[i]);
  }
}

int main() {
  float ifm[N * IR * IC];
  float ofm_sw[M * OR * OC];
  float wgt[N * M * K * K];

  timeval start, end;
  // test_data_gen(ifm, wgt);

  generate(ifm, wgt);
  gettimeofday(&start, NULL);
  convolution_sw(ifm, ofm_sw, wgt);
  gettimeofday(&end, NULL);
  printf("\nconvolution_sw %lu us\n",
         (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec));

  gettimeofday(&start, NULL);
  conv(ifm, ofm_sw, wgt);
  gettimeofday(&end, NULL);
  printf("\nconvolution_hw %lu us\n",
         (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec));
  return 0;
}
